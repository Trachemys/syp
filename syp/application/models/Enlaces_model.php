<?php
class Enlaces_model extends CI_Model{
	public function __consturct(){
		parent:__construct();
	}
	

    // Mostraremos los detalles de un enlace sacado vía KEY
    // Usaremos una key para el enlace en vez de la ID para evitar que nos roben todos los enlaces vía ID, así que generamos un
    // pseudorandom key y cuando insertemos el enlace le añadimos a esa pseudorandom key un -$id para que sea una key única
    public function perfil_enlaces_series($key){
            // Series
       $id =  $this->uri->segment(3);
        $this->db->select('links.url as url, users.id_user as id_user, contentsseasons.series as series, contentsepisodes.episode as episode, users.username as username, contentstitles.title as title, contentsseasons.number as seasonnumber');
        $this->db->from('links');
        $this->db->join('contentsepisodes', 'contentsepisodes.episode = links.content');
        $this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
        $this->db->join('users', 'users.id_user = links.user');
        $this->db->join('contentstitles', 'contentstitles.content = links.content');

        $this->db->where('links.key',$key);
        $this->db->where('links.id',$id);
       
        $consulta = $this->db->get();
        return $consulta->row();  

  
    }
    public function perfil_enlaces_peliculas($key){
        $id =  $this->uri->segment(3);
        $this->db->select('*');
        $this->db->from('links');
        $this->db->join('users', 'users.id_user = links.user');
        $this->db->where('links.key',$key);
        $this->db->where('links.id',$id);

        $consulta = $this->db->get();
        return $consulta->row();  
    }

	
	// Sacamos los enlaces relacionados con cada capítulo
		public function lista_enlaces_capitulo_visionado_online($content){
		$this->db->select('links.url AS url, links.date AS date, linksqualities.quality AS quality, users.username AS username, users.id_user AS iduser,links.id as idlink, links.key AS key, languagesubtitles.tag as idioma_sub, languageaudio.tag as idioma_audio, links.key as key');
		$this->db->from('links');
		$this->db->join('tbllinksqualities', 'tbllinksqualities.id = links.quality');
		$this->db->join('users', 'users.id_user = links.user');
        $this->db->join("languages as languagesubtitles", 'languagesubtitles.id = links.subtitles');
        $this->db->join("languages as languageaudio", 'languageaudio.id = links.language');
        $this->db->where('links.content',$content);
				$this->db->where('links.type','1');

		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function lista_enlaces_capitulo_descarga_directa($content){
		$this->db->select('links.url AS url, links.date AS date, linksqualities.quality AS quality, users.username AS username, users.id_user AS iduser, links.id as idlink, links.key AS key, languagesubtitles.tag as idioma_sub, languageaudio.tag as idioma_audio, links.key as key');
		$this->db->from('links');
		$this->db->join('tbllinksqualities', 'tbllinksqualities.id = links.quality');
		$this->db->join('users', 'users.id_user = links.user');
		$this->db->join("languages as languagesubtitles", 'languagesubtitles.id = links.subtitles');
        $this->db->join("languages as languageaudio", 'languageaudio.id = links.language');

		$this->db->where('links.content',$content);
				$this->db->where('links.type','2');

		$consulta = $this->db->get();
		return $consulta->result();
	}

	  function ver_si_reportado($idlink){
 
        $this->db->select('link');
        $this->db->from('userslinksreports');
        $this->db->where('link', $idlink);
        $consulta = $this->db->get();
     $count = $consulta->num_rows();
     return $count;

    }

    function info_key($key){
        $this->db->select('*');
        $this->db->from('links');
        $this->db->where('links.key', $key);
        $consulta = $this->db->get();
        return $consulta->row();
    }
    function reportar_enlace($idlink){
        $motivo = $this->input->post('type_report');
        $observaciones = $this->input->post('description');
        if ($this->ion_auth->logged_in())
        {
            $user = $this->ion_auth->user()->row()->id_user;
     
            $this->db->select('link');
            $this->db->from('userslinksreports');
            $this->db->where('link', $idlink);
            $this->db->where('user', $user);
            $consulta = $this->db->get();
            $count = $consulta->num_rows();

                if ($count !== 1){
                    $data = array(
                    'user' => "$user",
                    'link' => "$idlink",
                    'reason' => "$motivo",
                    'description' => "$observaciones",
                    'date' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('userslinksreports', $data);            
                } else {
                    $this->db->where('user', $user);
                    $this->db->where('link', $idlink);
                    $this->db->delete('userslinksreports');

                    $data = array(
                    'user' => $user,
                    'link' => $idlink,
                    'reason' => $motivo,
                    'description' => $observaciones,
                    'date' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('userslinksreports', $data);
                }

            $arraydata = array(
            "titulo" => "Reporte enviado",
            "mensaje"  => "Tu reporte se ha recibido correctamente.</br> Muchísimas gracias por informar de enlaces con algún tipo de problema, gracias a ti esta plataforma puede ofrecer contenidos de calidad.",
            "resultado"  => "correcto",
            );
        } else {
            $arraydata = array(
            "titulo" => "No hemos podido enviar tu reporte :(",
            "mensaje"  => "Usuario sin registrar.</br> Te agradecemos tu reporte pero no hemos podido registrarlo, es una función específica para usuarios. ¿Por qué no te registras? Es completamente gratuito y además nos echarías un cable a descartar los enlaces que no sirven.",
            "resultado"  => "correcto",
            );
        }

    return $arraydata;        
}


    public function lista_calidad(){
        $this->db->select('*');
        $this->db->from('linksqualities');

        $consulta = $this->db->get();
        return $consulta->result();

    }	

    public function lista_idiomas(){
        $this->db->select('*');
        $this->db->from('languages');
        
        $consulta = $this->db->get();
        return $consulta->result();

    }  

    public function lista_tipo_enlaces(){
        $this->db->select('*');
        $this->db->from('linkstypes');

        $consulta = $this->db->get();
        return $consulta->result();

    }   

    public function url_redirect(){
        $key = $this->input->post('key');
        $this->db->select('links.url');
        $this->db->from('links');
        $this->db->where('key', $key);
        $this->db->limit(1);
        $consulta = $this->db->get();
        return $consulta->row('url');
    }  	

    public function subir_enlace(){
        $fecha_hoy = date('Y-m-d H:i:s');        
        $id_user = $this->ion_auth->user()->row()->id;
        $url = $this->input->post('url');
        $resolucion = $this->input->post('resolucion');
        $tipo_enlace = $this->input->post('tipo_enlace');
        $audio = $this->input->post('audio');
        $subtitulos = $this->input->post('subtitulos');
        $content = $this->input->post('idcontenido');
        $key = bin2hex(openssl_random_pseudo_bytes(5)) . $content;

        if(is_numeric($resolucion) && is_numeric($tipo_enlace) && is_numeric($audio) && is_numeric($subtitulos) && is_numeric($content)){

            $data = array(
            'url' => $url ,
            'type' => $tipo_enlace ,
            'quality' => $resolucion ,
            'user' => $id_user , 
            'date' => $fecha_hoy , 
            'active' => '1' ,
            'subtitles' => $subtitulos ,        
            'content' => $content , 
            'language' => $audio , 
            'key' => $key ,                                                                           
            );

            $this->db->insert('links', $data); 
            $res = 'correcto';
        } else {
            $res = "incorrecto";
        }

        return $res;
    }


    public function tipo_reportes_enlaces(){
        $this->db->select('*');
        $this->db->from('reports');
        $this->db->where('type', '1');
        $consulta = $this->db->get();
        return $consulta->result();

    }

		}