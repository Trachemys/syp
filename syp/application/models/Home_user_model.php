<?php
class Home_user_model extends CI_Model{
	public function __consturct(){

		parent:__construct();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
	}

	public function recomendaciones_personales($id_user){

		$consulta = $this->db->get();
		return $consulta->result();
	}


	public function siguiendo($id_user){
		$this->db->select('usersfollowing.content');
		$this->db->from('usersfollowing');
		$this->db->where('usersfollowing.user',$id_user);



			$consulta = $this->db->get();
			return $consulta->result();
	}

	public function ultimo_cap_emitido($id_contenido){
		$fecha_hoy = date('Y-m-d H:i:s');
		$idioma = $this->ion_auth->user()->row()->language;

		$this->db->select('contentsepisodes.episode, contentstitles.title, contentsseries.rate, contentsplots.plot, contentsseries.series');
		$this->db->from('contentsseasons');
		$this->db->join('contentsepisodes', 'contentsepisodes.season = contentsseasons.id');
		$this->db->join('contentstitles', 'contentstitles.content = contentsseasons.series');
		$this->db->join('contentsplots', 'contentsplots.content = contentsseasons.series');
		$this->db->join('contentsseries', 'contentsseries.series = contentsseasons.series');
		$this->db->where('contentsseasons.series', $id_contenido);
		$this->db->where('contentsepisodes.date <', $fecha_hoy);
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('contentsplots.language', $idioma);

		$this->db->order_by('contentsepisodes.date', 'DESC');
		$this->db->limit(1);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function ver_si_visto_ultimo_cap($id_capitulo, $id_user){
		$this->db->select('*');
		$this->db->from('usersviewed');
		$this->db->where('usersviewed.user',$id_user);	
		$this->db->where('usersviewed.content',$id_capitulo);	
		$this->db->limit(1);
		$consulta = $this->db->get();
		     $count = $consulta->num_rows();
		     if ($count === 0){
		     	$res = '0'; // 0 = hay capítulos pendientes de ver
		     } else {
		     	$res = '1'; // 1 = no hay capítulos pendientes de ver
		     }
		     	return $res;

	}

	public function peliculas_pendientes_home($id_user){
		$idioma = $this->ion_auth->user()->row()->language;
		$this->db->select('userspending.content, contentsfilms.rate, contentsplots.plot, contentstitles.title');
		$this->db->from('userspending');
		$this->db->join('contentsfilms', 'contentsfilms.id = userspending.content');
		$this->db->join('contentstitles', 'contentstitles.content = userspending.content');
		$this->db->join('contentsplots', 'contentsplots.content = contentsfilms.id');
		$this->db->where('contentsplots.language', $idioma);
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('userspending.user', $id_user);
		$consulta = $this->db->get();
		return $consulta->result();

	}

	public function series_pendientes_home($id_user){
		$idioma = $this->ion_auth->user()->row()->language;
		$this->db->select('userspending.content, contentsseries.rate, contentstitles.title, contentsplots.plot');
		$this->db->from('userspending');
		$this->db->join('contentsseries', 'contentsseries.series = userspending.content');
		$this->db->join('contentstitles', 'contentstitles.content = userspending.content');
		$this->db->join('contentsplots', 'contentsplots.content = contentsseries.series');
		$this->db->where('contentsplots.language', $idioma);
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('userspending.user', $id_user);
		$consulta = $this->db->get();
		return $consulta->result();

	}	

		// Sacamos los capítulos con 3 días de estrenos y que tengan enlaces
	public function nuevos_capitulos_invitados(){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}

		$fecha_hoy = date('Y-m-d H:i:s');
		$data = new DateTime('-3 day');
 		$fecha_3_dias = $data->format('Y-m-d H:i:s');

		$this->db->select('contentsseasons.series, contentstitles.title');
		$this->db->group_by('contentsepisodes.episode');
		$this->db->from('contentsepisodes');
		$this->db->where('contentsepisodes.date >=', $fecha_3_dias);
		$this->db->where('contentsepisodes.date <=', $fecha_hoy);
		$this->db->join('links', 'links.content = contentsepisodes.episode');
		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
		$this->db->join('contentstitles', 'contentstitles.content = contentsseasons.series');
		$this->db->where('contentstitles.language', $idioma);
		$this->db->order_by('contentsepisodes.date', 'DESC');
		$this->db->limit(20);
		$consulta = $this->db->get();

		return $consulta->result();

	}

	public function estrenos(){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}

		$fecha_hoy = date('Y-m-d H:i:s');
		$data = new DateTime('-31 day');
 		$fecha_10_dias = $data->format('Y-m-d');

		$this->db->select('*');
		$this->db->from('contentsfilms');
		$this->db->group_by('contentsfilms.id');
		$this->db->where('contentsfilms.year >=', $fecha_10_dias);
		$this->db->where('contentsfilms.year <=', $fecha_hoy);
		//$this->db->join('links', 'links.content = contentsfilms.id');
		$this->db->join('contentstitles', 'contentstitles.content = contentsfilms.id');
		$this->db->join('contentsplots', 'contentsplots.content = contentsfilms.id');
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('contentsplots.language', $idioma);
		$this->db->order_by('contentsfilms.year', 'DESC');
		$this->db->limit(30);

		$consulta = $this->db->get();
		return $consulta->result();

	}

	public function trending_series(){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}


		$this->db->select('*');
		$this->db->from('trendings');
		$this->db->join('contentsseries', 'contentsseries.series = trendings.content');
		$this->db->group_by('contentsseries.series');
		$this->db->join('contentstitles', 'contentstitles.content = contentsseries.series');
		$this->db->join('contentsplots', 'contentsplots.content = contentsseries.series');
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('contentsplots.language', $idioma);
		$this->db->limit(20);

		$consulta = $this->db->get();
		return $consulta->result();

	}	

	public function trending_films(){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}


		$this->db->select('*');
		$this->db->from('trendings');
		$this->db->join('contentsfilms', 'contentsfilms.id = trendings.content');
		$this->db->group_by('contentsfilms.id');
		$this->db->join('contentstitles', 'contentstitles.content = contentsfilms.id');
		$this->db->join('contentsplots', 'contentsplots.content = contentsfilms.id');
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('contentsplots.language', $idioma);
		$this->db->limit(20);

		$consulta = $this->db->get();
		return $consulta->result();

	}	

	public function forzar_plot_en($content){
		$this->db->select('plot');
		$this->db->from('contentsplots');
		$this->db->where('contentsplots.language', '2');
		$this->db->where('contentsplots.id', $content);	
		$consulta = $this->db->get();
		return $consulta->row();		

	}

	public function forzar_plot_es($content){
		$this->db->select('plot');
		$this->db->from('contentsplots');
		$this->db->where('contentsplots.language', '2');
		$this->db->where('contentsplots.id', $content);
		$consulta = $this->db->get();
		return $consulta->row();	
	}

}