<?php
class Genres_model extends CI_Model{
	public function __consturct(){

		parent:__construct();

	}


	public function mostrar_generos_contenido($id_contenido){

		$this->db->select('genres.name');
  		$this->db->from('contentsgenres');
  		$this->db->join('genres', 'genres.id = contentsgenres.genre');
  		$this->db->where('contentsgenres.content', $id_contenido);
  		$consulta = $this->db->get();
  		return $consulta->result();

	}

		public function lista_generos(){
			$this->db->select('genres.name, genres.id');
			$this->db->from('genres');
			$consulta = $this->db->get();
			return $consulta->result();
		// Función para borrar la serie que está ya siguiendo

	}

	public function detalle_generos($id_genero){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentstitles', 'contentstitles.content = contents.id');
		$this->db->join('contentsgenres', 'contentsgenres.content = contentstitles.content');

		$this->db->where('contentsgenres.genre', $id_genero);
		$this->db->where('contentstitles.language', $idioma);

		$consulta = $this->db->get();
		return $consulta->result();

	}


}