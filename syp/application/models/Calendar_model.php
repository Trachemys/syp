<?php
class Calendar_model extends CI_Model{
	public function __consturct(){

		parent:__construct();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
	}


	public function events(){

		$user = $this->ion_auth->user()->row()->id_user;
		$idioma = $this->ion_auth->user()->row()->language;

	$this->db->select('contentsepisodes.episode, contentstitles.title, contentsepisodes.date,   contentsseasons.number AS seasonnumber, contentsepisodes.number AS episodenumber, contentsseasons.series');
	$this->db->from('contentsepisodes');
	$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
	$this->db->join('contentstitles', 'contentstitles.content = contentsseasons.series');
	$this->db->join('usersfollowing', 'usersfollowing.content = contentsseasons.series');
	$this->db->join('users', 'users.id_user = usersfollowing.user');
	$this->db->where('contentsepisodes.date is NOT NULL');
		$this->db->where('users.id_user',$user);
		$this->db->where('contentstitles.language', $idioma);
		$query = $this->db->get();

 $results = array();
    foreach ($query->result() as $row){
        $results[] = array(
                'id' => $row->series,
                'title' => 'S'. $row->seasonnumber .'E' . $row->episodenumber . ' ' . $row->title,
                'start' => $row->date,
                'url' => base_url() . 'series/' . $row->series . '/' . $row->title,
        );

    }
    return $results;

	}

		


}