<?php
class Add_series_model extends CI_Model{
	public function __consturct(){

		parent:__construct();

	}


	public function add_queue($content){
		$data = array('content' => $content);
		$this->db->insert('changesqueue', $data); 
	}

	public function delete_queue($content){
		$this->db->delete('changesqueue', array('content' => $content)); 

	}



	public function get_queue(){
		$this->db->select('changesqueue.content as id');
		$this->db->from('changesqueue');
		$this->db->join('contentsseries', 'contentsseries.series = changesqueue.content');
		$consulta = $this->db->get();
		return $consulta->result();
	}	

		public function add_trending($tmdbid){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');
		$this->db->where('contents.tmdbid', $tmdbid);
		$this->db->limit(1);
		$consulta = $this->db->get();

		$content = $consulta->row()->id;

		$array = array('content' => $content);
		$this->db->insert('trendings', $array); 		
	}

	public function get_info_content($content){
		$this->db->select('contents.tmdbid');
		$this->db->from('contents');
		$this->db->where('id', $content);
		$this->db->limit(1);
		$consulta = $this->db->get();
		return $consulta->row();

	}

	public function todos_relacionados(){
		$this->db->select('*');
		$this->db->from('recommendations');
		$this->db->join('contentsfilms', 'contentsfilms.id = recommendations.content');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function arreglar_relacionados($relacionado){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$this->db->where('contents.tmdbid', $relacionado);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function editar_relacionados($idpropia, $idtmdb){
		$data = array(
               'cont_rel' => $idpropia
            );

$this->db->where('related_content', $idtmdb);
$this->db->update('recommendations', $data); 
	}

	public function contar_ceros(){
		$this->db->select('*');
		$this->db->from('recommendations');
		$this->db->where('cont_rel', '0');
		$consulta = $this->db->get();

		return $consulta->num_rows();
	}
/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA EDITAR PELÍCULAS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con la edición de películas
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function editar_serie($data){
		// Sacamos la id a la que pertenece la serie vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');
		$consulta = $this->db->get();
		$resultado = $consulta->row();
		

		// Insertamos los datos en contentsseries
			$contentsseries = array(
               'duration' => $data['runtime'],
               'rate' => $data['rate'],
               'year' => $data['fecha_estreno'],
               'status' => $data['estado'],
               'trailer' => $data['trailer'],
            );
			$this->db->where('series', $resultado->id);
			$this->db->update('contentsseries', $contentsseries); 



		// Insertamos título en inglés
			$titulo_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'title' => $data['titulo_ingles']
			);
			$this->db->where('contentstitles.language', '2');
			$this->db->where('contentstitles.content', $resultado->id);
			$this->db->update('contentstitles', $titulo_ingles);

		// Insertamos título en español
			$titulo_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'title' => $data['titulo_español']
			);
			$this->db->where('contentstitles.language', '1');
			$this->db->where('contentstitles.content', $resultado->id);			
			$this->db->update('contentstitles', $titulo_español); 

		// Insertamos sinopsis en inglés
			$sinopsis_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'plot' => $data['sinopsis_ingles']
			);
			$this->db->where('contentsplots.content', $resultado->id);
			$this->db->where('contentsplots.language', '2');
			$this->db->update('contentsplots', $sinopsis_ingles); 

		// Insertamos sinopsis en español
			$sinopsis_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'plot' => $data['sinopsis_español']
			);
			$this->db->where('contentsplots.content', $resultado->id);
			$this->db->where('contentsplots.language', '1');			
			$this->db->update('contentsplots', $sinopsis_español);

	}

	public function borrar_generos($content){

		$this->db->where('contentsgenres.content', $content);
		$this->db->delete('contentsgenres');
	}

	public function borrar_contenidos_relacionados($content){
		$this->db->where('recommendations.content', $content);
		$this->db->delete('recommendations');
	}

	public function comprobar_season($serie,$season_number){
		$this->db->select('*');
		$this->db->from('contentsseasons');
		$this->db->where('contentsseasons.series', $serie);
		$this->db->where('contentsseasons.number', $season_number);
		$consulta = $this->db->get();

		if ($consulta->num_rows() === 0){
			return FALSE;
			echo "esto es falso falso";
		} else {
			return $consulta->row();
			echo "esto es verdadero verdadero";
		}		

	}	

	public function comprobar_capitulo($season_number,$episode_number,$id_serie_propia){
		$this->db->select('*');
		$this->db->from('contentsepisodes');
		$this->db->where('contentsepisodes.number', $episode_number);
		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
		$this->db->where('contentsseasons.number', $season_number);
		$this->db->where('contentsseasons.series', $id_serie_propia);

		$consulta = $this->db->get();

		if ($consulta->num_rows() === 0){
			return FALSE;
		} else {
			return $consulta->row();
		}			
	}

	public function editar_episodio($data,$id_capitulo){
			$episodio = array(
               'date' => $data['date'],
               'rate' => $data['rate'],
            );
			$this->db->where('contentsepisodes.episode', $id_capitulo);
			$this->db->update('contentsepisodes', $episodio); 

	}

	public function obtener_season($serie,$season_number){
		$this->db->select('*');
		$this->db->from('contentsseasons');
		$this->db->where('contentsseasons.series', $serie);
		$this->db->where('contentsseasons.number', $season_number);
		$consulta = $this->db->get();
		return $consulta->row();
	}

		public function comprobar_si_existe($tmdb_id){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');
		$this->db->where('contents.tmdbid', $tmdb_id);
		$consulta = $this->db->get();

		if ($consulta->num_rows() === 0){
			return FALSE;
		} else {
			return $consulta->row();
		}


	}
/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA AÑADIR SERIES
| A partir de esta línea empiezan todas las funciones que están
| relacionadas añadir películas
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function lista_completa_series(){
		$this->db->select('contents.tmdbid as tmdbid, contents.id as id');
		$this->db->from('contentsseries');
		$this->db->join('contents', 'contents.id = contentsseries.series');
		$this->db->where('contentsseries.series >', '98686');


		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function todas_series_añadir($tmdbid){
			// Insertamos en tblcontents
			$serie_content = array(
  			 'tmdbid' => $tmdbid ,
			);
			$this->db->insert('contents', $serie_content); 
			$id_contenido = $this->db->insert_id();

			// Insertamos en tblcontentsfilms
			$serie_contentsseries = array(
  			'series' => $id_contenido ,
			);
			$this->db->insert('contentsseries', $serie_contentsseries);

			// Creamos carpeta
			mkdir($_SERVER['DOCUMENT_ROOT']."/asset/img/series/".$id_contenido, 0777, true);
	}

	public function añadir_serie($data){
		// Sacamos la id a la que pertenece la serie vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');
		$consulta = $this->db->get();
		$resultado = $consulta->row();
		

		// Insertamos los datos en contentsseries
			$contentsseries = array(
               'duration' => $data['runtime'],
               'rate' => $data['rate'],
               'year' => $data['fecha_estreno'],
               'status' => $data['estado'],
               'trailer' => $data['trailer'],
            );
			$this->db->where('series', $resultado->id);
			$this->db->update('contentsseries', $contentsseries); 



		// Insertamos título en inglés
			$titulo_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'title' => $data['titulo_ingles']
			);
			$this->db->replace('contentstitles', $titulo_ingles); 

		// Insertamos título en español
			$titulo_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'title' => $data['titulo_español']
			);
			$this->db->replace('contentstitles', $titulo_español); 

		// Insertamos sinopsis en inglés
			$sinopsis_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'plot' => $data['sinopsis_ingles']
			);
			$this->db->replace('contentsplots', $sinopsis_ingles); 

		// Insertamos sinopsis en español
			$sinopsis_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'plot' => $data['sinopsis_español']
			);
			$this->db->replace('contentsplots', $sinopsis_español); 

	}


	public function añadir_generos_serie($data){
		// Sacamos la id a la que pertenece la película vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');

		$consulta = $this->db->get();
		$resultado = $consulta->row();

		// Sacamos la id de nuestros generos
		$this->db->select('*');
		$this->db->from('genres');
		$this->db->where('genres.tmdbid', $data['genero']);
		$consulta_generos = $this->db->get();
		$resultado_generos = $consulta_generos->row();

		// Insertamos los generos
			$generos = array(
  			 'content' => $resultado->id ,
   			 'genre' => $resultado_generos->id
			);
			$this->db->replace('contentsgenres', $generos); 
	}

	public function añadir_relaciones_serie($data){
	// Sacamos la id a la que pertenece la película vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');
		
		$consulta = $this->db->get();
		$resultado = $consulta->row();

	
		// Insertamos los contenidos relacionados
			$relacionado = array(
  			 'content' => $resultado->id ,
   			 'related_content' => $data['contenido_relacionado'] ,
   			 'cont_rel' => $data['cont_rel']
			);
			$this->db->replace('recommendations', $relacionado); 
	}

	public function relacionados_id_propia($tmdbid){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $tmdbid);
		$this->db->join('contentsseries', 'contentsseries.series = contents.id');
		$consulta = $this->db->get();
		return $consulta->row();


	}

	public function añadir_season_serie($data){
			$seasons = array(
  			 'series' => $data['series'] ,
   			 'number' => $data['number'] ,
   			 'tmdbid' => $data['tmdbid'] ,
   			 'year'   => $data['year'] ,
			);
			$this->db->insert('contentsseasons', $seasons);

			$id_season_propia = $this->db->insert_id();
			return $id_season_propia;
	}

	public function añadir_episode_serie_content($data){
		$episodes = array(
  		 'tmdbid' => $data['tmdbid']
		);
		$this->db->insert('contents', $episodes);
		$id_episodio_propia = $this->db->insert_id();
		return $id_episodio_propia;
	}
	
	public function añadir_episode_serie_contentsepisodes($data){
		 $episodes=array(
		  'episode'=>$data['episode'],
		  'season'=>$data['season'],
		  'number'=>$data['number'],
		  'date'=>$data['date'],
		  'rate'=>$data['rate'],
		 );
		 $this->db->replace('contentsepisodes', $episodes);
	}

	public function añadir_episode_español($data){
		$idioma=array(
		'content'=>$data['content'],
		'language'=>$data['language'],
		'title'=>$data['title'],
		);
		$this->db->replace('contentstitles', $idioma);
	}

	public function añadir_episode_ingles($data){
		$idioma=array(
		'content'=>$data['content'],
		'language'=>$data['language'],
		'title'=>$data['title'],
		);
		$this->db->replace('contentstitles', $idioma);
	}	
}