<?php
class Content_sections_model extends CI_Model{
	public function __consturct(){

		parent:__construct();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
	}

	public function estrenos(){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}

		$fecha_hoy = date('Y-m-d H:i:s');
		$data = new DateTime('-31 day');
 		$fecha_10_dias = $data->format('Y-m-d');

		$this->db->select('*');
		$this->db->from('contentsfilms');
		$this->db->group_by('contentsfilms.id');
		$this->db->where('contentsfilms.year >=', $fecha_10_dias);
		$this->db->where('contentsfilms.year <=', $fecha_hoy);
		//$this->db->join('links', 'links.content = contentsfilms.id');
		$this->db->join('contentstitles', 'contentstitles.content = contentsfilms.id');
		$this->db->join('contentsplots', 'contentsplots.content = contentsfilms.id');
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('contentsplots.language', $idioma);
		$this->db->order_by('contentsfilms.year', 'DESC');
		$this->db->limit(30);

		$consulta = $this->db->get();
		return $consulta->result();

	}

	public function top_rated_films(){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}


		$this->db->select('*');
		$this->db->from('contentsfilms');
		$this->db->group_by('contentsfilms.id');
		$this->db->join('contentstitles', 'contentstitles.content = contentsfilms.id');
		$this->db->join('contentsplots', 'contentsplots.content = contentsfilms.id');
		$this->db->where('contentstitles.language', $idioma);
		$this->db->where('contentsplots.language', $idioma);
		$this->db->order_by('contentsfilms.rate', 'DESC');
		$this->db->limit(100);

		$consulta = $this->db->get();
		return $consulta->result();		
	}

}