<?php
class Comments_model extends CI_Model{
	public function __consturct(){
		parent:__construct();
	}
	

    function todos_comentarios_padres($id_serie){
$this->db->select('tbluserscomments.id, tbluserscomments.user, tbluserscomments.status, tbluserscomments.comment, tbluserscomments.date,, tbluserscomments.parent_comment, tblusers.username, tblusers.id_user, tbluserscomments.votes ');
        $this->db->from('tbluserscomments');
        $this->db->where('tbluserscomments.content',$id_serie);
        $this->db->where('tbluserscomments.parent_comment', '0');
        $this->db->join('tblusers', 'tblusers.id_user = tbluserscomments.user');
        $consulta =  $this->db->get();
        if ($consulta->num_rows() > 0){
        return $consulta->result();
        } else {
            return FALSE;
        }

    }

    function todos_comentarios_hijos($padre,$id_serie){
        $this->db->select('tbluserscomments.id, tbluserscomments.user, tbluserscomments.status, tbluserscomments.comment, tbluserscomments.date,, tbluserscomments.parent_comment, tblusers.username, tblusers.id_user, tbluserscomments.votes ');
        $this->db->from('tbluserscomments');
        $this->db->where('tbluserscomments.content',$id_serie);
        $this->db->where('tbluserscomments.parent_comment', $padre);
        $this->db->where('tbluserscomments.parent_comment >', '0');
        $this->db->join('tblusers', 'tblusers.id_user = tbluserscomments.user');
        $consulta =  $this->db->get();
        return $consulta->result();
    }    

	    // get full tree comments based on news id
    function lista_completa($id_serie) {



       	$this->db->select('*');
				$this->db->from('tbluserscomments');
			$this->db->where('tbluserscomments.content',$id_serie);
					$this->db->join('tblusers', 'tblusers.id_user = tbluserscomments.user');

		$result = $this->db->get()->result_array();;
$datos = array();
		       foreach ($result as $row) {

            $datos[] = $row;
       }
        return $datos;

    }

    // to get child comments by entry id and parent id and news id
    function lista_padre($id_serie,$in_parent) {

$this->db->select('tbluserscomments.id, tbluserscomments.user, tbluserscomments.status, tbluserscomments.comment, tbluserscomments.date,, tbluserscomments.parent_comment, tblusers.username, tblusers.id_user, tbluserscomments.votes ');
$this->db->from('tbluserscomments');
			$this->db->where('tbluserscomments.content',$id_serie);
			$this->db->where('tbluserscomments.parent_comment',$in_parent);
					$this->db->join('tblusers', 'tblusers.id_user = tbluserscomments.user');

					$result = $this->db->get();


		return $result->result();

    }

    // to insert comments
    function añadir_comentario($id_user)
    {

        $comment = str_replace ("\r\n", "<br>", $this->input->post('comment') );
    	$this->db->set("user", $id_user);
        $this->db->set("content", $this->input->post('content'));
        $this->db->set("comment", $comment);
        $this->db->set("date", date('Y-m-d H:i:s'));
        $this->db->set("parent_comment", $this->input->post('parent_comment'));

        $this->db->insert('userscomments');
        return $this->input->post('parent_comment');
    }

    // borrar comentario
     function borrar_comentario()
    {
                    $user = $this->ion_auth->user()->row()->id_user;
        $idmensaje = $this->input->post('idmensaje');
                $this->db->where('user', $user);
                $this->db->where('id', $idmensaje);
                $this->db->delete('userscomments');

                $this->db->where('parent_comment', $idmensaje);
                $this->db->delete('userscomments');


    }
    function votar_comentario(){
        $user = $this->ion_auth->user()->row()->id_user;
        $idmensaje = $this->input->post('idmensaje');
        $this->db->select('*');
        $this->db->from('userscommentsvotes');
        $this->db->where('comment', $idmensaje);
        $this->db->where('user', $user);
        $consulta = $this->db->get();

        if($consulta->num_rows() !== 0){ // Si hay un voto lo borramos
        $this->db->query("UPDATE tbluserscomments SET votes=votes-1 WHERE id = $idmensaje");
        $this->db->select('*');
        $this->db->from('userscommentsvotes');
        $this->db->where('comment', $idmensaje);
        $this->db->where('user', $user);
        $this->db->delete('userscommentsvotes');

        } else {  // Si no hay ningún voto lo añadimos
        $data = array(
        'user' => $user,
        'comment' => $idmensaje,
        'date' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('userscommentsvotes', $data);
        $this->db->query("UPDATE tbluserscomments SET votes=votes+1 WHERE id = $idmensaje");

        }
    }

    function ver_si_votado($idmensaje){
        $user = $this->ion_auth->user()->row()->id_user;

        $this->db->select('id');
        $this->db->from('userscommentsvotes');
        $this->db->where('comment', $idmensaje);
        $this->db->where('user', $user);
        $consulta = $this->db->get();
        return $consulta->row();


    }

        function reportar_comentario(){
        $idmensaje = $this->input->post('reporte_id_comment');    
        $motivo = $this->input->post('motivo');
        $observaciones = $this->input->post('observaciones');

        if ($this->ion_auth->logged_in()){
            $user = $this->ion_auth->user()->row()->id_user;
        } else {
            $user = $this->input->ip_address(); 
        }

        // Comprobamos si existe el mensaje reportado
        $this->db->select('*');
        $this->db->from('userscomments');
        $this->db->where('userscomments.id', $idmensaje);
        $consulta = $this->db->get();
        $count = $consulta->num_rows();

        $data = array(
            'user' => "$user",
            'comment' => "$idmensaje",
            'type' => "$motivo",
            'description' => "$observaciones",
            'date' => date('Y-m-d H:i:s'),
            );

        if ($count > 0){
            $this->db->select('comment');
            $this->db->from('userscommentsreports');
            $this->db->where('comment', $idmensaje);
            $this->db->where('user', $user);
            $consulta = $this->db->get();
            $count = $consulta->num_rows();


            $this->db->where('comment', $idmensaje);
            $this->db->where('user', $user);
            $this->db->delete('userscommentsreports');

          
            $this->db->insert('userscommentsreports', $data);
            $arraydata = array("resultado"  => "correcto");

        } elseif ($count === 0) {


            $this->db->insert('userscommentsreports', $data);
            $arraydata = array("resultado"  => "correcto");

        } else {

            $arraydata = array("resultado"  => "error");
 }

    return $arraydata;

}

    function ver_si_reportado($idmensaje){
        if ($this->ion_auth->logged_in())
        {
                    $user = $this->ion_auth->user()->row()->id_user;
                } else {
                    $user = $this->input->ip_address(); 
                }

        $this->db->select('comment');
        $this->db->from('userscommentsreports');
        $this->db->where('comment', $idmensaje);
        $consulta = $this->db->get();
     $count = $consulta->num_rows();
     return $count;


    }

    function tipos_reportes(){
        $this->db->select('*');
        $this->db->from('reports');
        $this->db->where('type', '2');
        $consulta = $this->db->get();
        return $consulta->result();
    }
}
