<?php
class Reclamar_enlaces_model extends CI_Model{
	public function __consturct(){

		parent:__construct();

	}


	public function añadir_reclamo(){
		$content = $this->input->post('content');
		$type = $this->input->post('type');
		$date = date('Y-m-d H:i:s');
        if ($this->ion_auth->logged_in())
        {
            $id_user = $this->ion_auth->user()->row()->id_user;
        	// Comprobamos que el content existe
        	$this->db->select('*');
        	$this->db->from('contents');
        	$this->db->where('contents.id', $content);
        	$this->db->limit(1);
        	$consulta = $this->db->get();
				// Comprobamos si el usuario ya ha hecho la misma reclamación para
        		if ($consulta->num_rows() > 0){
					$this->db->select('*');
					$this->db->from('linksrequests');
					$this->db->where('user', $id_user);
					$this->db->where('type', $type);
					$this->db->where('content', $content);
					$this->db->limit(1);
					$consulta = $this->db->get();

						// Si el usuario no había hecho esta reclamación previamente la añadimos
						if ($consulta->num_rows() === 0){
							$data = array(
					        'content' => $content,
					        'type' => $type,
					        'user' => $id_user,
					        'date' => $date
					        );
							$this->db->insert('linksrequests', $data);

						// Si el usuario ya había hecho esta reclamación hacemos un update de la fecha
						} else {
							$data = array(
					        'date' => $date,
					        );
							$this->db->where('user', $id_user);
							$this->db->where('type', $type);
							$this->db->where('content', $content);
							$this->db->update('linksrequests', $data); 
						}
					// Devolvemos TRUE si se registra la petición
					return TRUE;						
				} else {
					// Devolvemos FALSE si no existe el contenido
					return FALSE;
				}

		} else {
			// Devolvemos FALSE si el usuario no está registrado
			return FALSE;
		}
	}


}