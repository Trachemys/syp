<?php
class Series_model extends CI_Model{
	public function __consturct(){
		parent:__construct();
	}
	


	// Mostraremos los detalles de una serie seleccionado por ID
	public function detalle_series($id_serie){
		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}

		$this->db->select('*, status.name as status');
		$this->db->from('contentsseries');

		$this->db->join('contentstitles', 'contentstitles.content = contentsseries.series');
		$this->db->join('contentsplots', 'contentsplots.content = contentsseries.series');
		$this->db->join('status', 'status.status = contentsseries.status');

		$this->db->where('contentsseries.series',$id_serie);
		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsplots.language',$idioma);
		$this->db->limit(1);

		$consulta = $this->db->get();

		// Si no existen datos con el idioma del usuario forzamos el idioma a inglés
		if ($consulta->num_rows() === 0){
			$idioma = '2'; // 2 = Idioma forzado a inglés
		$this->db->select('*, status.name as status');
		$this->db->from('contentsseries');

		$this->db->join('contentstitles', 'contentstitles.content = contentsseries.series');
		$this->db->join('contentsplots', 'contentsplots.content = contentsseries.series');
		$this->db->join('status', 'status.status = contentsseries.status');

		$this->db->where('contentsseries.series',$id_serie);
		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsplots.language',$idioma);
		$this->db->limit(1);

		$consulta = $this->db->get();
		}
		
		return $consulta->row();
		

	}
	// Sacamos la info del capitulo que lo mostramos en home/episode/id/loquesea
		public function detalle_capitulos($id_capitulo){
					if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1';
		}

		$this->db->select('contentsepisodes.number AS numeroepisodio, contentsseasons.number AS numerotemporada, contentstitles.title AS tituloserie, contentsepisodes.date AS date, contentsepisodes.duration AS duration, contentsepisodes.episode AS id, contentsseasons.series AS series  ');
		$this->db->from('contentsepisodes');

		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season', 'left');
		$this->db->join('contentstitles', 'contentstitles.content = contentsseasons.series', 'left');

		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsepisodes.episode',$id_capitulo);
		$consulta = $this->db->get();

		// Si no existen datos con el idioma del usuario forzamos el idioma a inglés
		if ($consulta->num_rows() === 0){
			$idioma = '2'; // 2 = Idioma forzado a inglés
		$this->db->select('contentsepisodes.number AS numeroepisodio, contentsseasons.number AS numerotemporada, contentstitles.title AS tituloserie, contentsepisodes.date AS date, contentsepisodes.duration AS duration, contentsepisodes.episode AS id, contentsseasons.series AS series  ');
		$this->db->from('contentsepisodes');

		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season', 'left');
		$this->db->join('contentstitles', 'contentstitles.content = contentsseasons.series', 'left');

		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsepisodes.episode',$id_capitulo);
		$consulta = $this->db->get();			
		}
		return $consulta->row();

	}
	

		//Temporadas de la serie
		public function lista_temporadas_detalle($id_serie){

		$this->db->select('*');
		$this->db->from('contentsseasons');
		$this->db->join('contentsseries', 'contentsseries.series = contentsseasons.series');
		$this->db->where('contentsseries.series',$id_serie);
		$this->db->order_by('contentsseasons.number', 'asc');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function listar_capitulos(){
		$temporada = $this->input->post('idtemporada');
		$serie = $this->input->post('idcontenido');	

		if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1';
		}

		$this->db->select('contentsepisodes.episode, contentsepisodes.number, contentstitles.title, contentsseasons.number as seasonnumber, contentsseries.series as series, contentsepisodes.date as date, contentsseasons.year as seasonyear');
		$this->db->from('contentsepisodes');

		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
		$this->db->join('contentsseries', 'contentsseries.series = contentsseasons.series');
		$this->db->join('contentstitles', 'contentstitles.content = contentsepisodes.episode', 'left');
		
		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsseasons.id',$temporada);
		$this->db->where('contentsseries.series',$serie);
		$this->db->order_by('contentsepisodes.number', 'asc');

		$consulta = $this->db->get();

				// Si no existen datos con el idioma del usuario forzamos el idioma a inglés
		if ($consulta->num_rows() === 0){
			$idioma = '2'; // 2 = Idioma forzado a inglés
		$this->db->select('contentsepisodes.episode, contentsepisodes.number, contentstitles.title, contentsseasons.number as seasonnumber, contentsseasons.year as seasonyear');
		$this->db->from('contentsepisodes');

		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
		$this->db->join('contentsseries', 'contentsseries.series = contentsseasons.series');
		$this->db->join('contentstitles', 'contentstitles.content = contentsepisodes.episode', 'left');
		
		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsseasons.id',$temporada);
		$this->db->where('contentsseries.series',$serie);
		$this->db->order_by('contentsepisodes.number', 'asc');

		$consulta = $this->db->get();

		}
				return $consulta->result();

		}

		public function recomendaciones_series($content){
			$this->db->select('*');
			$this->db->from('recommendations');
			$this->db->where('content', $content);
			$consulta = $this->db->get();
			return $consulta->result();
		}

		}