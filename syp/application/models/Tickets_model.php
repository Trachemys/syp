<?php
class Tickets_model extends CI_Model{
	public function __consturct(){

		parent:__construct();

	}


	public function ver_tickets_abiertos($id_user){

		$this->db->select('tickets.id, ticketstypes.type');
		$this->db->from('tickets');
		$this->db->where('tickets.status', '1'); // 1 = abierto
		$this->db->where('tickets.user', $id_user);
		$this->db->join('ticketstypes', 'ticketstypes.id = tickets.type');
		$consulta = $this->db->get();
		return $consulta->result();

	}

	public function ver_tickets_pendientes($id_user){

		$this->db->select('tickets.id, ticketstypes.type');
		$this->db->from('tickets');
		$this->db->where('tickets.status', '2'); // 2 = pendiente de respuesta
		$this->db->where('tickets.user', $id_user);
		$this->db->join('ticketstypes', 'ticketstypes.id = tickets.type');

		$consulta = $this->db->get();
		return $consulta->result();

	}

	public function ver_tickets_cerrados($id_user){

		$this->db->select('tickets.id, ticketstypes.type');
		$this->db->from('tickets');
		$this->db->where('tickets.status', '3'); // 3 = cerrado
		$this->db->where('tickets.user', $id_user);
		$this->db->join('ticketstypes', 'ticketstypes.id = tickets.type');
		
		$consulta = $this->db->get();
		return $consulta->result();

	}

	public function perfil_ticket($id_ticket){
		$this->db->select('tickets.id, ticketsstatus.status, tickets.date, tickets.last_update, tickets.user');
		$this->db->from('tickets');
		$this->db->where('tickets.id', $id_ticket);
		$this->db->join('ticketsstatus', 'ticketsstatus.id = tickets.status');
		$consulta = $this->db->get();
		return $consulta->row();

	}	

		public function ver_mensajes($id_ticket){
		$this->db->select('*');
		$this->db->from('ticketscomments');
		$this->db->where('ticketscomments.ticket', $id_ticket);
		$this->db->join('users', 'users.id_user = ticketscomments.user');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function tipos_tickets(){
		$this->db->select('*');
		$this->db->from('ticketstypes');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function nuevo_ticket(){
		$tipo = $this->input->post('type');
		$mensaje = $this->input->post('message');
		$id_user = $this->ion_auth->user()->row()->id;

		$data = array(
   'user' => $id_user ,
   'type' => $tipo ,
   'status' => '1',
   'date' => date('Y-m-d H:i:s'),
   'last_update' => date('Y-m-d H:i:s'),   
);
$this->db->insert('tickets', $data); 
   $insert_id = $this->db->insert_id();
$data = array(
   'ticket' => $insert_id ,
   'user' => $id_user ,
   'comment' => $mensaje,
   'date' => date('Y-m-d H:i:s'),
);
$this->db->insert('ticketscomments', $data); 
	}


	public function comprobar_ticket_mensaje($id_ticket){
			$id_user = $this->ion_auth->user()->row()->id;
		$this->db->select('*');
		$this->db->from('tickets');
		$this->db->where('tickets.user', $id_user);
		$this->db->where('tickets.id', $id_ticket);
		$consulta = $this->db->get();
		return $consulta->row();

	}


	public function nuevo_mensaje(){
	$id_user = $this->ion_auth->user()->row()->id;
	$id_ticket = $this->input->post('id_ticket');
	$mensaje = $this->input->post('message');


	$data = array(
   'ticket' => $id_ticket ,
   'user' => $id_user ,
   'comment' => $mensaje,
   'date' => date('Y-m-d H:i:s'),
);
$this->db->insert('ticketscomments', $data); 	

	// Comprobamos si es el propio usuario el que responde el ticket para marcarlo como abierto
		$this->db->select('*');
		$this->db->from('tickets');
		$this->db->where('tickets.user', $id_user);
		$this->db->where('tickets.id', $id_ticket);
		$consulta = $this->db->get();
		$numeroresultado = $consulta->num_rows();
		if ($numeroresultado === 1){
$data = array(
               'status' => '1'
            );

$this->db->where('tickets.id', $id_ticket);
$this->db->update('tickets', $data); 	


		} else { // Si no es el propio usuario es un administrador, por lo que lo marcamos como pendiente de su respuesta

$data = array(
               'status' => '2'
            );

$this->db->where('tickets.id', $id_ticket);
$this->db->update('tickets', $data); 	

		}

	}
}