<?php
class Search_model extends CI_Model{
	public function __consturct(){

		parent:__construct();
		$this->load->helper(array('url','language'));
	}

  	// Funciones para la busqueda con autocompletado a través de JQuery

  	// Como vamos a sacar los títulos y títulos alternativos tenemos que quitar valores duplicados
	function unique_multidim_array($array, $key) { 
	    $temp_array = array(); 
	    $i = 0; 
	    $key_array = array(); 
	    
	    foreach($array as $val) { 
	        if (!in_array($val[$key], $key_array)) { 
	            $key_array[$i] = $val[$key]; 
	            $temp_array[$i] = $val; 
	        } 
	        $i++; 
	    } 
	    return $temp_array; 
	} 

 function get_contents($q){
 	// Series
    $this->db->select('*');
    $this->db->like('title', $q);
    $this->db->join('contentsseries', 'contentsseries.series = contentstitles.content', 'right');
    $this->db->group_by('contentstitles.content'); 
    $this->db->order_by('contentsseries.year','DESC');
    $this->db->limit(20);
    $query1_series = $this->db->get('contentstitles')->result_array();

    // Pelis titulos
    $this->db->select('*');
    $this->db->like('contentstitles.title', $q);
    $this->db->join('contentsfilms', 'contentsfilms.id = contentstitles.content');
    $this->db->group_by('contentstitles.content'); 
    $this->db->order_by('contentsfilms.year','DESC');
    $this->db->limit(20);
    $query1_films = $this->db->get('contentstitles')->result_array();

    // Pelis títulos alternativos
    $this->db->select('*');
    $this->db->like('contentstitlesalternatives.title', $q);
    $this->db->join('contentsfilms', 'contentsfilms.id = contentstitlesalternatives.content');
    $this->db->group_by('contentstitlesalternatives.content'); 
    $this->db->order_by('contentsfilms.year','DESC');
	$this->db->limit(8); 
    $query2_films = $this->db->get('contentstitlesalternatives')->result_array();

$query_total_films = array_merge($query1_films,$query2_films);
$array_films = $this->unique_multidim_array($query_total_films,'content'); 
$array = array_merge($query1_series,$array_films);
    
      foreach ($array as $row){	
        $new_row['label']=htmlentities(stripslashes($row['title']));
        // Generamos el link, hacemos una consulta para saber si es una serie o no para establecer fimls/ o series/
        $this->db->select('series');
        $this->db->from('contentsseries');
		$this->db->where('contentsseries.series',$row['content']);
		$consulta = $this->db->get();
			$num = $consulta->num_rows();
				if($num === 0){
					$url = base_url().'films/'.$row['content']. '/'. stripslashes($row['title']);
					$img = base_url().'asset/img/peliculas/'.$row['content'].'/c.jpg';
					$tipo = 'Película';
					$año = substr($row['year'],0,4);
					$header_imagen = get_headers($img);
					if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
							$img = base_url().'asset/img/layout/caratula.jpg';
					}

				} else {
					$url = base_url().'series/'.$row['content']. '/'. stripslashes($row['title']);
					$img = base_url(). 'asset/img/series/'.$row['content'] .'/c.jpg';
					$tipo = 'Serie';
					$año = $row['year'];
					$header_imagen = get_headers($img);
					if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
							$img = base_url().'asset/img/layout/caratula.jpg';
					}					

				}

        $new_row['url']=htmlentities(stripslashes($url)); // Establecemos la url
        $new_row['image']=htmlentities(stripslashes($img));
        $new_row['type']=$tipo;
        $new_row['año']=$año;


        $row_set[] = $new_row;
      }
      echo json_encode($row_set); 
    
  }




  	// Funciones para la busqueda con el formulario
  function get_contents_form(){


  	$q = $this->input->get('search');


    $string = $this->db->query("SELECT *, MATCH(title) AGAINST ('$q') AS score FROM tblcontentstitles WHERE MATCH(title) AGAINST('$q') GROUP BY(content) ORDER BY score DESC ");
    $query = $string->result_array();

	return $query;
  }
  // Para pasar al controlador el tipo de contenido, si película o serie
  function tipo_contenido($id){
  	 	$this->db->select('series');
        $this->db->from('contentsseries');
		$this->db->where('contentsseries.series',$id);
		$consulta = $this->db->get();
			$num = $consulta->num_rows();
				if($num != 0){
					$tipo = "series";
					return $tipo;
				} else {
		 $this->db->select('id');
        $this->db->from('contentsfilms');
		$this->db->where('contentsfilms.id',$id);
		$consulta = $this->db->get();
		$num = $consulta->num_rows();
				if($num != 0){
					$tipo = "films";
					return $tipo;
				} else {
					$tipo = "episode";
					return $tipo;

				}
				}
  }

  function buscar_serie_capitulo($id){
  		$this->db->select('contentstitles.title as title, contentstitles.content as series, contentsepisodes.number as episode_number, contentsseasons.number as season_number');
  		$this->db->from('contentsepisodes');
		$this->db->join('contentsseasons', 'contentsseasons.id = contentsepisodes.season');
		$this->db->join('contentstitles', 'contentstitles.content = contentsseasons.series');
		$this->db->where('contentsepisodes.episode',$id);
				$consulta = $this->db->get();
				return $consulta->row();



  }


}

