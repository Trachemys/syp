<?php
class Add_movies_model extends CI_Model{
	public function __consturct(){

		parent:__construct();

	}

	public function get_queue(){
		$this->db->select('changesqueue.content as id');
		$this->db->from('changesqueue');
		$this->db->join('contentsfilms', 'contentsfilms.id = changesqueue.content');
		$consulta = $this->db->get();
		return $consulta->result();
	}

	public function delete_queue($content){
		$this->db->delete('changesqueue', array('content' => $content)); 

	}

/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| 
| AÑADIR PELÍCULAS A LA QUEUE DE CAMBIOS
| 
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function add_queue($content){
		$data = array('content' => $content);
		$this->db->insert('changesqueue', $data); 
	}

	public function add_trending($tmdbid){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$this->db->where('contents.tmdbid', $tmdbid);
		$this->db->limit(1);
		$consulta = $this->db->get();

		$content = $consulta->row()->id;

		$array = array('content' => $content);
		$this->db->insert('trendings', $array); 		
	}
	public function delete_trendings(){
		$this->db->where('trendings.content >', '0');
		$this->db->delete('trendings');
	}
/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| 
| OBTENER INFO DE LA PELÍCULA POR LA ID
| 
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function get_info_content($content){
		$this->db->select('contents.tmdbid');
		$this->db->from('contents');
		$this->db->where('id', $content);
		$this->db->limit(1);
		$consulta = $this->db->get();
		return $consulta->row();

	}

	public function get_imdb($content){
		$this->db->select('contents.imdbId as imdbid');
		$this->db->from('contents');
		$this->db->where('contents.id', $content);
		$consulta = $this->db->get();
		return $consulta->row();
	}

	public function update_rating($content, $imdbrating){
		$data = array('rate' => $imdbrating);
		$this->db->where('id', $content);
		$this->db->update('contentsfilms', $data); 
		echo 'ID'.$content;
	}

/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| 
| OBTENER LISTA COMPLETA DE LAS PELÍCULAS
| 
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
		public function lista_completa_peliculas(){
		$this->db->select('contents.tmdbid as tmdbid, contents.id as id');
		$this->db->from('contentsfilms');
		$this->db->join('contents', 'contents.id = contentsfilms.id');
		//$this->db->where('contents.id >', '54178');
		$consulta = $this->db->get();
		return $consulta->result();
	}


/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA EDITAR PELÍCULAS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con la edición de películas
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function editar_pelicula($data){
// Sacamos la id a la que pertenece la película vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$consulta = $this->db->get();
		$resultado = $consulta->row();
		

		// Insertamos los datos en contentsfilms
			$contentsfilms = array(
               'duration' => $data['runtime'],
               'rate' => $data['rate'],
               'popularity' => $data['popularity'],
               'year' => $data['fecha_estreno'],
               'status' => $data['estado'],
               'trailer' => $data['trailer'],
            );
			$this->db->where('id', $resultado->id);
			$this->db->update('contentsfilms', $contentsfilms); 

		// Insertamos la id de imdb en contents
			$contents = array(
               'imdbid' => $data['imdbid'],
            );
			$this->db->where('id', $resultado->id);
			$this->db->update('contents', $contents); 

		// Insertamos título en inglés
			$titulo_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'title' => $data['titulo_ingles']
			);
			$this->db->where('contentstitles.language', '2');
			$this->db->where('contentstitles.content', $resultado->id);
			$this->db->replace('contentstitles', $titulo_ingles); 

		// Insertamos título en español
			$titulo_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'title' => $data['titulo_español']
			);
			$this->db->where('contentstitles.language', '1');
			$this->db->where('contentstitles.content', $resultado->id);			
			$this->db->replace('contentstitles', $titulo_español); 

		// Insertamos sinopsis en inglés
			$sinopsis_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'plot' => $data['sinopsis_ingles']
			);
			$this->db->where('contentsplots.content', $resultado->id);
			$this->db->where('contentsplots.language', '2');
			$this->db->replace('contentsplots', $sinopsis_ingles); 

		// Insertamos sinopsis en español
			$sinopsis_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'plot' => $data['sinopsis_español']
			);
			$this->db->where('contentsplots.content', $resultado->id);
			$this->db->where('contentsplots.language', '1');			
			$this->db->replace('contentsplots', $sinopsis_español); 		
	}



	public function borrar_generos($content){

		$this->db->where('contentsgenres.content', $content);
		$this->db->delete('contentsgenres');
	}

	public function borrar_contenidos_relacionados($content){
		$this->db->where('recommendations.content', $content);
		$this->db->delete('recommendations');
	}

	public function comprobar_si_existe($tmdb_id){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$this->db->where('contents.tmdbid', $tmdb_id);
		$consulta = $this->db->get();

		if ($consulta->num_rows() === 0){
			return FALSE;
		} else {
			return $consulta->row();
		}


	}

	public function comprobar_si_existe_pelicula($tmdbid){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$this->db->where('contents.tmdbid', $tmdbid);
		$consulta = $this->db->get();

		if ($consulta->num_rows() === 0){
			return FALSE;
		} else {
			return TRUE;
		}

	}

		public function comprobar_si_existe_pelicula_borrar($tmdbid){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$this->db->where('contents.tmdbid', $tmdbid);
		$consulta = $this->db->get();

		return $consulta->num_rows();

	}

		public function relacionados_id_propia($tmdbid){
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $tmdbid);
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$consulta = $this->db->get();
		return $consulta->row();


	}
/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA AÑADIR PELÍCULAS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas añadir películas
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function añadir_pelicula($data){
		// Sacamos la id a la que pertenece la película vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		$consulta = $this->db->get();
		$resultado = $consulta->row();
		

		// Insertamos los datos en contentsfilms
			$contentsfilms = array(
               'duration' => $data['runtime'],
               'rate' => $data['rate'],
               'popularity' => $data['popularity'],               
               'year' => $data['fecha_estreno'],
               'status' => $data['estado'],
               'trailer' => $data['trailer'],
            );
			$this->db->where('id', $resultado->id);
			$this->db->update('contentsfilms', $contentsfilms); 

		// Insertamos la id de imdb en contents
			$contents = array(
               'imdbid' => $data['imdbid'],
            );
			$this->db->where('id', $resultado->id);
			$this->db->update('contents', $contents); 

		// Insertamos título en inglés
			$titulo_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'title' => $data['titulo_ingles']
			);
			$this->db->replace('contentstitles', $titulo_ingles); 

		// Insertamos título en español
			$titulo_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'title' => $data['titulo_español']
			);
			$this->db->replace('contentstitles', $titulo_español); 

		// Insertamos sinopsis en inglés
			$sinopsis_ingles = array(
  			 'content' => $resultado->id ,
   			 'language' => '2' ,
   			 'plot' => $data['sinopsis_ingles']
			);
			$this->db->replace('contentsplots', $sinopsis_ingles); 

		// Insertamos sinopsis en español
			$sinopsis_español = array(
  			 'content' => $resultado->id ,
   			 'language' => '1' ,
   			 'plot' => $data['sinopsis_español']
			);
			$this->db->replace('contentsplots', $sinopsis_español); 

	}

	public function añadir_generos_pelicula($data){
		// Sacamos la id a la que pertenece la película vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');

		$consulta = $this->db->get();
		$resultado = $consulta->row();

		// Sacamos la id de nuestros generos
		$this->db->select('*');
		$this->db->from('genres');
		$this->db->where('genres.tmdbid', $data['genero']);
		$consulta_generos = $this->db->get();
		$resultado_generos = $consulta_generos->row();

		// Insertamos los generos
			$generos = array(
  			 'content' => $resultado->id ,
   			 'genre' => $resultado_generos->id
			);
			$this->db->insert('contentsgenres', $generos); 
	}

	public function añadir_relaciones_pelicula($data){
	// Sacamos la id a la que pertenece la película vía ID TMDB
		$this->db->select('*');
		$this->db->from('contents');
		$this->db->where('contents.tmdbid', $data['tmdbid']);
		$this->db->join('contentsfilms', 'contentsfilms.id = contents.id');
		
		$consulta = $this->db->get();
		$resultado = $consulta->row();

	
		// Insertamos los contenidos relacionados
			$relacionado = array(
  			 'content' => $resultado->id ,
   			 'related_content' => $data['contenido_relacionado']
			);
			$this->db->insert('recommendations', $relacionado); 
	}

	public function todas_peliculas_añadir($tmdbid){
			// Insertamos en tblcontents
			$pelicula_content = array(
  			 'tmdbid' => $tmdbid ,
			);
			$this->db->insert('contents', $pelicula_content); 
			$id_contenido = $this->db->insert_id();

			// Insertamos en tblcontentsfilms
			$pelicula_contentsfilms = array(
  			'id' => $id_contenido ,
			);
			$this->db->insert('contentsfilms', $pelicula_contentsfilms);

			// Creamos carpeta
			mkdir($_SERVER['DOCUMENT_ROOT']."/asset/img/peliculas/".$id_contenido, 0777, true);

			return $id_contenido;

	}

	public function add_alternatives($content,$language,$title){
			$alternatives = array(
  			'content' => $content ,
  			'language' => $language,
  			'title' => $title,
			);
			$this->db->insert('contentstitlesalternatives', $alternatives);		
	}	

	public function add_films_queue($id){
			$data = array(
  			'content' => $id ,
			);
			$this->db->insert('changesqueue', $data);	

	}



}