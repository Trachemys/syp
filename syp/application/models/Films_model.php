<?php
class Films_model extends CI_Model{
	public function __consturct(){
		parent:__construct();
	}
	


	// Mostraremos los detalles de una serie seleccionado por ID
	public function detalle_peliculas($id_peli){
				if ($this->ion_auth->logged_in())
		{
			$idioma = $this->ion_auth->user()->row()->language;
		} else {
			$idioma = '1'; // El idioma por defecto para los invitados será el español
		}

				$this->db->select('*');
				$this->db->from('contentsfilms');
		$this->db->join('contentstitles', 'contentstitles.content = contentsfilms.id');
		$this->db->join('contentsplots', 'contentsplots.content = contentsfilms.id');
		$this->db->where('contentsfilms.id',$id_peli);
		$this->db->where('contentstitles.language',$idioma);
		$this->db->where('contentsplots.language',$idioma);
		$consulta = $this->db->get();
		return $consulta->row();
		

	}

/*
		//Temporadas de la serie
		public function lista_temporadas_detalle($id_serie){

		$this->db->select('*');
		$this->db->from('contentsseasons');
		$this->db->join('contentsseries', 'contentsseries.id_content = contentsseasons.id_media');
		$this->db->where('contentsseries.id',$id_serie);
		$consulta = $this->db->get();
		return $consulta->result();
	}
		
		//Capitulos de la serie
		public function lista_capitulos_detalle($id_serie){

		$this->db->select('*');
		$this->db->from('contentsepisodes');
		$this->db->join('contentsseries', 'contentsseries.id_content = contentsepisodes.id_media');
		$this->db->where('contentsepisodes.id_media',$id_serie);
		$consulta = $this->db->get();
		return $consulta->result();
	}
	


		//**Mostraremos a qué generos pertenece cada vídeo en su perfil
		public function lista_generos_detalle($id_serie){

		$this->db->select('*');
		$this->db->from('genres');
		$this->db->join('contentsgenres', 'contentsgenres.id_genre = genres.id');
		$this->db->join('contentsseries', 'contentsseries.id_content = contentsgenres.id_media');
		$this->db->where('contentsseries.id',$id_serie);
		$consulta = $this->db->get();
		return $consulta->result(); 
	}

*/
		}