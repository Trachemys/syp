<?php
class Corners_model extends CI_Model{
	public function __consturct(){

		parent:__construct();

	}


	public function corner_favorito($content){
		$user = $this->ion_auth->user()->row()->id;	
			
		$this->db->select('content');
		$this->db->from('usersfavourites');
		$this->db->where('usersfavourites.content', $content);
		$this->db->where('usersfavourites.user', $user);
		$this->db->limit(1);
		$consulta = $this->db->get();
		if ($consulta->num_rows() === 0){
			return false;
		} else {
			return true;
		}
	}

	public function corner_pendiente($content){
		$user = $this->ion_auth->user()->row()->id;	
			
		$this->db->select('content');
		$this->db->from('userspending');
		$this->db->where('userspending.content', $content);
		$this->db->where('userspending.user', $user);
		$this->db->limit(1);
		$consulta = $this->db->get();
		if ($consulta->num_rows() === 0){
			return false;
		} else {
			return true;
		}
	}

	public function corner_vista($content){
		$user = $this->ion_auth->user()->row()->id;	
			
		$this->db->select('content');
		$this->db->from('usersviewed');
		$this->db->where('usersviewed.content', $content);
		$this->db->where('usersviewed.user', $user);
		$this->db->limit(1);
		$consulta = $this->db->get();
		if ($consulta->num_rows() === 0){
			return false;
		} else {
			return true;
		}
	}

	public function corner_siguiendo($content){
		$user = $this->ion_auth->user()->row()->id;	
			
		$this->db->select('content');
		$this->db->from('usersfollowing');
		$this->db->where('usersfollowing.content', $content);
		$this->db->where('usersfollowing.user', $user);
		$this->db->limit(1);
		$consulta = $this->db->get();
		if ($consulta->num_rows() === 0){
			return false;
		} else {
			return true;
		}
	}

}