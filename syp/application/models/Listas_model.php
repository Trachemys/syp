<?php
class Listas_model extends CI_Model{
	public function __consturct(){

		parent:__construct();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
	}


		public function todas_listas_perfil(){
			if($this->ion_auth->logged_in())
  {
		$user = $this->ion_auth->user()->row()->id_user;
}

		$this->db->select('*');
		$this->db->from('userslists');
		$this->db->join('users', 'users.id_user = userslists.user');
		$this->db->where('users.id_user',$user);
		$consulta = $this->db->get();
		return $consulta->result();
}

    function ver_si_añadido($idlista){
        $user = $this->ion_auth->user()->row()->id_user;
        $idcontent = $this->uri->segment(2);
        $this->db->select('userslists.list');
        $this->db->from('userslists');
        $this->db->where('user', $user);
        $this->db->where('userslists.list', $idlista);
        $this->db->join('userslistscontents', 'userslistscontents.list = userslists.list');
        $this->db->where('userslistscontents.content', $idcontent);

        $consulta = $this->db->get();
        return $consulta->row();


    }

    function borrar_contenido_lista(){
    	$listaid = $this->input->post('listaid');
    	$idcontenido = $this->input->post('idcontenido');
    	$this->db->where('list', $listaid);
    	$this->db->where('content', $idcontenido);
    	$this->db->delete('userslistscontents');


    }

        function añadir_contenido_lista(){
    	$listaid = $this->input->post('listaid');
    	$idcontenido = $this->input->post('idcontenido');
    	$data = array(
        'list' => $listaid,
        'content' => $idcontenido,
);
			$this->db->insert('userslistscontents', $data);



    }
    function ver_total_listas_usuario(){
    	$user = $this->ion_auth->user()->row()->id_user;
    $this->db->select('list');
    $this->db->from('tbluserslists');
    $this->db->where('user', $user);
$query = $this->db->get();
$numerolistas = $query->num_rows();
return $numerolistas;

    }

    function crear_lista(){
    $user = $this->ion_auth->user()->row()->id_user;
	$nombre = $this->input->post('nombre');
	$descripcion = $this->input->post('descripcion');
	$privacidad = $this->input->post('privacidad');
	$fechahoy = date('Y-m-d H:i:s');
$data = array(
   'name' => $nombre ,
   'description' => $descripcion ,
   'state' => $privacidad ,
   'user' => $user ,   
   'date' => $fechahoy ,
);
			$this->db->insert('userslists', $data);

    }

        public function perfil_listas($idlista){
                $this->db->select('*');
                $this->db->from('userslists');
                $this->db->where('userslists.list',$idlista);
        $consulta = $this->db->get();
        return $consulta->row();

        

    }

    public function contenido_listas_series(){
                if ($this->ion_auth->logged_in())
        {
            $idioma = $this->ion_auth->user()->row()->language;
        } else {
            $idioma = '1'; // El idioma por defecto para los invitados será el español
        }
        $idlista = $this->uri->segment(2);
                $this->db->select('*');
                $this->db->from('tbluserslistscontents');
                $this->db->where('tbluserslistscontents.list',$idlista);
                $this->db->where('contentstitles.language',$idioma);

        $this->db->join('tblcontentsseries', 'tblcontentsseries.series = tbluserslistscontents.content');
        $this->db->join('contentstitles', 'contentstitles.content = contentsseries.series');
        
        $consulta = $this->db->get();
      
        return $consulta->result();
    }

     public function contenido_listas_peliculas(){
                        if ($this->ion_auth->logged_in())
        {
            $idioma = $this->ion_auth->user()->row()->language;
        } else {
            $idioma = '1'; // El idioma por defecto para los invitados será el español
        }
        $idlista = $this->uri->segment(2);
                 $this->db->select('*');
                $this->db->from('tbluserslistscontents');
                $this->db->where('tbluserslistscontents.list',$idlista);
                $this->db->where('contentstitles.language',$idioma);
        $this->db->join('tblcontentsfilms', 'tblcontentsfilms.id = tbluserslistscontents.content');
        $this->db->join('contentstitles', 'contentstitles.content = contentsfilms.id');
        
        $consulta = $this->db->get();
        return $consulta->result();
    }


    function editar_lista(){
    $user = $this->ion_auth->user()->row()->id_user;
    $lista = $this->input->post('idlista');
    $nombre = $this->input->post('nombre');
    $descripcion = $this->input->post('descripcion');
    $privacidad = $this->input->post('privacidad');

    // Comprobamos si la lista pertenece al usuario
    $this->db->select('*');
    $this->db->from('userslists');
    $this->db->where('list', $lista);
    $this->db->where('user', $user );
    $consulta = $this->db->get();
    if ($consulta->num_rows() === 0){
        $res = 'errortexto';
    } else {

    $data = array(
       'name' => $nombre ,
       'description' => $descripcion ,
       'state' => $privacidad ,

    );
    $this->db->where('list', $lista);
    $this->db->update('tbluserslists', $data); 
    $res = 'correcto';
    }
    return $res;
    }

     function seguir_lista(){
        $user = $this->ion_auth->user()->row()->id_user;
        $lista = $this->input->post('lista');
        $this->db->select('*');
        $this->db->from('userslistsfollow');
        $this->db->where('list', $lista);
        $this->db->where('user', $user);
        $consulta = $this->db->get();

        if($consulta->num_rows() !== 0){ // Si hay un follow del usuario lo borramos
        $this->db->query("UPDATE tbluserslists SET follow=follow-1 WHERE list = $lista");
        $this->db->select('*');
        $this->db->from('userslistsfollow');
        $this->db->where('list', $lista);
        $this->db->where('user', $user);
        $this->db->delete('userslistsfollow');

        } else {  // Si no hay ningún follow del usuario lo añadimos lo añadimos
        $data = array(
        'user' => $user,
        'list' => $lista,
        'date' => date('Y-m-d H:i:s'),
        );
        $this->db->insert('userslistsfollow', $data);
        $this->db->query("UPDATE tbluserslists SET follow=follow+1 WHERE list = $lista");

        }
    }

    function ver_si_seguido($lista){
        $user = $this->ion_auth->user()->row()->id_user;

        $this->db->select('list');
        $this->db->from('userslistsfollow');
        $this->db->where('list', $lista);
        $this->db->where('user', $user);
        $consulta = $this->db->get();
        return $consulta->row();


    }

    public function todas_listas_orden_seguir(){
if($this->input->post('submit') === 'Seguidores'){
    echo "seguidores";
} 

        $this->db->select('*');
        $this->db->from('userslists');
        $this->db->where('private', '0');
        $this->db->order_by("follow", "desc");
        $consulta = $this->db->get();

        return $consulta->result();
    }

    public function todas_listas_orden_fecha(){
        $this->db->select('*');
        $this->db->from('userslists');
        $this->db->where('private', '0');
        $this->db->order_by("date", "desc");
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function todas_listas_orden_contenido(){


    }

    public function background($contenido_random){
        $this->db->select('*');
        $this->db->from('contentsfilms');
        $this->db->where('contentsfilms.id', $contenido_random);
        $consulta = $this->db->get();
        $count = $consulta->num_rows();
        
        if ($count === 1){
            $url = base_url(). 'asset/img/peliculas/'.$contenido_random.'/b.jpg';
            return $url;
        } else {

        $this->db->select('*');
        $this->db->from('contentsseries');
        $this->db->where('contentsseries.series', $contenido_random);
        $consulta = $this->db->get();
        $count = $consulta->num_rows();

        if($count === 1){
            $url = base_url(). 'asset/img/series/'.$contenido_random.'/b.jpg';
            return $url;           
        } else {
            $this->load->library('background_image');
return $this->background_image->background_image();
        }
        }


    }
}