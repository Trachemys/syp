<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth','form_validation'));
		$this->load->helper(array('url','language'));
		$this->load->library('background_image');

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
	}

	// redirect if needed, otherwise display the user list
	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->is_admin()) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}
		else
		{
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->_render_page('auth/index', $this->data);
		}
	}

	// Mostrar formulario de login si el usuario no está logeado
	public function login()
	{
		$this->data['background'] = $this->background_image->select_background();

		
		if (!$this->ion_auth->logged_in())
{


			$this->data['identity'] = array('name' => 'identity',
				'id'    => 'identity',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$this->data['password'] = array('name' => 'password',
				'id'   => 'password',
				'type' => 'password',
			);
			
			$this->load->view('users/login', $this->data);
		} else{

							     redirect('profile', 'refresh');

		}
	}
	// Validación del formulario + ajax
public function login_ajax(){

if($this->input->is_ajax_request()){
$remember = (bool) $this->input->post('remember');
$login = $this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember);
// Form validations
$this->form_validation->set_rules('identity', 'Identity',  'trim|required|min_length[3]|max_length[50]|xss_clean');
$this->form_validation->set_rules('password', 'Password',  'trim|required|min_length[6]|max_length[100]|xss_clean');

	if($this->form_validation->run() === FALSE){
		$data = array(
			"identity"   => form_error("identity"),
			"password"   => form_error("password"),
			"resultado"  => "error"
			);
	}
	if($this->form_validation->run() === TRUE)
	{
		
			if ($login) // Probamos con el user como identity
			{
		$data = array(
			"resultado"  => "adelante"
			);			
				
				
					         } else {
					         			$data = array(
					         		"noexiste"   => "El usuario y contraseña no coinciden",
									"resultado"  => "error"
													);
					         }

		
	}

 echo json_encode($data);
} 

}
public function registro(){
	$this->data['background'] = $this->background_image->background_image();

	if ($this->ion_auth->logged_in())
		{
			redirect('profile');
		}
				$this->load->view('users/registro', $this->data);

}
  public function registro_ajax(){
	
if($this->input->is_ajax_request()){
        $this->form_validation->set_rules('username','Usuario','trim|min_length[3]|max_length[20]|required|is_unique[users.username]|alpha_numeric');
        $this->form_validation->set_rules('email','Email','trim|valid_email|required|is_unique[users.email]');
        $this->form_validation->set_rules('password','Contraseña','trim|min_length[6]|max_length[20]|required');
        $this->form_validation->set_rules('confirm_password','Confirmar contraseña','trim|matches[password]|required');

        if($this->input->post('codigo') !== ''){
        $this->form_validation->set_rules('codigo','Código','callback_comprobar_codigo_existencia[' . $this->input->post('codigo') . ']');
     	}
		     if ($this->form_validation->run() === FALSE)
                {
                          		$dataregistro = array(
			"username"   => form_error("username"),
			"email"   => form_error("email"),
			"password"   => form_error("password"),
			"confirm_password"   => form_error("confirm_password"),
			"codigo"   => 'El código ha expirado o no existe',

			"resultado"  => "error"

			);
 			echo json_encode($dataregistro);

                }
if($this->form_validation->run() === TRUE)
{
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $codigo = $this->input->post('codigo');

            if($this->ion_auth_model->comprobar_codigo_registro($codigo) === FALSE){
            	
            }

            if($this->ion_auth->register($username,$password,$email,$codigo))
            {
         $dataregistro = array(

			"mensaje"   => 'Tu usuario se ha registrado correctamente, ya solo te queda activarlo. <b>Para activar tu usuario debes acceder link de activación</b> que hemos enviado a tu dirección de correo '.$email. ' (AÚN NO ESTÁ CONFIGURADO PARA RECIBIR EL EMAIL, LO ACTIVAREMOS A MANO EN LA DB',

			"resultado"  => "correcto"

			);
          			echo json_encode($dataregistro);
   			                      	
            }

}

                        
        }
}

public function comprobar_codigo_existencia($codigo){
	$resultado = $this->ion_auth_model->comprobar_codigo_registro($codigo);
	if($resultado === 'FALSE'){
		$this->form_validation->set_message('comprobar_codigo_existencia', 'El {field} no existe o ha expirado.');
		return FALSE;
        } else {
        	return TRUE;
        }
	}



public function registro_completar(){
$this->load->model('Ion_auth_model');
					$this->Ion_auth_model->registro_completar();
}
	// log the user out
	public function logout()
	{
		$this->data['title'] = "Logout";

		// log the user out
		$logout = $this->ion_auth->logout();

		// redirect them to the login page
		redirect('home', 'refresh');
	}



	// change password
	public function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			// display the form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
			);
			$this->data['new_password'] = array(
				'name'    => 'new',
				'id'      => 'new',
				'type'    => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['new_password_confirm'] = array(
				'name'    => 'new_confirm',
				'id'      => 'new_confirm',
				'type'    => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			// render
			$this->_render_page('auth/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	// forgot password
	public function forgot_password()
	{
		// setting validation rules by checking whether identity is username or email
		if($this->config->item('identity', 'ion_auth') != 'email' )
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() == false)
		{
			$this->data['type'] = $this->config->item('identity','ion_auth');
			// setup the input
			$this->data['identity'] = array('name' => 'identity',
				'id' => 'identity',
			);

			if ( $this->config->item('identity', 'ion_auth') != 'email' ){
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth/forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity','ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if(empty($identity)) {

	            		if($this->config->item('identity', 'ion_auth') != 'email')
		            	{
		            		$this->ion_auth->set_error('forgot_password_identity_not_found');
		            	}
		            	else
		            	{
		            	   $this->ion_auth->set_error('forgot_password_email_not_found');
		            	}

		                $this->session->set_flashdata('message', $this->ion_auth->errors());
                		redirect("auth/forgot_password", 'refresh');
            		}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	// reset password - final step for forgotten password
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() == false)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['new_password_confirm'] = array(
					'name'    => 'new_confirm',
					'id'      => 'new_confirm',
					'type'    => 'password',
					'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				);
				$this->data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->_render_page('auth/reset_password', $this->data);
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}


	// activate the user
	public function activate($id, $code=false)
	{
		if ($code !== false)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	// deactivate the user
	public function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			return show_error('You must be an administrator to view this page.');
		}

		$id = (int) $id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() == FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth/deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	// create a new user
	public function create_user()
    {
        $this->data['title'] = $this->lang->line('create_user_heading');

        if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
        {
            redirect('auth', 'refresh');
        }

        $tables = $this->config->item('tables','ion_auth');
        $identity_column = $this->config->item('identity','ion_auth');
        $this->data['identity_column'] = $identity_column;

        // validate form input
        $this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'required');
        $this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'required');
        if($identity_column!=='email')
        {
            $this->form_validation->set_rules('identity',$this->lang->line('create_user_validation_identity_label'),'required|is_unique['.$tables['users'].'.'.$identity_column.']');
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email');
        }
        else
        {
            $this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        }
        $this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_phone_label'), 'trim');
        $this->form_validation->set_rules('company', $this->lang->line('create_user_validation_company_label'), 'trim');
        $this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

        if ($this->form_validation->run() == true)
        {
            $email    = strtolower($this->input->post('email'));
            $identity = ($identity_column==='email') ? $email : $this->input->post('identity');
            $password = $this->input->post('password');

            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name'  => $this->input->post('last_name'),
                'company'    => $this->input->post('company'),
                'phone'      => $this->input->post('phone'),
            );
        }
        if ($this->form_validation->run() == true && $this->ion_auth->register($identity, $password, $email, $additional_data))
        {
            // check to see if we are creating the user
            // redirect them back to the admin page
            $this->session->set_flashdata('message', $this->ion_auth->messages());
            redirect("auth", 'refresh');
        }
        else
        {
            // display the create user form
            // set the flash data error message if there is one
            $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

            $this->data['first_name'] = array(
                'name'  => 'first_name',
                'id'    => 'first_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('first_name'),
            );
            $this->data['last_name'] = array(
                'name'  => 'last_name',
                'id'    => 'last_name',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('last_name'),
            );
            $this->data['identity'] = array(
                'name'  => 'identity',
                'id'    => 'identity',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('identity'),
            );
            $this->data['email'] = array(
                'name'  => 'email',
                'id'    => 'email',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('email'),
            );
            $this->data['company'] = array(
                'name'  => 'company',
                'id'    => 'company',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('company'),
            );
            $this->data['phone'] = array(
                'name'  => 'phone',
                'id'    => 'phone',
                'type'  => 'text',
                'value' => $this->form_validation->set_value('phone'),
            );
            $this->data['password'] = array(
                'name'  => 'password',
                'id'    => 'password',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password'),
            );
            $this->data['password_confirm'] = array(
                'name'  => 'password_confirm',
                'id'    => 'password_confirm',
                'type'  => 'password',
                'value' => $this->form_validation->set_value('password_confirm'),
            );

            $this->_render_page('auth/create_user', $this->data);
        }
    }

	// edit a user
	public function edit_user($id)
	{
		$this->data['title'] = $this->lang->line('edit_user_heading');

		if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		{
			redirect('auth', 'refresh');
		}

		$user = $this->ion_auth->user($id)->row();
		$groups=$this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'required');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'required');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'), 'required');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'required');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = array(
					'first_name' => $this->input->post('first_name'),
					'last_name'  => $this->input->post('last_name'),
					'company'    => $this->input->post('company'),
					'phone'      => $this->input->post('phone'),
				);

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}



				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					//Update the groups user belongs to
					$groupData = $this->input->post('groups');

					if (isset($groupData) && !empty($groupData)) {

						$this->ion_auth->remove_from_group('', $id);

						foreach ($groupData as $grp) {
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

			// check to see if we are updating the user
			   if($this->ion_auth->update($user->id, $data))
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->messages() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }
			    else
			    {
			    	// redirect them back to the admin page if admin, or to the base url if non admin
				    $this->session->set_flashdata('message', $this->ion_auth->errors() );
				    if ($this->ion_auth->is_admin())
					{
						redirect('auth', 'refresh');
					}
					else
					{
						redirect('/', 'refresh');
					}

			    }

			}
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = array(
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		);
		$this->data['last_name'] = array(
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		);
		$this->data['company'] = array(
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
		);
		$this->data['phone'] = array(
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		);
		$this->data['password'] = array(
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		);
		$this->data['password_confirm'] = array(
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		);

		$this->_render_page('auth/edit_user', $this->data);
	}

	// create a new group
	public function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'required|alpha_dash');

		if ($this->form_validation->run() == TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = array(
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			);
			$this->data['description'] = array(
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			);

			$this->_render_page('auth/create_group', $this->data);
		}
	}

	// edit a group
	public function edit_group($id)
	{
		// bail if no group id given
		if(!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], $_POST['group_description']);

				if($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['group'] = $group;

		$readonly = $this->config->item('admin_group', 'ion_auth') === $group->name ? 'readonly' : '';

		$this->data['group_name'] = array(
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
			$readonly => $readonly,
		);
		$this->data['group_description'] = array(
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		);

		$this->_render_page('auth/edit_group', $this->data);
	}


	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	public function _valid_csrf_nonce()
	{
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $returnhtml);

		if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}


	/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA LOS PERFILES DE LOS USUARIOS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con los perfiles de usuarios
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/


public function perfil()
			{
	if(!$this->ion_auth->logged_in())
  {
    redirect('login');
  }
  	$this->load->model('Listas_model');
  	$this->load->library('mostrar_avatar');
  	$this->load->library('background_image');

	$datos['user'] = $user = $this->ion_auth->user()->row();
	$date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->registered_date);
    $datos['fecha_registro'] = $date->format('d/m/Y');

	$array_grupos = $this->ion_auth->get_users_groups($user->id)->result();
	$datos['grupos'] = '';
		foreach ($array_grupos as $item){
			$datos['grupos'].= ' '.$item->name;

		}
	$datos['listas'] = $this->_listas_usuario_perfil();
	$datos['solidarity'] = $this->ion_auth_model->get_solidarity($user->id);
	$datos['background'] = $this->background_image->select_background();
	$datos['avatar'] = $this->mostrar_avatar->mostrar_avatar($this->ion_auth->user()->row()->id);
	$datos['editar_avatar'] = $this->editar_avatar();
	$datos['total_criticas'] = $this->Ion_auth_model->get_total_comments_user($user->id);
	$datos['total_links'] = $this->Ion_auth_model->get_total_links_user($user->id);

				$this->load->view('users/perfil_test', $datos);

    }

public function _listas_usuario_perfil(){
	

		$result = $this->Listas_model->todas_listas_perfil();
		$html = '<br><center><table>
					  <tr>
			    <th>Nombre</th>
			    <th>Total contenido</th>
			    <th>Seguidores</th>
			  </tr>';
		foreach ($result as $item){
            $idlista = $item->list;
		   	$nombrelista = $item->name;
		   	$nombrelistaslimpio = preg_replace('/[^A-Za-z0-9\. -]/', '', $nombrelista);
		    $nombresinespacios = str_replace(" ","-",$nombrelistaslimpio);
		    $linklistas = '<a href=' . base_url() . 'list/' . $item->list . '/' . $nombresinespacios . '>' . $item->name . '</a>';

			 $html .= '

			  <tr>
			    <td>'.$linklistas.'</td>
			    <td>
			    	2 
			    </td>
			    <td>'.$item->follow.'</td>
			  </tr>
			 ';

		}
		$html.= '</table></center>';
		return $html;
	}


		//*******************************************************
		// SACAMOS TODAS LAS SERIES Y PELÍCULAS FAVORITAS Y DEMÁS
		//*******************************************************
    public function profile_user_viewed_films(){
    	$consulta = $this->ion_auth->get_users_viewed_films();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/peliculas/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;

    }

    public function profile_user_favourite_films(){
    	$consulta = $this->ion_auth->get_users_favourite_films();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/peliculas/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;

    }
    public function profile_user_pending_films(){

     	$consulta = $this->ion_auth->get_users_pending_films();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/peliculas/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;
     	
    }

    public function profile_user_favourite_series(){

     	$consulta = $this->ion_auth->get_users_favourite_series();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'series/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/series/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;
     	
    }    


    public function profile_user_pending_series(){

     	$consulta = $this->ion_auth->get_users_pending_series();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'series/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/series/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;
     	
    }  

    public function profile_user_following_series(){

     	$consulta = $this->ion_auth->get_users_following_series();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'series/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/series/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;
     	
    }      


    public function profile_user_viewed_series(){

     	$consulta = $this->ion_auth->get_users_viewed_series();
		$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'series/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/series/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;
     	
    } 
	public function _rating_poster_square($rate){

		if ($rate == '0'){
			$rate_box = 'r-neutral';
		} elseif ($rate < '5'){
			$rate_box = 'r-bad';
		} elseif ($rate < '8') {
			$rate_box = 'r-regular';
		} else {
			$rate_box = 'r-good';
		}
		return $rate_box;

	}



	public function editar_descripcion_user(){
		if (!$this->ion_auth->logged_in()){
			redirect('/login', 'refresh');
		}
		$this->form_validation->set_rules('description_user','Descripción','trim|required|min_length[5]|max_length[500]');
	 	if ($this->form_validation->run() === FALSE){	
	 		$data = array(
			'resultado'  => 'error'
			);
	 	} else {
	 		$descripcion = $this->input->post('description_user');
	 		$this->ion_auth_model->editar_descripcion_user($descripcion);
	 		$data = array(
			'resultado'  => 'correcto'
			);	 		
	 	}

	 	echo json_encode($data); 

	}
public function editar_avatar(){
$html = '';
$html.= '<form action="'.base_url().'users/editar_avatar_validacion" method="post" enctype="multipart/form-data">';
$html.= '<input type="file" name="avatar" accept="image/*"/>';
$html.= '<input type="submit" value="Editar avatar" name="submit">';
$html.= '</form>';
return $html;
}

public function editar_avatar_validacion(){
	$perfil = base_url(). 'profile';
// VALIDACIÓN DE LA FOTO
if (empty($_FILES['avatar']['name'])) {
			$this->session->set_flashdata('error_avatar','No has seleccionado ninguna imagen.' );
			redirect($perfil, 'refresh');
		}

					$iduser = $this->ion_auth->user()->row()->id;
					$rutasubida = 'asset/img/users/' . $iduser . '/avatar';
					$nombrearchivo = 'avatar';

$config['upload_path'] = $rutasubida;
$config['allowed_types'] = 'gif|jpg|png|jpeg|';
$config['max_size'] = '2048';
$config['file_name'] = $nombrearchivo;
$config['overwrite'] = TRUE;

$this->load->library('upload',$config);
$this->upload->initialize($config);
$this->upload->do_upload('userfile');

if (!$this->upload->do_upload('avatar')){
	//Errores en la subida de archivos
	$error= $this->upload->display_errors();
	$this->session->set_flashdata('error_avatar',$error );

redirect($perfil, 'refresh');

} else {
	// La imagen es válida, borramos todo lo que haya en la carpeta avatar del usuario
	$dir = $rutasubida;
$di = new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS);
$ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
foreach ( $ri as $file ) {
    $file->isDir() ?  rmdir($file) : unlink($file);
}
	// Resubimos el avatar. Primero lo subimos para comprobar que no hayan errores
	//borramos todo por si hay archivos con otras extensiones y lo volvemos a subir
$this->upload->do_upload('avatar');

	// Comprobamos su extension para generar el thumbnail
	$getName = $_FILES['avatar']['name'];
	$explodeName = explode(".", $getName);
	$extention = mb_strtolower(end($explodeName));


// Cambiamos el tamaño del avatar
$config['image_library'] = 'gd2';
$config['source_image'] = $rutasubida . '/avatar.'. $extention;
$config['width']         = 300;
$config['height']       = 300;
$config['maintain_ratio'] = TRUE;

$this->load->library('image_lib', $config);
    $this->image_lib->clear();
    $this->image_lib->initialize($config);
    $this->image_lib->resize();

	// Creamos el thumbnail
$config['image_library'] = 'gd2';
$config['source_image'] = $rutasubida . '/avatar.'. $extention;
$config['create_thumb'] = TRUE;
$config['maintain_ratio'] = TRUE;
$config['width']         = 75;
$config['height']       = 50;
    $this->image_lib->clear();
    $this->image_lib->initialize($config);
    $this->image_lib->resize();
	// Flashdata para mostrar que todo ok
		$this->session->set_flashdata('correcto_avatar','Has cambiado tu avatar correctamente');
redirect($perfil, 'refresh');

}

}


    public function perfil_terceros($id_user){
		$this->load->model('Ion_auth_model');
		$this->load->library('Mostrar_avatar');
		$this->load->library('Background_image');

		$id_user_vacio = $this->security->xss_clean($id_user);
		$datos['detalle'] = $this->Ion_auth_model->perfil_terceros($id_user_vacio);
		$datos['background'] = $this->background_image->select_background();
		if ($datos['detalle']->private === '0'){


			$date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $datos['detalle']->registered_date);
    		$datos['fecha_registro'] = $date->format('d/m/Y');

			$array_grupos = $this->ion_auth->get_users_groups($id_user_vacio)->result();
			$datos['grupos'] = '';
			foreach ($array_grupos as $item){
				$datos['grupos'].= ' '.$item->name;

			}

			$datos['avatar'] = $this->mostrar_avatar->mostrar_avatar($id_user);
			$datos['total_criticas'] = $this->Ion_auth_model->get_total_comments_user($id_user);
			$datos['total_links'] = $this->Ion_auth_model->get_total_links_user($id_user);

			$this->load->view('users/perfil_terceros', $datos);
		} else {
			echo "Este perfil es privado";
			}


    }


// Funciones de recuperar contraseña
public function pass_olvidada_form(){ 

	if (!$this->ion_auth->logged_in())
		{
		$this->data['background'] = $this->background_image->background_image();
		$this->load->view('users/pass_olvidada', $this->data);

				
			} else {
		redirect('/profile', 'refresh');

			}

}  														
public function pass_olvidada(){
	$this->load->model('Ion_auth_model');

	$this->form_validation->set_rules('email','Email','trim|valid_email|required');
 				if ($this->form_validation->run() === FALSE)
  				{
					$datapass = array(
			'mensaje'   => form_error('email'),
			'resultado'  => 'erroremail'
			);
 			echo json_encode($datapass); 
				}

				if($this->form_validation->run() === TRUE){
				$numeroresultado = count($this->Ion_auth_model->pass_olvidada_comprobar_email());
				if($numeroresultado === 1){
						$datapass = array(
			
			'resultado'  => 'emailenviado'
			);
 			echo json_encode($datapass); 
					$this->Ion_auth_model->pass_olvidada_codigo();
					
				} else {
					 						$datapass = array(
			'mensaje'   => 'Esta dirección de email no está asociada con ninguna cuenta de usuario.',
			'resultado'  => 'emailnoexiste'
			);
 			echo json_encode($datapass); 
				}
				}
				
			
}
public function pass_olvidada_form_completar(){
	$this->load->model('Ion_auth_model');
				if (!$this->ion_auth->logged_in())
		{
	$this->data['background'] = $this->background_image->background_image();

	$consulta = $this->Ion_auth_model->pass_olvidada_completar();

if ($consulta === 'error'){
			$this->data['titulomensaje'] = "¡Error! Hay un problema con este token";
        	$this->data['mensaje'] = "El token no existe o ha expirado";
        	$this->data['redirigirmensaje'] = base_url();
		$this->load->view('mensaje_generico_popup', $this->data);

} else {

	$this->load->view('users/pass_olvidada_completar', $this->data);
}
} else {
	     redirect('/profile', 'refresh');

}
}
public function pass_olvidada_form_enviar(){
		$this->load->model('Ion_auth_model');

    $this->form_validation->set_rules('password','Contraseña','trim|min_length[6]|max_length[20]|matches[confirm_password]|required');
    $this->form_validation->set_rules('confirm_password','Confirmar contraseña','trim|required');
  if ($this->form_validation->run() === FALSE){
		$datapass = array(
			'mensaje'   => form_error('password'),
			'resultado'  => 'mal'
			);
 			echo json_encode($datapass); 
  }
  if ($this->form_validation->run() === TRUE){ 
  $this->Ion_auth_model->pass_olvidada_form_enviar(); 
  						$datapass = array(
			'mensaje'   => 'Tu contraseña se ha modificado correctamente, ya puedes entrar nuevamente en tu cuenta. <br> ¡Te llevamos de vuelta al login!',
			'resultado'  => 'correcto'
			);
 			echo json_encode($datapass);          

}
}

											// Terminamos funciones de recuperar contraseña
   		public function editar_password(){
			 $this->load->model('Ion_auth_model');

        $this->form_validation->set_rules('new','Contraseña','trim|min_length[6]|max_length[20]|matches[confirm_password]|required');
        $this->form_validation->set_rules('confirm_password','Confirmar contraseña','trim|required');
  if ($this->form_validation->run() === FALSE)
                {
                          		$data = array(
			"mensaje"   => form_error("new"),
			"resultado"  => "error"
			);
 			echo json_encode($data);
                }
  if ($this->form_validation->run() === TRUE)
  {
  	$old = $_POST['old'];
		$new = $_POST['new'];
 			$old_password_matches = $this->Ion_auth_model->hash_password_db($this->ion_auth->user()->row()->id_user, $old);
		if ($old_password_matches === TRUE)
		{
			$this->Ion_auth_model->cambiar_password();
				$data = array(
			"mensaje"  => "¡Has actualizado tu contraseña!",
			"resultado"  => "correcto"
			);
							echo json_encode($data);

}	
			else
			{
						$data = array(
			"mensaje"  => "La contraseña no coincide con tu contraseña actual.",
			"resultado"  => "error"
			);
			echo json_encode($data);

			}
			
				      
			} 
    }

public function editar_perfil(){
	$this->load->library('background_image');
	// Relacionado con el form del idioma
	$this->data['opciones_idioma'] = $this->editar_idioma_opciones();
	$this->data['idioma_usuario'] = $this->ion_auth->user()->row()->language;
	$this->data['background'] = $this->background_image->select_background();
	// Cuentas de donaciones
	$tipos = $this->_opciones_cuenta_donaciones();
	$this->data['iconos_cuentas_donaciones'] = '';
	foreach ($tipos as $item){
	$img_name = strtolower($item->name) . '.png';
	$this->data['iconos_cuentas_donaciones'].= '<a><img src="'.base_url().'asset/img/icons/'.$img_name.'" class="icondonation '.strtolower($item->name).'" class="button '.$item->name.'"></a> ';	
	}
	$this->data['bitcoin'] = $this->_get_cuenta_donaciones_user_bitcoin();
	$this->data['paypal'] = $this->_get_cuenta_donaciones_user_paypal();

	$this->load->view('users/edit_profile', $this->data);
}
// Sacamos cuentas de bitcoin y paypal
public function _get_cuenta_donaciones_user_bitcoin(){
	$cuentas = $this->ion_auth_model->get_cuenta_donacion_bitcoin();

	if ($cuentas !== '0'){
		if ($cuentas->url === ''){
			$codigo = '';
		} else {
			$codigo = $cuentas->url;
		}
	return $codigo;
	} else {
		$codigo = '';
		return $codigo;

	}
}

public function _get_cuenta_donaciones_user_paypal(){
	$cuentas = $this->ion_auth_model->get_cuenta_donacion_paypal();
	if ($cuentas !== '0'){
		if ($cuentas->url === ''){
			$codigo = '';
		} else {
			$codigo = $cuentas->url;
		}
	return $codigo;
	} else {
		$codigo = '';
		return $codigo;

	}
}

 				public function editar_privacidad(){
 					$this->load->model('Ion_auth_model');
					$this->Ion_auth_model->editar_privacidad();
					$data = array(
			"mensaje"  => "La privacidad de tu cuenta ha sido modificada",
			"resultado"  => "correcto"
			);
			echo json_encode($data);

 				}

 				public function editar_idioma(){
 					$this->load->model('Ion_auth_model');
					$this->Ion_auth_model->editar_idioma();
					$data = array(
			"mensaje"  => "El idioma de tu cuenta se ha modificado correctamente",
			"resultado"  => "correcto"
			);
			echo json_encode($data);

 				} 				

 				public function editar_idioma_opciones(){
 					$this->load->model('Ion_auth_model');
					$opciones_idioma = $this->Ion_auth_model->editar_idioma_opciones();
					return $opciones_idioma;

 				} 				
 						public function editar_email_codigo(){
 				$this->form_validation->set_rules('emailnuevo','Email','trim|valid_email|required|is_unique[users.email]');
 				if ($this->form_validation->run() === TRUE)
  				{
 					$this->load->model('Ion_auth_model');
					$this->Ion_auth_model->editar_email_codigo();
					$data = array(
			"mensaje"  => "Hemos enviado un email de confirmación a " . $this->input->post('emailnuevo') . ". Para completar el cambio de email debes pinchar el link de activación.",
			"resultado"  => "correcto"
			);
			echo json_encode($data);
				} 
				if ($this->form_validation->run() === FALSE)
 {
				$data = array(
			"mensaje"  => form_error('emailnuevo'),
			"resultado"  => "error"
			);
							echo json_encode($data);

				}
			}
			public function _opciones_cuenta_donaciones(){
				$tipos = $this->ion_auth_model->fuentes_donaciones();
				return $tipos;

			}
			public function editar_cuenta_donaciones_validacion(){
 				$this->form_validation->set_rules('codigo','Código','required');
 				if ($this->form_validation->run() === TRUE)
  				{
  				$this->ion_auth_model->editar_cuenta_donaciones();
				$data = array(
			"mensaje"  => "Cuenta de donación modificada correctamente con el codigo ". $this->input->post('codigo'),
			"resultado"  => "correcto"
			);
			echo json_encode($data);
  				} else {
				$data = array(
			"mensaje"  => "Hubo un error con la modificación de tu cuenta. Vuelve a intentarlo",
			"resultado"  => "error"
			);
			echo json_encode($data);
  				}

			}



 				
 				public function editar_email_completar(){

					$this->load->model('Ion_auth_model');

	$this->data['background'] = $this->background_image->background_image();

	$consulta = $this->Ion_auth_model->editar_email_completar();

if ($consulta === 'error'){
		
			$this->data['titulomensaje'] = "¡Error! Algo está pasando con este token";
        	$this->data['mensaje'] = "El token no existe o ha expirado";
        	$this->data['redirigirmensaje'] = base_url();
          
	
		$this->load->view('mensaje_generico_popup', $this->data);

} else {
			$this->data['titulomensaje'] = "¡Email actualizado!";
        	$this->data['mensaje'] = "Tu email se ha actualizado correctamente";
        	$this->data['redirigirmensaje'] = base_url();
	$this->load->view('mensaje_generico_popup', $this->data);
}

 				}

 				public function info_grupos(){
 				$this->load->model('Ion_auth_model');
				$datos = $this->Ion_auth_model->info_grupos();

 				}

 		public function editar_aportacion(){
 			$aportacion = $this->ion_auth_model->editar_aportacion();
 			$data = array("resultado"  => $aportacion);
 			echo json_encode($data);


 		}
	/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA EL CALENDARIO
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con el calendario
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
    public function calendar(){

    	$this->load->view('calendar');

    }

        public function eventscalendar(){
    	$this->load->model('Calendar_model');
    	$eventos = $this->Calendar_model->events();
		echo json_encode($eventos);

    }

/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA LISTAS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con las listas
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
public function nueva_lista(){
	$this->load->model('Listas_model');

	if ($this->Listas_model->ver_total_listas_usuario() < 10){ // Si el usuario tiene menos de 10 listas le dejamos
$this->form_validation->set_rules('nombre', 'Nombre',  'trim|required|min_length[3]|max_length[50]|xss_clean');
$this->form_validation->set_rules('descripcion', 'Descripción',  'trim|required|min_length[3]|max_length[500]|xss_clean');
			if($this->form_validation->run() === TRUE){

		$this->Listas_model->crear_lista();
		$data = array(
            "resultado"  => "correcto",
            );
	} 
			if($this->form_validation->run() === FALSE){
				$data = array(
            "resultado"  => "errortexto",
            );

}
} else { // Si el usuario tiene 10 listas
	$data = array(
            "resultado"  => "errormax",
            );
}

 echo json_encode($data);

}

public function editar_lista(){
	$this->load->model('Listas_model');

$this->form_validation->set_rules('nombre', 'Nombre',  'trim|required|min_length[3]|max_length[50]|xss_clean');
$this->form_validation->set_rules('descripcion', 'Descripción',  'trim|required|min_length[3]|max_length[500]|xss_clean');
			if($this->form_validation->run() === TRUE){

		$resultado = $this->Listas_model->editar_lista();
		$data = array(
            "resultado"  => $resultado,
            );
	} 
			if($this->form_validation->run() === FALSE){
				$data = array(
            "resultado"  => "errortexto",
            );

}


 echo json_encode($data);

}

}
