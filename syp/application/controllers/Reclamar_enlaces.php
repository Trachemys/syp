<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reclamar_enlaces extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->library('ion_auth');
      $this->load->library('form_validation');




	}

  public function index(){
  		 $this->form_validation->set_rules('content', 'Contenido', 'required');
                $this->form_validation->set_rules('type', 'Tipo', 'required');

                if ($this->form_validation->run() == FALSE)
                {
                  $datos['heading'] = 'Error inesperado';
                  $datos['message'] = 'Actividad sospechosa';
              $this->load->view('errors/html/error_404',$datos);
                }
                else
                {
                  $this->load->model('Reclamar_enlaces_model');
                  $res = $this->Reclamar_enlaces_model->añadir_reclamo();
                    if ($res == TRUE){
                      $data = array(
                      "resultado" => 'correcto',
                      "mensaje"   => 'Tu petición para añadir enlaces se ha guardado correctamente, intentaremos añadir enlaces en la mayor brevedad posible. </br><b>¡Pásate en unas horas!</b>',
                      "header" => 'Petición enviada correctamente',
                      );
                    } else {
                      $data = array(
                      "resultado" => 'error',
                      "mensaje"   => 'Esta función está reservada para usuarios registrados. <br> ¡Anímate y hazte una cuenta! Es completamente gratuito y desbloquearás muchísimas características..',
                      "header" => 'Error en la petición',
                      );                      
                    }
      echo json_encode($data);
                }
  }
}