<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generos extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->library('ion_auth');


}
public function detalle_genero($id_genero){
	$lista = $this->listar_contenido_genero($id_genero);
	echo $lista; // Lo cambiaremos por una view

}
 public function listar_contenido_genero($id_genero){
	$this->load->model('Genres_model');
	$lista = $this->Genres_model->detalle_generos($id_genero);
	$html = '';
	foreach ($lista as $item){
		$html.= $item->title . '</br>';
	}
	return $html;
 }
  

  }
