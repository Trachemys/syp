<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->library('ion_auth');
			$this->load->library('grocery_CRUD');

		if (!$this->ion_auth->is_admin())
		{
			$this->session->set_flashdata('message', 'You must be an admin to view this page');
			redirect('login');
		}



	}
			public function index(){
		$datos['username'] = $this->ion_auth->user()->row()->username;

			$this->load->view('admin/header', $datos);	
			$this->load->view('admin/index');	
			$this->load->view('admin/footer');	

			}


			public function _tablas($output = null){
						$datos['username'] = $this->ion_auth->user()->row()->username;

						$this->load->view('admin/header',$datos);	
						$this->load->view('admin/tablas',(array)$output);
						$this->load->view('admin/footer');	

			}
			public function ver_users()
			{
			$crud = new grocery_CRUD();
			$crud->set_table('tblusers');
			$crud->columns('id_user','username','ip_address','email', 'active', 'created_on');
			$crud->add_action('Grupos', 'Grupos', 'demo/action_more','ui-icon-plus');
			$crud->set_subject('usuarios');
			$output = $crud->render();
			$this->_tablas($output);
	}

			public function ver_tickets_abiertos()
			{
			$crud = new grocery_CRUD();
			$crud->set_table('tbltickets');
			$crud->columns('id','date','user','type');
			$crud->add_action('Ver ticket', 'Ver ticket', 'tickets','ui-icon-plus');
			$crud->set_subject('Tickets');
			$crud->where('status','1');
			$crud->set_relation('type','tblticketstypes','{type}');
			$crud->set_relation('user','tblusers','{username}');

			$output = $crud->render();
			$this->_tablas($output);
	}

				public function ver_codigos()
			{
			$crud = new grocery_CRUD();
			$crud->set_table('tblcodepremium');
			$crud->columns('code','created_on','expiration_date', 'uses', 'premium_days');
			$crud->add_action('Grupos', 'Grupos', 'demo/action_more','ui-icon-plus');
			$crud->set_subject('código promocional');

			$output = $crud->render();
			$this->_tablas($output);
	}

 	public function ver_peticiones()
			{
			$crud = new grocery_CRUD();
			$crud->set_table('tbllinksrequests');
			$crud->columns('id','content','type','date');
			$crud->add_action('Grupos', 'Grupos', 'demo/action_more','ui-icon-plus');
			$crud->set_relation('type','tbllinkstypes','{type}');
			$crud->set_relation('content','tblcontentstitles','{title}');

			$crud->set_subject('Links requeridos por los users');

			$output = $crud->render();
			$this->_tablas($output);
	}

  }
