<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

	public function __construct(){
			parent::__construct();
		$this->load->library('background_image');
		$this->load->library('ion_auth');



	}

	public function _background_user_contents(){
	$this->load->model('ion_auth_model');
	$query = $this->ion_auth_model->background_get_contents();
	shuffle($query);
	$background = '';
	foreach ($query as $item){
			$background.= $this->ion_auth_model->background_type_content($item['content']);
			break;
	}
	// Comprobamos si existe contenido para el usuario, si no existe mostramos background generico
	if ($background === ''){
            $this->load->library('background_image');
			return $this->background_image->background_image();
	} else {
		return $background;
			}
	}


	public function index(){
				if ($this->ion_auth->logged_in())
		{
		$datos['background'] = $this->_background_user_contents();
		$this->load->view('calendar/calendar', $datos);
	} else {
		$datos['background'] = $this->background_image->background_image();
		$datos['mensaje'] = 'El calendario es una función especial para usuarios registrados.';
		$this->load->view('mensaje_generico_plano', $datos);

	}
	}

}
