<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_sections extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->model('Content_sections_model');
			$this->load->library('background_image');






	}
public function index_films(){
	$datos['background'] = $this->background_image->select_background();
	$this->load->view('contenido_secciones/films', $datos);
}
public function top_rated_films(){
	$consulta = $this->Content_sections_model->top_rated_films();
	$html = '
			<div class="row">
				<div class="col-4">
				<br><br><br>
  					<section class="regular slider">
		';
		foreach ($consulta as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 200);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/peliculas/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									<p class="infoposter_show">
										<span class="infoposter_title_show">'.$item->title.'</span></br>
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		echo $html;
}



  
	public function _rating_poster_square($rate){

		if ($rate == '0'){
			$rate_box = 'r-neutral';
		} elseif ($rate < '5'){
			$rate_box = 'r-bad';
		} elseif ($rate < '8') {
			$rate_box = 'r-regular';
		} else {
			$rate_box = 'r-good';
		}
		return $rate_box;

	}



	public function _corner_generator($content){
		$this->load->model('Corners_model');
		if ($this->ion_auth->logged_in()){

			if ($this->Corners_model->corner_favorito($content) == TRUE){
				$corner = '<div class="corner c-favourite"></div>';
			} elseif($this->Corners_model->corner_pendiente($content) == TRUE){
				$corner = '<div class="corner c-pending"></div>';
			} elseif($this->Corners_model->corner_vista($content) == TRUE){
				$corner = '<div class="corner c-viewed"></div>';
			} elseif($this->Corners_model->corner_siguiendo($content) == TRUE){
				$corner = '<div class="corner c-following"></div>';

			} else {
				$corner = '';
			}
		} else {
			$corner = '';
		}
		return $corner;

	}

  }
