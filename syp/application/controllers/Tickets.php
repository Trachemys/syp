<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tickets extends CI_Controller {

	public function __construct(){
			parent::__construct();
			$this->load->library('ion_auth');




	}

  public function index(){
  		if (!$this->ion_auth->logged_in())
		{
			redirect('login', 'refresh');
		}
    $this->load->model('Tickets_model');
    $id_user = $this->ion_auth->user()->row()->id;
    $datos['tipos_tickets'] = $this->Tickets_model->tipos_tickets();
    $tickets_abiertos = $this->Tickets_model->ver_tickets_abiertos($id_user);
    $tickets_pendientes = $this->Tickets_model->ver_tickets_pendientes($id_user);
    $tickets_cerrados = $this->Tickets_model->ver_tickets_cerrados($id_user);

    $datos['tickets_abiertos'] = '';
    foreach ($tickets_abiertos as $ticket){
   	$datos['tickets_abiertos'].= '<a href="'.base_url().'tickets/'.$ticket->id.'">Ticket #'.$ticket->id.' </a> '.$ticket->type.'</br>' ;
    }

    $datos['tickets_pendientes'] = '';
    foreach ($tickets_pendientes as $ticket){
   	$datos['tickets_pendientes'] = '<a href="'.base_url().'tickets/'.$ticket->id.'">Ticket #'.$ticket->id.' </a> '.$ticket->type.'</br>' ;
    }    

    $datos['tickets_cerrados'] = '';
    foreach ($tickets_cerrados as $ticket){
    $datos['tickets_cerrados'] = '<a href="'.base_url().'tickets/'.$ticket->id.'">Ticket #'.$ticket->id.' </a> '.$ticket->type.'</br>' ;
    }

    $this->load->view('tickets/index', $datos);
  }

  	public function nuevo_ticket_validacion(){
  					$this->load->library('form_validation');
  					$this->load->model('Tickets_model');

				$this->form_validation->set_rules('type', 'Tipo de ticket', 'required');
				$this->form_validation->set_rules('message', 'Mensaje', 'required');

 				if ($this->form_validation->run() == FALSE)
                {
             $this->session->set_flashdata('error', 'Para enviar un ticket debes completar todos los campos.');               	
			redirect('tickets/index', 'refresh');
                }
                else
                {
             $this->session->set_flashdata('correcto', 'Hemos recibido tu ticket, en menos de 24 horas te responderemos.');               	
             $this->Tickets_model->nuevo_ticket();
			redirect('tickets/index', 'refresh');
                }

  	}
  	public function nuevo_mensaje_validacion(){
  		$this->load->library('form_validation');
  		$this->load->model('Tickets_model');
  				$id_ticket = $this->input->post('id_ticket');
  				$redirect = $this->input->post('redirect');
		$comprobar = $this->Tickets_model->comprobar_ticket_mensaje($id_ticket);
		$numeroresultado = count($comprobar);
		if ($numeroresultado === 1  || $this->ion_auth->is_admin()){

				$this->form_validation->set_rules('message', 'Mensaje', 'required');

 				if ($this->form_validation->run() == FALSE)
                {
             $this->session->set_flashdata('error', 'No has escrito ningún mensaje.');
             redirect($redirect, 'refresh');
               	
                }
                else
                {
             $this->session->set_flashdata('correcto', 'Has enviado el mensaje correctamente');               	
             $this->Tickets_model->nuevo_mensaje();
             redirect($redirect, 'refresh');

                }
            } else {
            	echo "no cuela";
            }
  	}
  public function perfil_ticket($id_ticket){
  	  		if (!$this->ion_auth->logged_in())
		{
			redirect('login', 'refresh');
		}

  		$this->load->model('Tickets_model');
  		$id_ticket_vacio = $this->security->xss_clean($id_ticket);
		$datos['detalle'] = $this->Tickets_model->perfil_ticket($id_ticket_vacio);
		$numeroresultado = count($datos['detalle']);
		if ($numeroresultado === 1){
			$id_user = $this->ion_auth->user()->row()->id;
				if($id_user === $datos['detalle']->user || $this->ion_auth->is_admin()){
					$datos['mensajes'] = $this->ver_mensajes($id_ticket);
					$this->load->view('tickets/perfil_ticket', $datos);

		} else {
			echo "No tienes permisos para ver este ticket.";
		}
		} else {
			echo "No existe ticket con esta ID";
		}



  }

	public function ver_mensajes($id_ticket){
		$this->load->model('Tickets_model');
		$this->load->library('Mostrar_avatar');
		$mensajes = $this->Tickets_model->ver_mensajes($id_ticket);
		$html = "";
		$html .=  $mensajes == 0 ? "<ul class='tree'>" : "<ul>";

		foreach ($mensajes as $re){
			            	$avatar_usuario = $this->mostrar_avatar->mostrar_avatar($re->id_user, $tipo = "thumb");
			                $html .= " <li class='comment' id='comment'>
                <div class= 'avatar-container'><img src=".$avatar_usuario."></div> 

                <div>".$re->username."</div> 

                <div>".$re->comment."</div>
                <div>".$re->date."</div>"
                ;
		}
return $html;

	}
  
  }
