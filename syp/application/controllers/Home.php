<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
			parent::__construct();


			$this->load->library('form_validation');
            $this->load->library('table');
			$this->load->library('ion_auth');
			$this->load->library('background_image');

			$this->load->helper('form');
			$this->load->helper('html');
            $this->load->helper('smiley');

	}
/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES DEL HOME
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con el HOME de la web
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/

	public function test(){
		$date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', '2017-11-12 13:55:47');
    echo $date;   // output:  2017-01-21 00:00:00
    echo '<br/>'.$date->diffForHumans();  // output: 7 hours ago
	}


	public function index()
	{

		$email = $this->session->userdata('email');
		$datos['lista_generos'] = $this->_lista_generos();
		$datos['capitulos_pendientes'] = '';
		$datos['peliculas_pendientes'] = '';
		$datos['series_pendientes'] = '';
		// CARGAMOS INFORMACIÓN PARA USUARIOS LOGEADOS Y SIN LOGEAR
		$datos['trendings_series'] = $this->_trending_series_home();
		$datos['trendings_films'] =$this->_trending_films_home();
		$datos['estrenos_cine'] = $this->_estrenos_home();
		$datos['nuevos_capitulos'] = '';

		// COOKIES
		$datos['cookies'] = $this->_index_cookies();

		// BACKGROUND
		$datos['background'] = $this->background_image->select_background();


		// CARGAMOS INDEX E INFORMACIÓN RELEVANTE PARA USUARIOS LOGEADOS
		if ($this->ion_auth->logged_in())
		{
		$id_user = $this->ion_auth->user()->row()->id;
		$datos['capitulos_pendientes'] = $this->_capitulos_disponibles_home($id_user);
		$datos['peliculas_pendientes'] = $this->_peliculas_pendientes_home($id_user);
		$datos['series_pendientes'] = $this->_series_pendientes_home($id_user);

		} else {
		// CARGAMOS INDEX E INFORMACIÓN RELEVANTE PARA USUARIOS SIN LOGEAR
		$this->load->library('background_image');

		}
		$this->load->view('index', $datos);

	}

	public function _index_cookies(){
		$cookie = get_cookie("sypac");
		if ($cookie === '1'){
			$html = '';
		} else {
			$html = '
			<div id="cookies_container" class="cookies_container">
				<br>
				Utilizamos cookies propias y de terceros para prestar nuestros servicios.
				<br>
				Destinamos una pequeña parte de tu CPU al minado de criptomonedas para poder mantener el servidor. Su efecto es nulo en el rendimiento de tu PC. Esta opción es desactivable desde tu perfil (gratis)
				<br>
				<form method="POST" id="cookies_accept_form" action="'.base_url().'cookies_accept/add_cookie_accept">
					<button id="cookies_accept" type="submit">Aceptar</button>
					<a href="'.base_url().'cookies" target="_blank"><input type="button" id="cookies_info" value="Más info" /></a>
				</form>
				<br>
			</div>';
		}
		return $html;

}

public function test_cookies_user(){
	$this->load->library('Ion_auth');
	$this->ion_auth->check_cookie_login();
}



public function _capitulos_disponibles_home($id_user){
			$this->load->model('Home_user_model');
			$siguiendo = $this->Home_user_model->siguiendo($id_user);
			$html = '';
		foreach ($siguiendo as $item){
					$ultimo_cap = $this->Home_user_model->ultimo_cap_emitido($item->content, $id_user);
					$ver_si_visto = $this->Home_user_model->ver_si_visto_ultimo_cap($ultimo_cap->episode, $id_user);
					if ($ver_si_visto === '0'){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($ultimo_cap->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($ultimo_cap->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($ultimo_cap->series);
			$url = base_url() . 'series/' .$ultimo_cap->series. '/'. urlencode(str_replace(' ','-',$ultimo_cap->title));

			$ruta_caratula = base_url().'asset/img/series/' .$ultimo_cap->series. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
			if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
			}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta serie.';
			}
			$html.='


		    				<div>
		    					<a class="poster_link" href="'.$url.'">
									<div class="poster_slide">
										<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$ultimo_cap->rate.'</span></p>
										
										<img src="'.$ruta_caratula.'">
										'.$corner.'
										<p class="infoposter">
										<span class="infoposter_title">'.$ultimo_cap->title.'</span></br>
											<p class ="infoposter_plot">
											'.$plot_reducido.'...
											</p>
										</p>
									</div>
			   					</a>
		   					</div>';
						
					}
		}
		return $html;
}

public function _peliculas_pendientes_home($id_user){
			$this->load->model('Home_user_model');
			$peliculas_pendientes = $this->Home_user_model->peliculas_pendientes_home($id_user);
			$html = '';
			// Hacemos un if para comprobar si el array está vacio, en caso de que no esté vacío generamos el html
			if (!empty($peliculas_pendientes)){
$html = '
			<div class="row">
				<div class="col-4">
					<div class="slide_header"><h4 class="pending_films_header" onclick="toggle_films_pendientes()">▼ Películas pendientes</h4></div>
  					<section class="regular slider pending_films_section" style="display:none">
		';
		foreach ($peliculas_pendientes as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($item->content);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));

			$ruta_caratula = base_url().'asset/img/films/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									'.$corner.'
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		}
		return $html;
	}

public function _series_pendientes_home($id_user){
			$this->load->model('Home_user_model');
			$series_pendientes = $this->Home_user_model->series_pendientes_home($id_user);
			$html = '';
			// Hacemos un if para comprobar si el array está vacio, en caso de que no esté vacío generamos el html
			if (!empty($series_pendientes)){
$html = '
			<div class="row">
				<div class="col-4">
					<div class="slide_header"><h4 class="pending_series_header" onclick="toggle_series_pendientes()">▼ Series pendientes</h4></div>
  					<section class="regular slider pending_series_section" style="display:none">
		';
		foreach ($series_pendientes as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($item->content);
			$url = base_url() . 'series/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));

			$ruta_caratula = base_url().'asset/img/series/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}
			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta serie.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									'.$corner.'
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		}
		return $html;
	}


	public function _trending_series_home(){
		$this->load->model('Home_user_model');
		$trending = $this->Home_user_model->trending_series();
		shuffle($trending);
		$html = '
			<div class="row">
				<div class="col-4">
					<div class="slide_header"><h4>Series populares</h4></div>
  					<section class="regular slider">
		';
		foreach ($trending as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($item->content);
			$url = base_url() . 'series/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/series/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta serie.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									'.$corner.'
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		return $html;
	}


	public function _trending_films_home(){
		$this->load->model('Home_user_model');
		$trending = $this->Home_user_model->trending_films();
		shuffle($trending);
		$html = '
			<div class="row">
				<div class="col-4">
					<div class="slide_header"><h4>Películas populares</h4></div>
  					<section class="regular slider">
		';
		foreach ($trending as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($item->content);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/peliculas/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									'.$corner.'
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		return $html;
	}

	public function _estrenos_home(){
		$this->load->model('Home_user_model');
		$estrenos = $this->Home_user_model->estrenos();
		$html = '
			<div class="row">
				<div class="col-4">
					<div class="slide_header"><h4>Estrenos de Cine</h4></div>
  					<section class="regular slider">
		';
		foreach ($estrenos as $item){
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($item->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($item->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($item->content);
			$url = base_url() . 'films/' .$item->content. '/'. urlencode(str_replace(' ','-',$item->title));
			$ruta_caratula = base_url().'asset/img/peliculas/' .$item->content. '/c.jpg';

			$header_imagen = get_headers($ruta_caratula);
				if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
					$ruta_caratula = base_url().'asset/img/layout/caratula.jpg';
				}

			if ($plot_reducido === ''){
				$plot_reducido = 'No tenemos disponible una sinopsis para esta película.';
			}
			$html.='

						<a class="poster_link" href="'.$url.'">
		    				<div>
								<div class="poster_slide">
									<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$item->rate.'</span></p>
									
									<img src="'.$ruta_caratula.'">
									'.$corner.'
									<p class="infoposter">
									<span class="infoposter_title">'.$item->title.'</span></br>
										<p class ="infoposter_plot">
										'.$plot_reducido.'...
										</p>
									</p>
								</div>
		   					</div>
		   				</a>';
		}
		$html.='
  					</section>
				</div>
			</div>		
		';
		return $html;
	}



	public function _rating_poster_square($rate){

		if ($rate == '0'){
			$rate_box = 'r-neutral';
		} elseif ($rate < '5'){
			$rate_box = 'r-bad';
		} elseif ($rate < '8') {
			$rate_box = 'r-regular';
		} else {
			$rate_box = 'r-good';
		}
		return $rate_box;

	}



	public function _corner_generator($content){
		$this->load->model('Corners_model');
		if ($this->ion_auth->logged_in()){

			if ($this->Corners_model->corner_favorito($content) == TRUE){
				$corner = '<div class="corner c-favourite"></div>';
			} elseif($this->Corners_model->corner_pendiente($content) == TRUE){
				$corner = '<div class="corner c-pending"></div>';
			} elseif($this->Corners_model->corner_vista($content) == TRUE){
				$corner = '<div class="corner c-viewed"></div>';
			} elseif($this->Corners_model->corner_siguiendo($content) == TRUE){
				$corner = '<div class="corner c-following"></div>';

			} else {
				$corner = '';
			}
		} else {
			$corner = '';
		}
		return $corner;

	}



/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCION PARA VER EL PERFIL DEL CONTENIDO (SERIES, PELÍCULAS, CAPÍTULOS)
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con los perfiles de los contenidos
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/



		public function perfil_series($id_serie){
		$this->load->model('Series_model');
		$this->load->model('Listas_model');
		$html='';
		// Hacemos esta pequeña comprobación para saber si el usuario está logeado o no y establecer la variable idusuario que usaremos en los comentarios a la hora de generar los padres e hijos.			
		if ($this->ion_auth->logged_in()){
			$datos['idusuario'] = $this->ion_auth->user()->row()->id;
		} else {
			$datos['idusuario'] = "";
		}

		// Añadir o eliminar contenido de las listas del usuario			
		$datos['listas'] = $this->listas_detalle_contenido();
		$datos['crear_listas_url'] = $this->_crear_listas_url();

		// Sacamos la info relacionada con la serie vía id
		$id_serie_vacio = $this->security->xss_clean($id_serie);
		$datos['detalle'] = $this->Series_model->detalle_series($id_serie_vacio);
		$numeroresultado = count($datos['detalle']);

		// Sacamos las temporadas
		$datos['temporadas'] = $this->Series_model->lista_temporadas_detalle($id_serie);

		// Con este if comprobamos que realmente existe una serie con esta id, si no luego nos saltarán objetos y foreaches inválidos
		if ($numeroresultado === 1){
			// Establecemos el color de los botones según el estado que ha marcado el usuario previamente
			$datos['colorbotonseguir'] = $this->botones_mostrar_seguir();
			$datos['colorbotonfavorito'] = $this->botones_mostrar_favorito();
			$datos['colorbotonpendiente'] = $this->botones_mostrar_pendiente();
			$datos['colorbotonvista']  = $this->botones_mostrar_vista();

			// Carátula y banner
			$datos['poster'] = base_url() . 'asset/img/series/' . $datos['detalle']->content . '/c.jpg';
			$datos['background'] = base_url() . 'asset/img/series/' . $datos['detalle']->content . '/b.jpg';
			// Rating
			$datos['rate_box'] = $this->_rating_poster_square($datos['detalle']->rate);

			// Sacamos los comentarios de la serie
			$datos['comentarios'] = $this->listado_comentarios($id_serie);

			// Sacamos los generos de la serie
			$datos['generos'] = $this->generos_contenido($id_serie);

			// Sacamos las recomendaciones de la serie
			$datos['relacionados'] = $this->_recomendaciones_series_perfil($id_serie);

			// Tipso de reporte de los comentarios
			$datos['tipo_reportes'] = $this->_tipo_reportes_comentarios();

			
			// Cargamos la vista si existe serie con esta ID
			$this->load->view('series/content_series', $datos);
		} else {
		// Mensaje de error si no existe con esta ID (cambiamos este mensaje por una vista)			
			echo "No existe esta serie";
				}




	}

	public function listar_capitulos(){
				$this->load->model('Series_model');
				$this->load->model('Viewed_model');
				$capitulos = $this->Series_model->listar_capitulos();
    			$vueltas = 0;
    				echo '<div class="row">';
					foreach($capitulos as $item){
						// Si vuelta = 0 mostramos el encabezado de la temporada
						if ($vueltas == 0){
		    				echo '<div class="row">';
		    				if ($item->seasonnumber === '0'){
		    				echo '<h4>Especiales</h4>';
		    				} else {
		    				echo '<h4>Temporada ' .$item->seasonnumber. '</h4>';

		    				}
		    				echo '</div>';							
						}
						$idcap = $item->episode;
						// Comprobamos si el cap está marcado como visto o no
						$ver_si_visto = $this->Viewed_model->ver_si_visto_capitulo($idcap);	
						if ($ver_si_visto > '0'){
						$colorbotonvisto = 'viewed.png';
						} else {
						$colorbotonvisto = 'no_viewed.png';
						}
						// Sacamos la imagen del capítulo, en caso de que no exista ponemos una imagen plantilla
						$imagen = base_url().'asset/img/series/'.$item->series.'/seasons/'.$item->seasonnumber.'/'.$item->number.'.jpg';
						$header_imagen = get_headers($imagen);
						if ($header_imagen[0] == 'HTTP/1.0 404 Not Found') {
							$imagen = base_url().'asset/img/layout/chapter.png';
						}


						$nombrecap = $item->title;
   						$nombrecaplimpio = preg_replace('/[^A-Za-z0-9\. -]/', '', $nombrecap);
    					$nombresinespacios = str_replace(" ","-",$nombrecaplimpio);
    					if ($vueltas % 4 == 0){
    						echo '</div>';
    						echo '<div class="row">';
    					}
    					echo '<div class="col-1 chapter">';
    					echo '<a class="cap-trigger" id="cap-trigger" href="' .base_url().'episode/'.$item->episode. '/'. $nombresinespacios. '">';
    					echo '<img class="chapter_image" src="'.$imagen.'" class="chapter_image">';
    					echo '<p class="chapter_date">'.$item->date.'</p>';

    					echo '<p class="chapter_title"> Capítulo '.$item->number.': '.$item->title.'</p>';
    					echo '</a>';

						echo '<div class="form_visto_caps">';
						echo '<form id="pendiente" class="cap_visto" action="'.base_url() .'home/vista" method="post">';
    					echo '<button type="submit" id="vista" class="viewed_button" name="vista" style="border: 0; background: transparent">
   						<img src="'.base_url().'asset/img/icons/'.$colorbotonvisto.'" alt="submit" class="viewed_button" />
    					</button>

						<input type="hidden" name="idcontenido"  id="idcontenido" value="'.$item->episode.'" />
						<input type="hidden" name="idseasonchapter"  id="idseasonchapter" value="temp'.$item->seasonnumber.'" />
						</form>';
						echo '</div>';
						echo '</div>';
					$vueltas++;

					}
					echo '</div>';
				}

	
	

	public function perfil_capitulos($id_capitulo){
				$this->load->model('Series_model');
				$this->load->model('Enlaces_model');

		// Sacamos los comentarios relacionados con este episodio
		$datos['comentarios'] = $this->listado_comentarios($id_capitulo);
		$datos['tipo_reportes'] = $this->_tipo_reportes_comentarios();

		// Sacamos la info relacionada con el episodio vía id
		$id_capitulo_vacio = $this->security->xss_clean($id_capitulo);
		$datos['detalle'] = $this->Series_model->detalle_capitulos($id_capitulo_vacio);
		$numeroresultado = count($datos['detalle']);
		$datos['background'] = $this->background_image->background_image_enlace_serie($datos['detalle']->series);

		// Sacamos los enlaces del capítulo
		$datos['enlaces_visionado_online'] =  $this->enlaces_visionado_online($id_capitulo);
		$datos['enlaces_descarga_directa'] =  $this->enlaces_descarga_directa($id_capitulo);

		// Generamos los datos necesarios para el formulario (tipos de calidad, idiomas y tipos de enlaces)
		 $datos['lista_calidad'] = $this->lista_calidad();
		 $datos['lista_idiomas'] = $this->lista_idiomas();
		 $datos['lista_tipo_enlaces'] = $this->lista_tipo_enlaces();

		// Sacamos tipos de reportes para los enlaces
		 $datos['tipo_reportes_enlaces'] = $this->_tipo_reportes_enlaces();
		$this->load->view('series/content_capitulos', $datos);
	}



			public function perfil_peliculas($id_peli){
		$this->load->model('Films_model');
		$this->load->model('Listas_model');
		$html='';

		// Hacemos esta pequeña comprobación para saber si el usuario está logeado o no y establecer la variable idusuario que usaremos en los comentarios a la hora de generar los padres e hijos.			
					if ($this->ion_auth->logged_in())
		{
		$datos['idusuario'] = $this->ion_auth->user()->row()->id;
		}	else {
			$datos['idusuario'] = "";
		}
		$datos['listas'] = $this->listas_detalle_contenido();

		// Sacamos la info relacionada con la película vía id
		$id_peli_vacio = $this->security->xss_clean($id_peli);
		$datos['detalle'] = $this->Films_model->detalle_peliculas($id_peli_vacio);
		$numeroresultado = count($datos['detalle']);

		// Con este if comprobamos que realmente existe una serie con esta id, si no luego nos saltarán objetos y foreaches inválidos
		if ($numeroresultado === 1){
			
		// Establecemos el color de los botones según el estado que ha marcado el usuario previamente
		$datos['colorbotonfavorito'] = $this->botones_mostrar_favorito();
		$datos['colorbotonpendiente'] = $this->botones_mostrar_pendiente();
		$datos['colorbotonvista']  = $this->botones_mostrar_vista();

		// Carátula y banner
		$datos['caratula'] = base_url() . 'asset/img/films/' . $datos['detalle']->content . '/c.png';
		$datos['banner'] = base_url() . 'asset/img/films/' . $datos['detalle']->content . '/b.png';

		// Sacamos los comentarios de la película
			$datos['comentarios'] = $this->listado_comentarios($id_peli);

		// Sacamos los generos de la serie
		$datos['generos'] = $this->generos_contenido($id_peli);

				$this->load->view('pelicula', $datos);
				} else {
					echo "No existe esta película";
				}
	}


			public function perfil_peliculas_enlaces($id_peli){
				echo $id_peli;
			$this->load->model('Enlaces_model');
			$this->load->model('Films_model');
		$id_peli_vacio = $this->security->xss_clean($id_peli);
		$datos['detalle'] = $this->Films_model->detalle_peliculas($id_peli_vacio);
		$numeroresultado = count($datos['detalle']);
		$numeroresultado = count($datos['detalle']);

		// Con este if comprobamos que realmente existe una serie con esta id, si no luego nos saltarán objetos y foreaches inválidos
		if ($numeroresultado === 1){
		// Sacamos los enlaces del capítulo
		$datos['enlaces_visionado_online'] =  $this->enlaces_visionado_online($id_peli);
		$datos['enlaces_descarga_directa'] =  $this->enlaces_descarga_directa($id_peli);

		// Generamos los datos necesarios para el formulario (tipos de calidad, idiomas y tipos de enlaces)
		 $datos['lista_calidad'] = $this->lista_calidad();
		 $datos['lista_idiomas'] = $this->lista_idiomas();
		 $datos['lista_tipo_enlaces'] = $this->lista_tipo_enlaces();

		$this->load->view('pelicula_enlaces', $datos);	
		} else {
			echo "No existe esta película";
		}			
			}	

		public function _recomendaciones_series_perfil($content){
			$this->load->model('Series_model');
			$contenido_recomendado = $this->Series_model->Recomendaciones_series($content);
			$html = '';
		foreach ($contenido_recomendado as $item){
			// Sacamos los detalles de la serie recomendada
			$serie = $this->Series_model->detalle_series($item->cont_rel);
			// Sacamos el color de la caja del rating
			$rate_box = $this->_rating_poster_square($serie->rate);
			// Reducimos el plot a 300 carácteres
			$plot_reducido = substr($serie->plot, 0, 260);
			// Sacamos el corner
			$corner = $this->_corner_generator($serie->series);
			$url = base_url() . 'series/' .$serie->series. '/'. urlencode(str_replace(' ','-',$serie->title));



			$html.='


		    				<div>
		    					<a class="poster_link" href="'.$url.'">
									<div class="poster_slide">
										<p class="rating_poster '.$rate_box.'"><span class="rating_number">'.$serie->rate.'</span></p>
										
										<img src="'.base_url().'asset/img/series/'.$serie->series.'/c.jpg">
										'.$corner.'
										<p class="infoposter">
										<span class="infoposter_title">'.$serie->title.'</span></br>
											<p class ="infoposter_plot">
											'.$plot_reducido.'...
											</p>
										</p>
									</div>
			   					</a>
		   					</div>';
						
					
		}

		return $html;
		}	

/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCION INFO EXTRA SERIES Y PELÍCULAS (generos, relacionado....)
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con info extra de los contenidos
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
public function generos_contenido($id_contenido){
				$this->load->model('Genres_model');
				$html = '';
				$generos = $this->Genres_model->mostrar_generos_contenido($id_contenido);
				foreach ($generos as $item){
					$html.= $item->name . ' ';
				}
				return $html;
}

private function _lista_generos(){
				$this->load->model('Genres_model');
				$generos = $this->Genres_model->lista_generos();
				$html ='';
				foreach ($generos as $item){
					$html.=  $item->name . '</br>';
				}
return $html;
}

public function _crear_listas_url(){
	if ($this->ion_auth->logged_in()){
		$html = '<div class="create-list"><a href="/profile"><h5 class="list-title">Crear nueva lista</h5></a></div>';
	} else {
		$html = '';
	}
return $html;

}

/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES RELACIONADAS CON LOS ENLACES
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con los ENLACES
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	

		public function perfil_enlaces($key=NULL){
				$this->load->model('Enlaces_model');
				$this->load->model('Ion_auth_model');
				$this->load->library('Mostrar_avatar');
				$this->load->library('Comprobar_uploader');

		// Sacamos la info relacionada con el enlace vía key
		$key_enlace_vacio = $this->security->xss_clean($key);
		$datos['detalle'] = $this->Enlaces_model->perfil_enlaces_series($key_enlace_vacio);

		// Comprobamos si es uploader
		$res = $this->Ion_auth_model->comprobar_uploader($datos['detalle']->id_user);
		$datos['uploader'] = $this->comprobar_uploader->comprobar_uploader($res);

		// Sacamos los campos de donación del usuario
		$datos['donaciones_iconos'] = $this->_donaciones_iconos($datos['detalle']->id_user, $datos['detalle']->username);

		$numeroresultado = count($datos['detalle']);
		if($numeroresultado != 0){

        $datos['avatar'] = $this->mostrar_avatar->mostrar_avatar($datos['detalle']->id_user, $tipo = "thumb");
		$datos['background'] = $this->background_image->background_image_enlace_serie($datos['detalle']->series);

		// Si el usuario está logeado comprobamos si ya ha visto este capítulo previamente, en caso de que ya lo haya visto no añadimos nada, en caso de que sea la primera vez que lo ve lo añadimos a la db
		if ($this->ion_auth->logged_in())
		{
			$this->load->model('Viewed_model');
			$ver_si_visto = $this->Viewed_model->ver_si_visto_capitulo($datos['detalle']->episode);
			if ($ver_si_visto === 0){
			$this->Viewed_model->marcar_vista_automatico($datos['detalle']->episode);
			}

		}		

		$this->load->view('enlace', $datos);
		// Si no es una serie comprobamos si es una película
		} else { 
		$datos['detalle'] = $this->Enlaces_model->perfil_enlaces_peliculas($key_enlace_vacio);
		$numeroresultado = count($datos['detalle']);
		if($numeroresultado != 0){
		// Película	
        $datos['avatar'] = $this->mostrar_avatar->mostrar_avatar($datos['detalle']->id_user, $tipo = "thumb");
		$datos['background'] = $this->background_image->background_image_enlace_pelicula($datos['detalle']->content);
		$this->load->view('enlace', $datos);

		} else {
			echo "Este enlace no existe";
		}

}
}

	public function listar_enlaces($enlaces){
				$html = '';
				$this->load->model('Ion_auth_model');
				$this->load->library('Comprobar_uploader');
				$this->load->library('Links_images');
				foreach ($enlaces as $item){
				// Comprobamos si el usuario es un uploader para ponerle un iconito
					$res = $this->Ion_auth_model->comprobar_uploader($item->iduser);
					$uploader = $this->comprobar_uploader->comprobar_uploader($res);

				// Sacamos el nombre del host para pasar la variable al contorlador detectar_imagen_servidor_video para establecer la imagen				
						$domain = $item->url;
						$imagen_server = $this->links_images->detectar_imagen_servidor_video($domain);
				// Sacamos todo lo relacionado con el tema de los reportes
						$idlink = $item->idlink;
				$totalreportes = $this->Enlaces_model->Ver_si_reportado($idlink);
            	$botonreporte = '<input type="image" value=' . $item->key . ' class="report_icon_link" src="'.base_url().'asset/img/icons/report.png"';	
            	$html.= "<a href=".base_url().'link/'.$item->key.'/'.$idlink.">";	
                $html.= '
						<tr>
							<td class="host_image"><a href='.base_url().'link/'.$item->key.'/'.$idlink.'><img src="'.base_url().'asset/img/icons/hosts/' .$imagen_server.'"></a></td>
						    <td><a href='.base_url().'link/'.$item->key.'/'.$idlink.'><img src="'.base_url().'asset/img/icons/flags/' .$item->idioma_audio.'.png"></a></td>
						    <td><a href='.base_url().'link/'.$item->key.'/'.$idlink.'><img src="'.base_url().'asset/img/icons/flags/' .$item->idioma_sub.'.png"></a></td>

						    <td><a href='.base_url().'link/'.$item->key.'/'.$idlink.'>'.$item->quality.'</a></td>
						    <td><a href='.base_url().'link/'.$item->key.'/'.$idlink.'>'.$item->username . ' ' . $uploader.'</a></td>
						    <td>'.$botonreporte.'<div class="total_report">('.$totalreportes.')</div></td>
						    </a>
						</tr>
                ';


				

				}
				return $html;
	}
	function reportar_enlaces(){
	$this->load->model('Enlaces_model');
	$enlace = $this->Enlaces_model->info_key($this->input->post('key_link_input'));
	$id_enlace = $enlace->id;
		if ($id_enlace == NULL){
			echo "error";
		} else {
			echo json_encode($this->Enlaces_model->reportar_enlace($id_enlace));	
		}
}

public function _tipo_reportes_enlaces(){
	$this->load->model('Enlaces_model');
	$tipo_reportes_enlaces = $this->Enlaces_model->tipo_reportes_enlaces();
	$html = '';
	foreach ($tipo_reportes_enlaces as $item){
		$html.= '<option value="'.$item->id.'">'.$item->title.'</option>';
	}
	return $html;
}
	// Con estas funciones dividimos los tipos de enlaces, visionado online, descarga directa...
	public function enlaces_visionado_online($content){
	$enlaces = $this->Enlaces_model->lista_enlaces_capitulo_visionado_online($content);
	$html = $this->listar_enlaces($enlaces);
	return $html;
	}
	public function enlaces_descarga_directa($content){
	$enlaces = $this->Enlaces_model->lista_enlaces_capitulo_descarga_directa($content);
	$html = $this->listar_enlaces($enlaces);
	return $html;
	}

	
	

	public function lista_calidad(){
		$this->load->model('Enlaces_model');
		$lista_calidad = $this->Enlaces_model->lista_calidad();
		return $lista_calidad;
	}

	public function lista_idiomas(){
	$this->load->model('Enlaces_model');
	$lista_idiomas = $this->Enlaces_model->lista_idiomas();
	return $lista_idiomas;
	}

	public function lista_tipo_enlaces(){
	$this->load->model('Enlaces_model');
	$lista_tipo_enlaces = $this->Enlaces_model->lista_tipo_enlaces();
	return $lista_tipo_enlaces;
	}

	public function url_redirect(){
	$this->load->model('Enlaces_model');
	$url = $this->Enlaces_model->url_redirect();
	$datos['total'] = count($url);

    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $datos['url'] = "http://" . $url;
    } else {
	$datos['url'] = $url;
	}
	$this->load->view('hide_url/url', $datos);
}

	public function subir_enlace(){
		$this->form_validation->set_rules('url', 'url', 'trim|required|min_length[5]|valid_url|is_unique[links.url]');
		$redirect = $this->input->post('redirect');
			if($this->ion_auth->logged_in()){

		 		if ($this->form_validation->run() == FALSE){
		 			$mensaje = "Este enlace ya ha sido añadido con anterioridad";
		 			$this->session->set_flashdata('error', $mensaje);

		 			redirect($redirect, 'refresh');


			 	} else {
					$this->load->model('Enlaces_model');
					echo $this->Enlaces_model->subir_enlace();
					$mensaje = "¡Has añadido el enlace correctamente!";
		 			$this->session->set_flashdata('correcto', $mensaje);

		 			redirect($redirect, 'refresh');

		 		}
			} else {
					$mensaje = "Debes entrar en tu cuenta de usuario para añadir un enlace";
		 			$this->session->set_flashdata('error', $mensaje);

		 			redirect($redirect, 'refresh');
			}

	}

		public function _donaciones_iconos($iduser,$nombreuser){
			$iconos_donaciones = $this->Ion_auth_model->donaciones_user($iduser);
			$html = '';			
			if ($iconos_donaciones) {



			$html ='Subir contenido de calidad conlleva muchísimo tiempo y dedicación. Si te gustan los aportes del usuario '.$nombreuser.' considera agradecérselo con una donación.';
			$html.= '<center><div class = "donations">';
			foreach ($iconos_donaciones as $item){
			$img_name = strtolower($item->name) . '.png';
			// Si es paypal añadimos un a href
			if ($item->type === '1'){
			$html.= '<a href="'.$item->url.'" target="_blank"><img src="'.base_url().'asset/img/icons/'.$img_name.'" class="icondonation '.strtolower($item->name).'"> </a> ';
			// Si es Bitcoin no añadimos enlace ya que abriremos un modal box que también generamos aquí
			} else {
			$html.= '<a href="#"><img src="'.base_url().'asset/img/icons/'.$img_name.'" class="icondonation '.strtolower($item->name).'"></a> ';
			$html.='<div id ="donation_bitcoin_modal" title="Donación vía Bitcoins">';
			$html.='<center><h5>Código QR</h5></center>';
			$html.='<center><h5>Wallet</h5></center>';
			$html.='<input type="text" value="'.$item->url.'"></input><input type="button" value="Copiar"></input>';
			$html.='<center><h5>Blockchain URL</h5></center>';
			$html.='<a href="https://blockchain.info/payment_request?address='.$item->url.'" target="_blank">'.$item->url.'</a>';
			$html.='</div>';
			}

			}
			$html.='</br>La donación integra la recibirá el usuario</center>';
			$html.='</div>';
			}

			return $html;
		}


/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES DE COMENTARIOS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con los comentarios en la ficha de cada contenido
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/

		
		function listado_comentarios($id_serie){
			// NO ES LA MEJOR FORMA DE HACERLO PUESTO QUE HAGO MUCHAS MINICONSULTAS PARA SACAR LOS HIJOS, HABRÍA QUE SACAR UN ARRAY CON TODO Y LUEGO IR RECORRIÉNDOLO

			$this->load->model('Comments_model');
			$this->load->library('Mostrar_avatar');
			$this->load->model('Ion_auth_model');
			$this->load->library('Comprobar_uploader');
			$padres = $this->Comments_model->todos_comentarios_padres($id_serie);

			$html = '
					<div class="comments-container">
					<ul id="comments-list" class="comments-list">';
			if ($padres == FALSE){
				$html.= '<h5><center> No hay ninguna crítica, ¡Anímate a escribir una!<center></h5><br>';
			} else {
			foreach ($padres as $item){
				// Datos de los comentarios padre
				// Avatar
				$avatar_usuario = $this->mostrar_avatar->mostrar_avatar($item->user, $tipo = "thumb");
				// Boton de reportar
            	$botonreporte = '<input type="image" value=' . $item->id . ' class="reporteicon" src="'.base_url().'asset/img/icons/report.png"';
            	// Iconos
           		$commenticons = parse_smileys($item->comment, base_url() . 'asset/img/icons/smileys');
           		// Link al perfil
            	$perfilusuariolink = '<a href=' . base_url() . 'users/' . $item->id_user . '/' . $item->username . '>' . $item->username . '</a>';
            	// Icono de uploader
				$res = $this->Ion_auth_model->comprobar_uploader($item->id_user);
				$uploader = $this->comprobar_uploader->comprobar_uploader($res);
				// Votos
        		$accionformvotar = base_url() . 'home/votar_comentario';
        		$color_si_votado = 'btn_like';
        		$icono_boton = 'checkmark';
        			if ($this->ion_auth->logged_in()){
        				$datos['versivotado'] = $this->Comments_model->Ver_si_votado($item->id);
						$numeroresultado = count($datos['versivotado']);
							if ($numeroresultado === 1){
			                        $color_si_votado = 'btn_like_true';
			                        $icono_boton = 'heart';
							}
					}
				$total_votos = '';
				if($item->votes === '1'){
                    $total_votos= '<p class="like_total_message">A ' .$item->votes. ' persona le ha gustado esta crítica</p>';
	
                }
                if ($item->votes > '1'){
                    $total_votos= '<p class="like_total_message">A ' .$item->votes. ' personas les ha gustado esta crítica</p>';
                }
                // Fecha
                Carbon\Carbon::setLocale('es');
                $fecha = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->date);
    			$fecha_humana = $fecha->diffForHumans();


				$html.= '<li>';
				// Generamos los comentarios padres
				$html.=	'<div class="comment-main-level">';
				$html.= '<div class="comment-avatar"><img src="'.$avatar_usuario.'" alt=""></div>';
				$html.= '
					<div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name by-author">'.$perfilusuariolink.' '.$uploader.'</h6>
							<span>'.$fecha_humana.'</span>
							<i class=""><a  href="#comment_form" class="reply" value="'.$item->id.'">Responder</a></i>
							<i class="">'.$botonreporte.'</i>

						</div>
						<div class="comment-content">
							'.$commenticons.'
						</div>
						<div class="comment-footer">
							<div class="votos">

								<form id="megusta'.$item->id.'" class="me_gusta_form" action="'.$accionformvotar.'" method="post">
                    			<input type="hidden" name="idmensaje"  id="idmensaje" value="'.$item->id.'" />
                    			<button class="'.$color_si_votado.'" id="botongustar" name="botongustar"><i class="icon-'.$icono_boton.'"></i> Me gusta</button>
                    			</form>
                    			<div class="total_votes">
                    			<span>
                    			'.$total_votos.'
                    			</span>
                    			</div>
                    		</div>

						</div>
					</div>
				</div>';
				$html.= '<ul class="comments-list reply-list">';
				// Generamos los comentarios hijos
				if($item->parent_comment == 0 ){
					$hijos = $this->Comments_model->todos_comentarios_hijos($item->id,$id_serie);
							foreach ($hijos as $item){
	// Datos de los comentarios padre
					// Avatar
					$avatar_usuario = $this->mostrar_avatar->mostrar_avatar($item->user, $tipo = "thumb");
					// Boton de reportar
	            	$botonreporte = '<input type="image" value=' . $item->id . ' class="reporteicon" src="'.base_url().'asset/img/icons/report.png"';
	            	// Iconos
	           		$commenticons = parse_smileys($item->comment, base_url() . 'asset/img/icons/smileys');
	           		// Link al perfil
	            	$perfilusuariolink = '<a href=' . base_url() . 'users/' . $item->id_user . '/' . $item->username . '>' . $item->username . '</a>';
	            	// Icono de uploader
					$res = $this->Ion_auth_model->comprobar_uploader($item->id_user);
					$uploader = $this->comprobar_uploader->comprobar_uploader($res);
					// Votos
	        		$accionformvotar = base_url() . 'home/votar_comentario';
	        		$color_si_votado = 'btn_like';
	        			if ($this->ion_auth->logged_in()){
	        				$datos['versivotado'] = $this->Comments_model->Ver_si_votado($item->id);
							$numeroresultado = count($datos['versivotado']);
							$icono_boton = 'checkmark';
								if ($numeroresultado === 1){
			                        $color_si_votado = 'btn_like_true';
			                        $icono_boton = 'heart';
								}
						}
					$total_votos = '';
					if($item->votes === '1'){
	                    $total_votos= '<p class="like_total_message">A ' .$item->votes. ' persona le ha gustado esta crítica</p>';
		
	                }
	                if ($item->votes > '1'){
	                    $total_votos= '<p class="like_total_message">A ' .$item->votes. ' personas les ha gustado esta crítica</p>';
	                }
	                // Fecha
	                Carbon\Carbon::setLocale('es');
	                $fecha = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->date);
	    			$fecha_humana = $fecha->diffForHumans();

								$html.= '<li>
						<div class="comment-avatar"><img src="'.$avatar_usuario.'" alt=""></div>
						<div class="comment-box">
							<div class="comment-head">
							<h6 class="comment-name by-author">'.$perfilusuariolink.' '.$uploader.'</h6>
							<span>'.$fecha_humana.'</span>
							<i class="">'.$botonreporte.'</i>
							</div>
							<div class="comment-content">
								'.$commenticons.'
							</div>
							<div class="comment-footer">
								<div class="votos">

									<form id="megusta'.$item->id.'" class="me_gusta_form" action="'.$accionformvotar.'" method="post">
	                    			<input type="hidden" name="idmensaje"  id="idmensaje" value="'.$item->id.'" />
	                    			<button class="'.$color_si_votado.'" id="botongustar" name="botongustar"><i class="icon-'.$icono_boton.'"></i> Me gusta</button>
	                    			</form>
	                    			<div class="total_votes">
	                    			<span>
	                    			'.$total_votos.'
	                    			</span>
	                    			</div>
	                    		</div>

							</div>
					</li>';
						}
				}
				$html.= '</ul></li>';
			}
			}
				$html.= '</ul></div>';
				return $html;
		}
	

function votar_comentario(){
			if ($this->ion_auth->logged_in())
		{
	$idmensaje = $this->input->post('idmensaje');
	$this->load->model('Comments_model');
	$this->Comments_model->votar_comentario();
	$data = array(
			"resultado" => 'correcto',
			);
 			echo json_encode($data);
} else {
	$data = array(
			"resultado" => 'error',

			);
 			echo json_encode($data);
}
}



function nuevo_comentario($id_serie){
    $this->load->model('Comments_model');
	$this->load->model('Series_model');

        
        $id_serie_vacio = $this->security->xss_clean($id_serie);
		$data['detalle'] = $this->Series_model->detalle_series($id_serie_vacio);
if (!$this->ion_auth->logged_in())
		{

	$data = array(
			"resultado" => 'error',
			"mensaje"   => 'Para comentar debes entrar en tu cuenta.',

			);
 			echo json_encode($data);
		}
        //set validation rules       
        $this->form_validation->set_rules('comment', 'Mensaje', 'required|trim|htmlspecialchars');
        if ($this->form_validation->run() == FALSE) {
            // if not valid load comments
	$data = array(
			"resultado" => 'error',
			"mensaje"   => validation_errors(),

			);
 			echo json_encode($data);
        } else {
            //if valid send comment to admin to tak approve
            		$id_user = $this->ion_auth->user()->row()->id;
            $this->Comments_model->añadir_comentario($id_user);
            $this->session->set_flashdata('validacion', '¡Mensaje enviado!');
            	$data = array(
			"resultado" => 'correcto',
			"mensaje"   => 'Mensaje enviado.',

			);
 			echo json_encode($data);
        } 
    }
function borrar_comentario()
{
	$idmensaje = $this->input->post('idmensaje');
	$this->load->model('Comments_model');
	$this->Comments_model->borrar_comentario();

}
function reportar_comentario()
{
	$this->load->model('Comments_model');
	$this->Comments_model->reportar_comentario();
	echo json_encode($this->Comments_model->reportar_comentario());


}

function _tipo_reportes_comentarios(){
	$this->load->model('Comments_model');
	$tipo_reportes = $this->Comments_model->tipos_reportes();
	$html= '<select name="motivo">';
	foreach ($tipo_reportes as $item){
		$html.= '<option value="'.$item->type.'">'.$item->title.'</option>';
	}
	$html.= '</select>';
	return $html;

}
/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA MARCAR COMO VISTO/PENDIENTE/FAVORITO...
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con el marcaje de los contenidos por los usuarios
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
	public function seguir()
	{
					$this->load->model('Follow_model');

$versisigue = $this->Follow_model->ver_si_sigue();

	if (!$this->ion_auth->logged_in()){
		$databoton = array(
			'resultado' => 'error',

			);
		   	echo json_encode($databoton);

	} elseif ($versisigue > 0) {
						$this->Follow_model->desmarcar_seguido();
							$databoton = array(
			'resultado' => 'desmarcado',

			);
		   	echo json_encode($databoton);


	} else {
			$this->Follow_model->marcar_seguido();
				$databoton = array(
			'resultado' => 'marcadoseguir',
			'mensaje'   => '¡Siguiendo esta serie!',

			);
		   	echo json_encode($databoton);
	}

	}

	public function favorito()
	{
					$this->load->model('Favourites_model');

$versifavorita = $this->Favourites_model->ver_si_favorito();

	if (!$this->ion_auth->logged_in()){
		$databoton = array(
			'resultado' => 'error',

			);
		   	echo json_encode($databoton);

	} elseif ($versifavorita > 0) {
						$this->Favourites_model->desmarcar_favorito();

	$databoton = array(
			'resultado' => 'desmarcado',

			);
		   	echo json_encode($databoton);

	} else {
			$this->Favourites_model->marcar_favorito();
	$databoton = array(
			'resultado' => 'marcadofav',
			'mensaje'   => '¡Marcado como favorito!',

			);
		   	echo json_encode($databoton);
	}

	}

	public function pendiente()
	{
					$this->load->model('Pending_model');

$versipendiente = $this->Pending_model->ver_si_pendiente();

	if (!$this->ion_auth->logged_in()){
		$databoton = array(
			'resultado' => 'error',

			);
		   	echo json_encode($databoton);

	} elseif ($versipendiente > 0) {
								$this->Pending_model->desmarcar_pendiente();
	$databoton = array(
			'resultado' => 'desmarcado',

			);
		   	echo json_encode($databoton);

	} else {
			$this->Pending_model->marcar_pendiente();
	$databoton = array(
			'resultado' => 'marcadopend',
			'mensaje'   => '¡Marcado como pendiente!',

			);
		   	echo json_encode($databoton);
	}

	}

	public function vista()
	{
					$this->load->model('Viewed_model');

$versivista = $this->Viewed_model->ver_si_vista();

	if (!$this->ion_auth->logged_in()){
	$databoton = array(
			'resultado' => 'error',

			);
		   	echo json_encode($databoton);

	} elseif ($versivista > 0) {
						$this->Viewed_model->desmarcar_vista();

	$databoton = array(
			'resultado' => 'desmarcado',

			);
		   	echo json_encode($databoton);

	} else {
			$this->Viewed_model->marcar_vista();
	$databoton = array(
			'resultado' => 'marcadovista',
			'mensaje'   => '¡Marcado como vista!',

			);
		   	echo json_encode($databoton);	}

	}


	// Relacionado con los botones de seguir, favorito, pendiente y vista

public function botones_mostrar_seguir(){
				$this->load->model('Follow_model');

		$versisigue = $this->Follow_model->ver_si_sigue();
		if ($versisigue > '0'){
			$datos['colorbotonseguir'] = ' button_following';

		} else {
			$datos['colorbotonseguir'] = '';

		}
		return $datos['colorbotonseguir'];
}

public function botones_mostrar_favorito(){
			$this->load->model('Favourites_model');

		$versifavorito = $this->Favourites_model->ver_si_favorito();
		if ($versifavorito > '0'){
			$datos['colorbotonfavorito'] = ' button_favourite';
		} else {
			$datos['colorbotonfavorito'] = '';
		}
				return $datos['colorbotonfavorito'];

}
public function botones_mostrar_pendiente(){
			$this->load->model('Pending_model');

		$versipendiente = $this->Pending_model->ver_si_pendiente();
		if ($versipendiente > '0'){
			$datos['colorbotonpendiente'] = ' button_pending';
		} else {
			$datos['colorbotonpendiente'] = '';
		}
						return $datos['colorbotonpendiente'];

}
public function botones_mostrar_vista(){
			$this->load->model('Viewed_model');

		$versivista = $this->Viewed_model->ver_si_vista();
		if ($versivista > '0'){
			$datos['colorbotonvista'] = ' button_viewed';
		} else {
			$datos['colorbotonvista'] = '';
		}
						return $datos['colorbotonvista'];


}

/*
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| FUNCIONES PARA LAS LISTAS
| A partir de esta línea empiezan todas las funciones que están
| relacionadas con las listas de los usuarios
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
| -------------------------------------------------------------------
*/
public function listas(){
	$this->load->model('Listas_model');
	if($this->input->post('accion') === 'Eliminar'){
		$this->Listas_model->borrar_contenido_lista();

 			
	} 

	if($this->input->post('accion') === 'Añadir') {
		$this->Listas_model->añadir_contenido_lista();

 			
	}

}

public function perfil_listas($idlista){
			$datos['background'] = $this->background_image->background_image();

		$this->load->model('Listas_model');
		$this->load->library('mostrar_avatar');
		$this->load->model('Ion_auth_model');
		$this->load->library('Comprobar_uploader');
// Sacamos la info relacionada con la serie vía id
		$id_lista_vacio = $this->security->xss_clean($idlista);
		$datos['detalle'] = $this->Listas_model->perfil_listas($id_lista_vacio);
		$numeroresultado = count($datos['detalle']);
		if ($this->ion_auth->logged_in()){
			$idusersesion = $this->ion_auth->user()->row()->id; } else { $idusersesion='';}
								// Este if englobará todo lo que viene, lo realizamos para saber
								// si la lista existe o no
								if ($numeroresultado !== 0){

		$iduserlista = $datos['detalle']->user;
		// Información del usuario
		$nombreuser = $this->ion_auth->user($iduserlista)->row()->username;
		$datos['avatar'] = $this->mostrar_avatar->mostrar_avatar($iduserlista, 'thumb');
		$datos['perfilusuariolink'] = '<a href=' . base_url() . 'users/' . $datos['detalle']->user . '/' . $nombreuser . '>' . $nombreuser . '</a>';
		$res = $this->Ion_auth_model->comprobar_uploader($iduserlista);
		$datos['uploader'] = $this->comprobar_uploader->comprobar_uploader($res);

		$datos['listaseries'] = $this->Listas_model->contenido_listas_series();
		$datos['listapeliculas'] = $this->Listas_model->contenido_listas_peliculas();



		// Para hacer el background
		$total_contenido = array_merge($datos['listaseries'], $datos['listapeliculas']);
		$datos['background'] = $this->background_lista($total_contenido);

		$datos['botoneditar'] = '';
		$datos['formularioeditar'] = '';



					$publico='';
					if($datos['detalle']->state === '0'){ 
					$publico='checked';}

					$privado='';
					if($datos['detalle']->state === '1'){ 
					$privado='checked';}
		// Comprobamos si la sesión y el dueño de la lista son los mismos para darles la posibilidad de editar su lista
		        						if ($this->ion_auth->logged_in())
									{
		if($this->ion_auth->user()->row()->id === $this->ion_auth->user($iduserlista)->row()->id){
			$datos['botoneditar'] =  ' <a href="#"><input type="image" id="botoneditar" class="botoneditar" name="botoneditar" src="'.base_url().'asset/img/icons/settings-gears.png"  /></a>';

			$datos['formularioeditar'] ='<form id="editarlista" class="editarlista" action="'.base_url().'users/editar_lista" method="post">
</br><b>Nombre de la lista</b></br>
 <input type="text" name="nombre" value="'.$datos['detalle']->name.'" class="nombre"><br></br>
<b>Descripción</b></br>
 <textarea name="descripcion" maxlength="500" class="descripcion" rows="5" cols="25">'.$datos['detalle']->description.'</textarea></br></br>
 <input type="radio" name="privacidad" value="0" '.$publico.' > Lista pública
 </br><input type="radio" name="privacidad" value="1" '.$privado.'> Lista privada 
 </br>
 <input type="hidden" name="idlista" value="'.$datos['detalle']->list.'" class="idlista">
 </br><input type="submit" value="Editar lista" class="editar"  id="editar" name="editar" />
 </form>

			';

		}
		}
									// Seguir lista
        							// Cambiamos el color del botón si sigue la lista
								$lista = $datos['detalle']->list;
        						$clase_boton = 'follow_button_list';        						
        						if ($this->ion_auth->logged_in())
									{
        						$datos['versiseguido'] = $this->Listas_model->Ver_si_seguido($lista);
								$numeroresultado = count($datos['versiseguido']);
								if ($numeroresultado === 1){
			                                        $clase_boton = 'following_button_list';
									}} 
									
					// Div de la parte de seguimiento
					$datos['siguiendo'] = '';
        					$accionformseguir = base_url() . 'home/seguir_lista';
                    $datos['siguiendo'].= '<form id="seguirform'.$datos['detalle']->list.'" class="seguir_form" action="'.$accionformseguir.'" method="post">
                    <input type="hidden" name="lista"  id="lista" value="'.$datos['detalle']->list.'" />
						<button class="'.$clase_boton.'" type="submit" id="seguirbutton" name="seguirbutton" value="seguir" ></button>
                    </form>';
                        
										$datos['siguiendo_numero'] = '';
                    					if($datos['detalle']->follow === '1'){
                    					$datos['siguiendo_numero'].= $datos['detalle']->follow. ' persona está siguiendo esta lista';
	
                    					}
                    					if ($datos['detalle']->follow > '1'){
                                        $datos['siguiendo_numero'].= $datos['detalle']->follow. ' personas están siguiendo esta lista';
                                        }
                          // Mostramos la lista si NO es privada
                          if($datos['detalle']->state === '0') {            
							$this->load->view('lista', $datos);
								} 
							// Mostraremos la lista en caso de que SEA PRIVADA y pertenezca al usuario que tenga la sesión abierta	
							if($datos['detalle']->state === '1' && $iduserlista === $idusersesion) {            
							$this->load->view('lista', $datos);
								} 

							// Mensaje de error si la lista es privada y no pertenece al usuario de la sesión
							if($datos['detalle']->state === '1' &&  $iduserlista !== $idusersesion) {
								$usuario_link = '<a href="'.base_url().'users/'.$iduserlista.'/'.$nombreuser.'">'.$nombreuser.'</a>';

							$datos['mensaje'] = "LISTA PRIVADA </br> Esta lista pertenece al usuario " . $usuario_link . " y solo él puede ver el contenido.";
							$this->load->view('mensaje_generico_plano', $datos);

								} 
								} else {

							$datos['mensaje'] = "No existe ninguna lista con esta ID.";
							$this->load->view('mensaje_generico_plano', $datos);
}

}
public function background_lista($total_contenido){
	shuffle($total_contenido);
	$contenido_random='';
	foreach ($total_contenido as $item){
		$contenido_random.= $item->content;
		break;
	}
	$tipo_contenido = $this->Listas_model->background($contenido_random);
	return $tipo_contenido;
}



public function listas_detalle_contenido(){
			if ($this->ion_auth->logged_in()){

		$result = $this->Listas_model->todas_listas_perfil();
		$html = '';
		foreach ($result as $item){
            	$idlista = $item->list;
            	// Ver si ya ha añadido el contenido a su lista
            	$datos['versiañadido'] = $this->Listas_model->Ver_si_añadido($idlista);
								$numeroresultado = count($datos['versiañadido']);
								if ($numeroresultado > 0){
									                $botontexto = 'Eliminar';
 													$botonstyle = 'background-color:#B40404;';
               									
								} 
								if($numeroresultado === 0) {
													$botontexto = 'Añadir';
													$botonstyle = 'background-color:#04B404;';
										}


   	$nombrelista = $item->name;
   	$nombrelistaslimpio = preg_replace('/[^A-Za-z0-9\. -]/', '', $nombrelista);
    $nombresinespacios = str_replace(" ","-",$nombrelistaslimpio);
    $linklistas = '<a href=' . base_url() . 'list/' . $item->list . '/' . $nombresinespacios . '>' . $item->name . '</a>';

 $html .= '<br><center><table>
  <tr>
    <th>Nombre</th>
    <th>Añadir/Eliminar</th>
    <th>Seguidores</th>
  </tr>
  <tr>
    <td>'.$linklistas.'</td>
    <td>
		<form id="lista'.$idlista.'" class="lista" action="'.base_url().'home/listas" method="post">
 		<input type="hidden" name="listaid"  id="listaid" value="'.$idlista.'" />
  		<input type="hidden" name="idcontenido"  id="idcontenido" value="'.$this->uri->segment(2).'" />
 		<input type="hidden" name="accion" class="accion"  id="accion" value="'.$botontexto.'" />
 		<button id="botonlistas" name="botonlistas" class="button_list_add" style="'.$botonstyle.'" value="botonlistas"">'.$botontexto.'</button>
 		</form>	
    </td>
    <td>'.$item->follow.'</td>
  </tr>
 
</table></center>';

               




		}
		}
 // Comprobamos si el usuario está loggeado para mostrar sus listas, en caso contrario mostramos un mensaje
if ($this->ion_auth->logged_in())
		{
		$datos['listas'] = $html;
	} else {


		$html = '<br><h4>¿Nuevo por aquí?</h4><br>';
		$html.= 'Las listas son una función que te permite configurar un catálogo propio con tus series y películas favoritas para compartir con amigos o simplemente crear catálogos temáticos.<br><br>';
		$html.= '<h5>Registrarme</h5>';
	}	
// Comprobamos si el usuario tiene listas creadas, en caso contrario mostramos un mensaje
		if($this->ion_auth->logged_in()){
		          $datos['vernumerolistas'] = $this->Listas_model->todas_listas_perfil();
								$numerolistas = count($datos['vernumerolistas']);
								if($numerolistas === 0){
											$html = '¡Aún no has creado ninguna lista! Ve a tu perfíl para crear listas y poder añadir contenidos a estas.';
								}
							}
							return $html;

}

function seguir_lista(){
			if ($this->ion_auth->logged_in())
		{
	$lista = $this->input->post('lista');
	$this->load->model('Listas_model');
	$this->Listas_model->seguir_lista();
		$data = array(
			"resultado" => 'correcto',

			);
 			echo json_encode($data);
} else {
	$data = array(
			"resultado" => 'error',

			);
 			echo json_encode($data);
}
}

function todas_listas(){
$this->load->model('Listas_model');
$datos['listas'] = $this->Listas_model->todas_listas_orden_seguir();



$this->load->view('todas_listas',$datos);





}



}
