<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	public function __construct(){
			parent::__construct();


			$this->load->library('form_validation');
            $this->load->library('table');


			$this->load->helper('form');
			$this->load->helper('html');

    		$this->load->model('Search_model');


	}

  function get_contents(){
    if (isset($_GET['term'])){
      $string = strtolower($_GET['term']);
      $this->Search_model->get_contents($string);
    }
  }



  function get_contents_form($q = NULL){
  				$this->load->library('background_image');
  				$datos['background'] = $this->background_image->background_image();
  		// Si el GET está vacío no hacemos ninguna búsqueda y mostramos un mensaje
  		$q = $this->input->get('search');
		if($q === NULL || $q===''){
		$datos['lista_busqueda'] = '¿Qué vemos hoy?';
				 

		} else {

  	    $resultado = $this->Search_model->get_contents_form();
  	    // Comprobamos que existen coincidencias, en caso de que no existan mostramos un mensaje
  	    $numeroresultado = count($resultado);
  	    if ($numeroresultado === 0){
  		$datos['lista_busqueda'] = '¡No se han encontrado coincidencias con ' .$q. '!';	    	
  	    } else {
  	    // Existen coincidencias, vamos palante
  	    $lista_busqueda='';
		foreach ($resultado as $row){
		// Comprobamos si es una serie o película para establecer su enlace y portada
		$tipo_contenido = $this->Search_model->tipo_contenido($row['content']);

		if ($tipo_contenido === 'series'){ // Si es una serie
			$tipo = '</br>Serie';
			$url =  '<a href="'.base_url().'series/'.$row['content']. '/'. stripslashes($row['title']) .'">'.$row['title'].'</a>';
			$img = '<img src="'.base_url().'asset/img/series/'.$row['content'].'/c.jpg"></br>';
		} elseif ($tipo_contenido === 'films') { // Si es una película
			$tipo = '</br>Película';
			$url =  '<a href="'.base_url().'films/'.$row['content']. '/'. stripslashes($row['title']) .'">'.$row['title'].'</a>';

			$img = '<img src="'.base_url().'asset/img/peliculas/'.$row['content'].'/c.jpg"></br>';			
		} else { // Si es un capítulo (En caso de capítulo vamos a sacar a qué serie pertenece y redirigimos al usuario al perfil de esa serie)
			$buscar_serie_capitulo = $this->Search_model->buscar_serie_capitulo($row['content']);
			
			
			$tipo = '</br> S'.$buscar_serie_capitulo->season_number.'E'.$buscar_serie_capitulo->episode_number.' '.$row['title'];
			$url =  '<a href="'.base_url().'series/'.$buscar_serie_capitulo->series. '/'. stripslashes($buscar_serie_capitulo->title) .'">'.$buscar_serie_capitulo->title.'</a>';
			$img = '<img src="'.base_url().'asset/img/peliculas/'.$row['content'].'/c.jpg"></br>';				
		
		  // Generamos la lista
		}

		$lista_busqueda.= $img; // Carátula
	    $lista_busqueda.= $url;// Título del contenido
	    $lista_busqueda.= $tipo .'</br>'; // Tipo de contenido (Película o serie)

		}
		$datos['lista_busqueda'] = $lista_busqueda;

		}
		
		$this->load->view('search',$datos);

}
}
  }
