<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>
      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>


</head>
  <!-- MENSAJES -->

<style type='text/css'>
.comentarios {
padding: 5px;
border: 1px solid #000000;
margin-top: 15px;
list-style: none;
}
.seguir {
}
.pendiente {
}
.favorito {
}
.vista {
}
</style>
         <!-- MENSAJES -->

  <!-- BOTONES -->
<style type='text/css'>

.errorboton {
  display:none;
}

</style>
         <!-- BOTONES -->
<style>
    #dialog{
        display: none;
    }
    </style>
         <!-- DIVS DE MENSAJES, ENLACES, FORMULARIOS -->
<style>

    #container_formulario_enlaces{
        display: none;
    }
    #container_mensajes{
        display: show;
        
    }    
    

    </style>
<body>

<script>
$(document).ready(function(){
    $("#link_enlaces").click(function(){
        $(".listaenlace").show();
        $(".tipo_enlace").show();
        $("#container_formulario_enlaces").hide();

    });

    $("#link_formulario").click(function(){
        $("#container_formulario_enlaces").show();
        $(".listaenlace").hide();
        $(".tipo_enlace").hide();

    });

});


</script>
<a href="#" id="link_enlaces">Ver enlaces</a>
</br>
<a href="#" id="link_formulario">Añadir enlace</a>
</br>




 <div class ="tipo_enlace"> <H3>ENLACES VISIONADO ONLINE</H3></div>
<?php echo $enlaces_visionado_online; ?>
<div class = "faltan_enlaces"> ¿Faltan enlaces? ¿Están todos caídos? ¡Avísanos y los pondremos de inmediato!</div>
 <div class ="tipo_enlace"> <H3>ENLACES DESCARGA DIRECTA</H3></div>
<?php echo $enlaces_descarga_directa; ?>
<div class = "faltan_enlaces"> ¿Faltan enlaces? ¿Están todos caídos? ¡Avísanos y los pondremos de inmediato!</div>



<div id = "container_formulario_enlaces">
              <H1>FORMULARIO ENLACES</H1>
<form id="subir_enlace" class="subir_enlace"  action="<?php echo base_url(). 'home/subir_enlace'?>" method='post'>
<b>URL</b></br>
<input type="text" name="firstname"></br>
<b>Resolución</b></br>
<select name="resolucion">
<?php
      foreach ($lista_calidad as $item){
         echo '<option value="'.$item->id.'">'.$item->quality.'</option>';
      }
?>
  </select></br>

  <b>Tipo de enlace</b></br>
<select name="tipo_enlace">
  <?php
      foreach ($lista_tipo_enlaces as $item){
         echo '<option value="'.$item->id.'">'.$item->type.'</option>';
      }
?>
</select></br>
  <b>Idioma audio</b></br>
  <select name="idioma_audio">
  <?php
      foreach ($lista_idiomas as $item){
         echo '<option value="'.$item->id.'">'.$item->language.'</option>';
      }
?>
</select></br>

  <b>Subtitulos</b></br>
<select name="idioma_audio">
<option value="0">Sin subtitulos </option>
  <?php
      foreach ($lista_idiomas as $item){
         echo '<option value="'.$item->id.'">'.$item->language.'</option>';
      }
?>
</select></br></br>
<input  type="hidden" name='idcontenido'  id='idcontenido' value="0" />
<input  type="submit" name="submit"  value="Enviar"/>


</form>
</div>








<!-- Script para sacar la id del comentario padre para responder a alguien -->
 <script> 
 jQuery(document).on('click', '.reply', function(e){  
  event.preventDefault();

  var id = $(this).attr("value");
    $('#parent_comment').attr("value",id);
$(".form-control").focus();



});
      </script>

<script>
$('.reply').click(function() {
    $('#formcomentario').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#formcomentario").dialog({
        autoOpen: false,
        modal: true,
                 width: '500px',

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#formcomentario').dialog('close');
            })
        }
    });
}); 

</script>



<!-- Script para refrescar los me gusta-->
  <script>
  jQuery(document).on('click', '.me_gusta_form', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

  if(json.resultado === 'correcto'){

      var notification = alertify.notify('Votado', 'success', 3, function(){  console.log('dismissed'); });
}
if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', 'Para poder votar comentarios debes entrar con tu usuario. En caso de no estar registrado te recomendamos hacerlo para desbloquear muchísimas funcionalidades. Más info...', function(){ });

      $(".errorvoto").append(json.mensaje).css({"display":"block"});

}

   $('#container_mensajes').load('<?php echo base_url(uri_string());?> #container_mensajes');



},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  



<!-- Script para refrescar los mensajes borrados-->
  <script>
  jQuery(document).on('click', '.borrar_form', function(e){  
  event.preventDefault();
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){

 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes', function() {
      var notification = alertify.notify('Mensaje borrado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

if(json.resultado === 'error'){

      var notification = alertify.notify('El mensaje no se ha podido borrar', 'error', 3, function(){  console.log('dismissed'); });

}
  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>



<!-- Script para abrir el formulario de reporte -->
<script type="text/javascript">
jQuery(document).on('click', '.reporteicon', function(e){  
  event.preventDefault();

      $(".errorreporte,.correctoreporte").html("").css({"display":"none"});

    $('#reporte').dialog('open');
    var id = $(this).attr("value");
    $('#reporte_id_enlace').attr("value",id);

});



jQuery(document).ready(function() {
    jQuery("#reporte").dialog({
        autoOpen: false,
        modal: true,
         width: '500px',
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#reporte').dialog('close');
            })
        }
    });
}); 

</script>

<!-- Script para mostrar mensajes de reporte -->

<script>

$(document).ready(function()
{
  $("#report_enlace").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
$("#observaciones").val("");
setTimeout(function(){
    $("#reporte").dialog('close')
}, 0);
if(json.resultado =="correcto"){

    if(json.mensaje){
      var notification = alertify.notify('Tu reporte se ha enviado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      $(".errorreporte").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify('Hubo un problema al enviar tu reporte.', 'success', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script para recargar div después de enviar mensaje -->
<script>

  jQuery(document).on('submit', '.comment_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

      $(".errormensaje,.correctomensaje").html("").css({"display":"none"});
 $('#container_mensajes').load('<?php echo base_url(uri_string());?> #container_mensajes');


$("#comment").val("");
    $('#parent_comment').val("");


if(json.resultado =="correcto"){

    if(json.mensaje){
      //$(".correctomensaje").append(json.mensaje).css({"display":"block"});
var notification = alertify.notify('¡Mensaje publicado!', 'success', 3, function(){  console.log('dismissed'); });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      //$(".errormensaje").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify('Error al enviar mensaje.', 'error', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>





</body>

</html>