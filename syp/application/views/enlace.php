<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/users.css' ?>">
	<title>LINK</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


	
</head>
<script>
if(top===self){
location.replace("<?php echo base_url()?>");

    alert("Solo puedes abrir esta página directamente desde la web.")
}
</script>
<body>

	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background; ?>">
	<!-- HEADER -->
	<header>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
						<?php 

							if(isset($detalle->episode)){
							echo '<a href='.base_url().'episode/'. $detalle->episode . '/'. $detalle->title.'><li>VOLVER</li></a>';
						} else {
							echo '<a href='.base_url().'films/links/'. $detalle->content.'><li>VOLVER</li></a>';

						}
						?>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->

				</div>
			</div>
		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div id="main-wrapper" class="wrapper">
			<div class="row">
				<div class="col-4">

				Agradecimientos a</br>
				<?php echo $detalle->username .' '. $uploader;?> </br></br>
				<img src="<?php echo $avatar; ?>" height="100" width="100"></br></br>
<form action="<?php echo base_url()?>home/url_redirect" target="_blank" method="POST">
<input type="hidden" value="<?php echo $this->uri->segment(2);?>" name="key"> </input>
<input type="submit" value="VER ENLACE"></input>
</form>
</br>
<?php echo $donaciones_iconos ?>



				</div>
			</div>
		</div>
	</div>





<!-- Script para abrir modal bitcoin-->
<script>
  $('.icondonation.bitcoin').click(function() {
    $('#donation_bitcoin_modal').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#donation_bitcoin_modal").dialog({
        autoOpen: false,
        modal: true,
 				maxWidth:400,
                maxHeight: 300,
                width: 400,
                height: 300,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#donation_bitcoin_modal').dialog('close');
            })

        }

    });
}); 
</script> 


<!-- Script para refrescar capítulos vistos-->
<?php if(isset($detalle->episode)){
	?>
 <script>
 // Si no hay error se vuelve a pulsar en el botón de la temporada  para actualizar sus botones

$(document).ready(function(){
parent.document.getElementById("<?php echo 'temp'.$detalle->seasonnumber; ?>").click();

});
</script>
<?php } ?>


</body>

</html>