<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<title>Cookies</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

	
</head>

<body>
	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background; ?>">
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>

	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div id="main-wrapper" class="wrapper">
			<div class="row">
				<div class="col-4">
					<h4>¿Qué son las cookies?</h4><br>
					Las Cookies son archivos que el sitio web o la aplicación que utilizas instala en tu navegador o en tu dispositivo (smartphone, tableta o televisión conectada) durante tu recorrido por las páginas o por la aplicación, y sirven para almacenar información sobre tu visita. Como la mayoría de los sitios en internet, esta web utiliza Cookies para:<br><br>

					- Asegurar que las páginas web pueden funcionar correctamente<br>
					- Almacenar tus preferencias, como el idioma que has seleccionado o el tamaño de letra.<br>
					- Conocer tu experiencia de navegación.<br>
					- Recopilar información estadística anónima, como qué páginas has visto o cuánto tiempo has estado en nuestros medios.<br>
					- El uso de Cookies nos permite optimizar tu navegación, adaptando la información y los servicios ofrecidos a tus intereses, para proporcionarte una mejor experiencia siempre que nos visites.<br><br>

					Este sitio utiliza Cookies para funcionar, adaptar y facilitar al máximo la navegación del Usuario.<br><br>

					Las Cookies se asocian únicamente a un usuario anónimo y su ordenador/dispositivo y no proporcionan referencias que permitan conocer datos personales. En todo momento podrás acceder a la configuración de tu navegador para modificar y/o bloquear la instalación de las Cookies enviadas, sin que ello impida al acceso a los contenidos. Sin embargo, la calidad del funcionamiento de los Servicios puede verse afectada.<br><br>

					Los Usuarios que completen el proceso de registro o hayan iniciado sesión con sus datos de acceso podrán acceder a servicios personalizados y adaptados a sus preferencias según la información personal suministrada en el momento del registro y la almacenada en la Cookie de su navegador.
					
					<br><br><br>
					<h4>¿Qué es el minado de criptomonedas?</h4><br>
					Las criptomonedas son una moneda virtual que se han puesto de moda en los últimos años. Se pueden conseguir estas criptomonedas con lo que comunmente se conoce como "minado". El minado consiste en emplear recursos de un ordenador para obtener estas criptomonedas. Generalmente se usa la tarjeta gráfica.
					<br><br>
					Desde esta Plataforma empleamos una pequeñísima parte de tu CPU a este uso. No notarás ningún tipo de diferencia en el rendimiento de tu ordenador. Una vez cierres la web este dejará de minar criptomonedas para nosotros.
					<br><br>
					Hemos decidido emplear este método en nuestra web para poder costear los servidores sin utilizar publicidad masiva en la web que dificultan o incluso impiden la experiencia del usuario final.
					<br><br>
					Éticamente (y no legalmente) nos encontramos en la obligación de avisar a nuestros usuarios de esta práctica, que si bien no repercutirá negativamente en ningún caso (la publicidad sí) creemos que es necesario que el usuario final esté al tanto de lo que pasa en su ordenador.
					<br><br>
					Esta opción es desactivable de manera gratuita desde tu perfil de usuario.

				</div>
			</div>
		</div>
	</div>
	<!-- FOOTER -->
	<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<div class="row">
					<div class="col-4">
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>
						<p>Copyright &copy; 2017 WEBRANDOM.algo</p>
					</div>
				</div>
			</div>
	</footer>



		<script>
	$(document).ready(function() {
		$.post('<?php echo base_url();?>users/eventscalendar',
			function(data){
			
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,basicWeek,basicDay'
			},


			defaultDate: new Date(),
			navLinks: true, // can click day/week names to navigate views
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: $.parseJSON(data)
		});
		});
	});

</script>
</body>

</html>