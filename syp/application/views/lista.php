<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/lists.css' ?>">
	<title><?php echo $detalle->name . ' - Listas' ?></title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>

    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

	
</head>

<body>
	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background; ?>">
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div class="wrapper">
			<div class="row">
				<div class="col-1 avatar">
					<div class="col-4">
						<div class="edit">
						</div>
						<div class="follow">
						<?php echo $siguiendo ?>
						</div>
					</div>
					<h4><?php echo $detalle->name . $botoneditar ?></h4>
					<p class="description"><?php echo $detalle->description ?></p></br>
					<p class="follow_number"><?php echo $siguiendo_numero ?></p></br>

					<?php echo $perfilusuariolink . ' ' . $uploader ?>
					</br>
					<div class="col-4">
						Compartir en mis redes sociales
							<div class="share-social">
								<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
								<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>	
							</div>
					</div>
				</div>



				<div class="col-3 container">
					<div class="header-content"><span>Series</span></br></div>
						<?php				
						foreach ($listaseries as $item){
							 $nombre = $item->title;
							 $nombresinespacios = str_replace(" ","-",$nombre);
							 $link = '<a href=' . base_url() . 'series/' . $item->series . '/' . $nombresinespacios . '>' . $item->title . '</a>';
							 echo $link.'</br>';
						}
						?>
				</br>
					<div class="header-content"><span>Películas</span></br></div>
						<?php				
						foreach ($listapeliculas as $item){
							 $nombre = $item->title;
							 $nombresinespacios = str_replace(" ","-",$nombre);
							 $link = '<a href=' . base_url() . 'films/' . $item->content . '/' . $nombresinespacios . '>' . $item->title . '</a>';
							 echo $link.'</br>';
						}
						?>					
					</div>



			</div>
		</div>
	<!-- MODAL DIVS -->
	<div class = "formlistawrapper" id="formlistawrapper" title="Editar lista">
		<?php echo $formularioeditar ?>
	</div>


	<!-- FOOTER -->
	<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<div class="row">
					<div class="col-4">
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>
						<p>Copyright &copy; 2017 WEBRANDOM.algo</p>
					</div>
				</div>
			</div>
	</footer>

	<!-- Script para abrir modal del botón editar listas de los usuarios -->
 <script>
  $('#botoneditar').click(function() {
    $('#formlistawrapper').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#formlistawrapper").dialog({
        autoOpen: false,
        modal: true,
                    maxHeight: 500,
                    height: 500,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#formlistawrapper').dialog('close');
            })
        }
    });
}); 
</script> 

<!-- Script para guardar la lista -->

<script>

$(document).ready(function()
{
  $("#editarlista").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
 $('.infolista').load('<?php echo base_url(uri_string());?> .infolista')


if(json.resultado =="correcto"){
      var notification = alertify.notify('¡Lista editada correctamente!', 'success', 3, function(){  console.log('dismissed'); });
setTimeout(function(){
    $("#formlistawrapper").dialog('close')
}, 0);
    location.reload();



}

if(json.resultado =="errortexto"){
      alertify.alert('El título y la descripción deben tener como mínimo 3 carácteres.', function(){ });



}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>
<!-- Script para refrescar seguir-->
  <script>
  jQuery(document).on('click', '.seguir_form', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
var json = JSON.parse(data);

if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', '<b>¡Enhorabuena! Has descubierto una nueva función.</b> </br> Para poder seguir listas debes tener una cuenta de usuario. ¡Es completamente gratuito!', function(){ });


}
 $('.follow').load('<?php echo base_url(uri_string());?> .follow', function() {
 });
  $('.follow_number').load('<?php echo base_url(uri_string());?> .follow_number', function() {
  follow_number
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  

</body>

</html>