<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 


      <!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>


  <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>
<hr>
<h4> Cambiar email </h4>
<form action="<?php echo base_url(). 'users/editar_email_codigo' ?>"  method="post" id="editar_email">
    <div class="erroremail"> </div>
    <div class="correctoemail"> </div>
    <label><b>Email</b></label></br>
    <input type="text" placeholder="Nuevo email" name='emailnuevo' id='emailnuevo' required></br>
      <button type="submit">Guardar</button>
</form>
<hr>

<h4> Privacidad de la cuenta </h4>

<form action="<?php echo base_url(). 'users/editar_privacidad' ?>"  method="post" id="editar_privacidad">
  <input type="radio" name="privacidad" value="0" <?php  if($this->ion_auth->user()->row()->private === '0'){echo "checked";} ?>> Cuenta pública<br>

  <input type="radio" name="privacidad" value="1" <?php  if($this->ion_auth->user()->row()->private === '1'){echo "checked";} ?>> Cuenta privada<br>
  Si estableces tu cuenta como pública cualquier persona podrá ver las series y películas que tienes marcadas así como tu actividad reciente. En caso de marcar la cuenta privada estos datos únicamente los podrás ver tú.<br>
        <button type="submit">Guardar</button>

  </form>
  <hr>

<h4> Idioma </h4>

<form action="<?php echo base_url(). 'users/editar_idioma' ?>"  method="post" id="editar_idioma">
<select name="idioma">
<?php 
foreach ($opciones_idioma as $item){
  $opcion = '';
  if ($item->id === $idioma_usuario){$opcion = 'selected ';}
  echo   '<option '.$opcion.'value="'.$item->id.'">'.$item->language.'</option>';
}
?>
</select></br>
Toda la información de las películas y series las verás en este idioma</br>
        <button type="submit">Guardar</button>

  </form>
<hr>  
  <h4>[UPLOADERS] Cuentas para recibir donativos </h4>
  <?php echo $iconos_cuentas_donaciones ?>

<hr>
<h4> Cambiar contraseña </h4>
<form action="<?php echo base_url(). 'users/editar_password' ?>"  method="post" id="editar_contraseña">
    <div class="errorpass"> </div>
    <div class="correctopass"> </div>

    <label><b>Contraseña actual</b></label></br>
    <input type="password" placeholder="Contraseña actual" name='old' id='old' required></br>

    <label><b>Nueva contraseña</b></label></br>
    <input type="password" placeholder="Nueva contraseña" name='new' id='new' required></br>
    <label><b>Confirmar nueva contraseña</b></label></br>
    <input type="password" placeholder="Confirmar nueva contraseña" name='confirm_password' id='confirm_password' required></br>
      <button type="submit">Guardar</button>
</form>

<div id="bitcoin_modal_edit" class="bitcoin_modal_edit" style="display:none;" title="Editar Bitcoin">
  
  <form action="<?php echo base_url()?>profile/edit/donation" method="POST" class="cuentas_donaciones">
  Código wallet<br>
  <input type="text" name="codigo" value="CÓDIGO PARA RECIBIR DONACIONES">
  <input type="hidden" name="tipo" value="2"> 
    <input type="submit" value="Submit">
 
</form> 
</div>

<div id="paypal_modal_edit" class="paypal_modal_edit" style="display:none;" title="Editar PayPal"  >
  
  <form action="<?php echo base_url()?>profile/edit/donation" method="POST" class="cuentas_donaciones">
  Email PayPal<br>
  <input type="text" name="codigo" value="EMAIL">
  <input type="hidden" name="tipo" value="1">  
    <input type="submit" value="Submit">

</form> 
</div>
<!-- Script para abrir modal bitcoin-->
<script>

  $('.icondonation.bitcoin').click(function() {
    $('#bitcoin_modal_edit').dialog().dialog('open');


});
    jQuery("#bitcoin_modal_edit").dialog({
        autoOpen: false,
        modal: true,
        maxWidth:400,
                maxHeight: 300,
                width: 400,
                height: 300,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#bitcoin_modal_edit').dialog('close');
            })

        }

    });
 
</script> 
<!-- Script para abrir modal paypal-->
<script>

  $('.icondonation.paypal').click(function() {
    $('#paypal_modal_edit').dialog().dialog('open');


});
    jQuery("#paypal_modal_edit").dialog({
        autoOpen: false,
        modal: true,
        maxWidth:400,
                maxHeight: 300,
                width: 400,
                height: 300,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#paypal_modal_edit').dialog('close');
            })

        }

    });
 
</script> 
<script>
$(document).ready(function()
{
  $("#editar_contraseña").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="error"){

    if(json.mensaje){
      $(".errorpass").append(json.mensaje).css({"display":"block"});
            alertify.alert('Error con el cambio de contraseña', json.mensaje, function(){ });

    }

}
if(json.resultado =="correcto"){

    if(json.mensaje){
      $(".correctopass").append(json.mensaje).css({"display":"block"});
            alertify.alert('Contraseña modificada correctamente', json.mensaje, function(){ });


    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script cambio de cuentas de donaciones-->
<script>
$(document).ready(function()
{
  $(".cuentas_donaciones").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="error"){
      alertify.alert('Error', json.mensaje, function(){ });

}
if(json.resultado =="correcto"){

            alertify.alert('Cuenta de donación modificada correctamente', json.mensaje, function(){ });
$(".ui-dialog-content").dialog("close");



}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>
<script>
$(document).ready(function()
{
  $("#editar_email").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="error"){

    if(json.mensaje){
      alertify.alert('¡Error con el cambio de email!', json.mensaje, function(){ });

    }

}
if(json.resultado =="correcto"){

    if(json.mensaje){
      alertify.alert('Confirmación de email', json.mensaje, function(){ });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<script>
$(document).ready(function()
{
  $("#editar_privacidad").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="correcto"){

    if(json.mensaje){
      var notification = alertify.notify('El estado de privacidad de tu cuenta se ha modificado correctamente.', 'success', 3, function(){ });

    }

}

else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<script>
$(document).ready(function()
{
  $("#editar_idioma").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="correcto"){

    if(json.mensaje){
      var notification = alertify.notify('El idioma de tu cuenta se ha modificado correctamente.', 'success', 3, function(){   });

    }

}

else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>


</body>
