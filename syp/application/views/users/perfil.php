<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>


<title><?php echo $titulo; ?></title>
<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>
</head>


 <body>
 <a href="<?php echo base_url(). 'profile/edit' ?>">Editar perfil</a> </br>
 <a href="<?php echo base_url(). 'logout' ?>">Logout</a> </br>

 <h4>Usuario:  <?php echo $user->username; ?> </h4>
     <?php echo $this->session->flashdata('error_avatar') .'</br>'; ?>
     <?php echo $this->session->flashdata('correcto_avatar') .'</br>'; ?>
<img src="<?php echo $avatar?>">

<?php echo $editar_avatar ?>
 <h4>Grupos:</h4>
<?php foreach ($grupos as $item){

    echo $item->name . ',';
    } ?> </br>
<!-- FORMULARIO PARA CREAR NUEVA LISTA -->
 <div class = "formlistawrapper" id="formlistawrapper" title="Crear nueva lista">
 <center>
 <form id="nuevalista" class="nuevalista" action="<?= base_url() ?>users/nueva_lista" method='post'>
  
<b>Nombre de la lista</b></br>
 <input type="text" name="nombre" class="nombre"><br>
<b>Descripción</b></br>
 <textarea name="descripcion" maxlength="500" class="descripcion" rows="5" cols="25"></textarea>
 <input type="radio" name="privacidad" value="0" checked> Lista pública
 <input type="radio" name="privacidad" value="1"> Lista privada
 <input type="submit" value='Crear lista' class='crear'  id='crear' name='crear' />
 </form>
 </center>
</div>

<!-- FORMULARIO PARA EDITAR LISTA -->
 <div class = "formlisteditawrapper" id="formlisteditawrapper" title="Editar lista">
 <center>
 <form id="editarlista" class="editarlista" action="<?= base_url() ?>users/editar_lista" method='post'>
   <input type="hidden" name="idlista"  id="idlista" class="idlista"><br>

<b>Nombre de la lista</b></br>
 <input type="text" name="nombre" class="nombre" id="nombre"><br>
<b>Descripción</b></br>
 <textarea name="descripcion" maxlength="500" class="descripcion" id="descripcion" rows="5" cols="25"></textarea></br>
 <input type="radio" name="privacidad" id="publica" value="0"> Lista pública
 <input type="radio" name="privacidad" id="privada" value="1"> Lista privada
 </br>
 <input type="submit" value='Editar lista' class='crear'  id='crear' name='crear' />
 </form>
 </center>
</div>


 <h4>Listas:</h4>
 <button type ="button" value='crearlista' class='crearlista'  id='crearlista' name='crearlista' style="width: 200px;" />Crear nueva lista</button>
 </br></br></br>


<div class = "listaswrapper">
<table> 
<?php foreach ($listas as $item){
            echo "<tr>";
 echo' <tr>
    <th style="display: none;">Nombre</th>
    <th>Nombre</th>
    <th>Descripción</th>
    <th>Seguidores</th>
    <th>Estado</th>

  </tr>';
            $url = '<a href="'. base_url(). 'list/'. $item->list. '/' . htmlentities(stripslashes($item->name)) .'"> '.$item->name.'</a>';
            
            echo "<td name='id' id='id' style='display: none;'>".$item->list."</td>";
            echo "<td name='listaname' id='listaname' style='display: none;'>".$item->name."</td>";

            echo "<td name='url' id='url'>".$url."</td>";
            echo "<td name='listadescription' id='listadescription'>".$item->description."</td>";
            echo "<td name='listfollow' id='listfollow'>".$item->follow."</td>";
            if($item->state != '0'){
              $privacidad = 'Privada';
            } else {
              $privacidad = 'Pública';
            }
                        echo "<td name='privacidad' id='privacidad'>".$privacidad."</td>";

            echo " <td name='button'><button type='button' class='editarlista_button' onClick='selectedlist(this)'>Editar lista</button></td>";

            echo "</tr>";
    } ?>
</table>
    </br>
</div>
<h1> Series </h1>

<h4>Favoritas</h4>
<?php foreach ($favoritosseries as $item){
       $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?> </br>




<h4>Siguiendo</h4>
<?php foreach ($siguiendo as $item){
 $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h4>Vistas</h4>
<?php foreach ($vistasseries as $item){
 $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h4>Pendientes</h4>
<?php foreach ($pendientesseries as $item){
 $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h1> Películas </h1>

<h4>Favoritas</h4>
<?php foreach ($favoritospeliculas as $item){
       $url = base_url() . 'films/' . $item->id . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>


<h4>Vistas</h4>
<?php foreach ($vistaspeliculas as $item){
       $url = base_url() . 'films/' . $item->id . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h4>Pendientes</h4>
<?php foreach ($pendientespeliculas as $item){
       $url = base_url() . 'films/' . $item->id . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

</body>
<!-- Script para abrir modal del botón NUEVAS listas de los usuarios -->
 <script>
  $('#crearlista').click(function() {
    $('#formlistawrapper').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#formlistawrapper").dialog({
        autoOpen: false,
        modal: true,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#formlistawrapper').dialog('close');
            })
        }
    });
}); 
</script> 

<!-- Script para abrir modal del botón EDITAR listas de los usuarios -->
 <script>
 
  $('.editarlista_button').click(function() {

    $('#formlisteditawrapper').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#formlisteditawrapper").dialog({
        autoOpen: false,
        modal: true,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#formlisteditawrapper').dialog('close');
            })
        }
    });
}); 
</script> 

<!-- Script para guardar la NUEVA lista -->

<script>

$(document).ready(function()
{
  $("#nuevalista").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
 $('.listaswrapper').load('<?php echo base_url(uri_string());?> .listaswrapper')


if(json.resultado =="correcto"){
      var notification = alertify.notify('¡Lista creada!', 'success', 3, function(){  console.log('dismissed'); });
setTimeout(function(){
    $("#formlistawrapper").dialog('close')

}, 0);


}
if(json.resultado =="errormax"){
      alertify.alert('Has alcanzado el límite máximo de listas', 'Solo puedes tener 10 listas activas a la vez. Para crear una nueva lista tendrás que borrar otra.', function(){ });
setTimeout(function(){
    $("#formlistawrapper").dialog('close')
}, 0);

}
if(json.resultado =="errortexto"){
      alertify.alert('El título y la descripción deben tener como mínimo 3 carácteres.', function(){ });



}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>


<!-- Script para editar la lista -->

<script>

$(document).ready(function()
{
  $("#editarlista").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
 $('.listaswrapper').load('<?php echo base_url(uri_string());?> .listaswrapper')


if(json.resultado =="correcto"){
      var notification = alertify.notify('¡Lista editada!', 'success', 3, function(){});
setTimeout(function(){

    $("#formlisteditawrapper").dialog('close')


}, 0);


}

if(json.resultado =="errortexto"){
      alertify.alert('El título y la descripción deben tener como mínimo 3 carácteres.', function(){ });



}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>


<script>
    //Función para pasar los parametros de editar lista
    selectedlist = function(boton) {
      console.log("selectedlist");
        var celdas = $(boton).closest('tr').find('td'); 
        
        var id = celdas.eq(0).text(); 
        var listaname = celdas.eq(1).text(); 
        var listadescripcion = celdas.eq(3).text(); 
        var privacidad = celdas.eq(5).text(); 

        
        $('#idlista').val(id); 
        $('#nombre').val(listaname); 
        $('#descripcion').val(listadescripcion); 
if (privacidad == 'Pública'){
        document.getElementById("publica").checked = true;

} else {
        document.getElementById("privada").checked = true;
}
        console.log('HOLA');

    }
    </script>
</html>