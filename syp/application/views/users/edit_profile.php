<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/edit_profile.css' ?>">
	<title>Editar perfil</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Relacionado con las alertas -->
	<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
	<!-- CSS -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
	<!-- Default theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
	<!-- Semantic UI theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>

    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>


</head>

<body>
	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background; ?>">
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div id="main-wrapper" class="wrapper">
			<div class="row">
				<!-- EDIT EMAIL -->
				<div class="col-4">
					<div class="header-content"><p class="header-text">Editar email</p></div>
					<form action="<?php echo base_url(). 'users/editar_email_codigo' ?>"  method="post" id="editar_email">
    					<div class="erroremail"> </div>
    					<div class="correctoemail"> </div>
    					<input type="text" placeholder="NUEVO EMAIL DE USUARIO" name='emailnuevo' id='emailnuevo' required>
      					<input type="submit" value="CAMBIAR EMAIL"></input>
					</form>
				</div>
				<!-- EDIT ACCOUNT PRIVACITY -->
				<div class="col-4">
					<div class="header-content"><p class="header-text">Privacidad</p></div>
					  	Si estableces tu cuenta como pública cualquier persona podrá ver las series y películas que tienes marcadas así como tu actividad reciente. En caso de marcar la cuenta privada estos datos únicamente los podrás ver tú.</br></br>

						<form action="<?php echo base_url(). 'users/editar_privacidad' ?>"  method="post" id="editar_privacidad">
  							<input type="radio" name="privacidad" value="0" <?php  if($this->ion_auth->user()->row()->private === '0'){echo "checked";} ?>> Cuenta pública

  							<input type="radio" name="privacidad" value="1" <?php  if($this->ion_auth->user()->row()->private === '1'){echo "checked";} ?>> Cuenta privada<br><br>
        					<input type="submit" value="CAMBIAR PRIVACIDAD"></input>
  						</form>
				</div>
				<!-- EDIT ACCOUNT LANGUAGE -->
				<div class="col-4">
					<div class="header-content"><p class="header-text">Idioma</p></div>
						Toda la información de las películas y series las verás en este idioma<br>

						<form action="<?php echo base_url(). 'users/editar_idioma' ?>"  method="post" id="editar_idioma">
							</br><select name="idioma">
								<?php 
									foreach ($opciones_idioma as $item){
  										$opcion = '';
  										if ($item->id === $idioma_usuario){$opcion = 'selected ';}
											echo '<option '.$opcion.'value="'.$item->id.'">'.$item->language.'</option>';
									}
								?>
							</select></br></br>
        					<input type="submit" value="CAMBIAR IDIOMA"></input>
  						</form>
				</div>	
				<!-- EDIT ACCOUNT DONATIONS -->
				<div class="col-4">
					<div class="header-content"><p class="header-text">Cuentas para donaciones</p></div>
						¿Eres uploader? De ser así permite que el resto de usuarios puedan agradecer tus aportaciones a través de donaciones en las siguientes plataformas. Recuerda que Bitcoin es completamente anónima pero en PayPal tus datos son de acceso público</br></br>
						  <?php echo $iconos_cuentas_donaciones ?>
				</div>	
				<!-- EDIT ACCOUNT BENEFITS -->
				<div class="col-4">
					<div class="header-content"><p class="header-text">Mi aportación</p></div>
						 A través de estas opciones podrás establecer en qué grado quieres ayudar a nuestra Plataforma para pagar sus servidores.<br><br>

						<form action="<?php echo base_url(). 'users/editar_aportacion' ?>"  method="post" id="editar_aportacion" class="editar_aportacion">
							<div class= "aportacion_radio">
	  							<input type="radio" name="aportacion" value="1"  <?php  if($this->ion_auth->user()->row()->solidarity === '1'){echo "checked";} ?>> Publicidad
	  							<input type="radio" name="aportacion" value="2"  <?php  if($this->ion_auth->user()->row()->solidarity === '2'){echo "checked";} ?>> Minado
	  							<input type="radio" name="aportacion" value="3"  <?php  if($this->ion_auth->user()->row()->solidarity === '3'){echo "checked";} ?>> Ambas
	  							<input type="radio" name="aportacion" value="4"  <?php  if($this->ion_auth->user()->row()->solidarity === '4'){echo "checked";} ?>> Ninguna <br><br>
	  							<input type="submit" value="CAMBIAR TIPO DE APORTACIÓN"></input>
	  						</div>
						
						</form>
				</div>					
				<!-- EDIT PASSWORD -->
				<div class="col-4">
					<div class="header-content"><p class="header-text">Editar contraseña</p></div>
					<form action="<?php echo base_url(). 'users/editar_password' ?>"  method="post" id="editar_contraseña">
    					<div class="errorpass"> </div>
    					<div class="correctopass"> </div>
					    <input type="password" placeholder="CONTRASEÑA ACTUAL" name='old' id='old' required></br>
					    <input type="password" placeholder="NUEVA CONTRASEÑA" name='new' id='new' required></br>
    					<input type="password" placeholder="CONFIRMAR NUEVA CONTRASEÑA" name='confirm_password' id='confirm_password' required></br>
      					<input type="submit" value="CAMBIAR CONTRASEÑA"></input>
					</form>
				</div>	




			</div>

		</div>
	</div>
	<!-- MODAL DIVS -->
<div id="bitcoin_modal_edit" class="bitcoin_modal_edit" style="display:none;" title="Editar Bitcoin">
	<form action="<?php echo base_url()?>profile/edit/donation" method="POST" class="cuentas_donaciones">
		<div class="actual_url">
		<br>Código wallet actual<br>
		<input type="text" name="codigoactual" placeholder="<?php echo $bitcoin; ?>" disabled></br></br>
	</div>
		Nuevo código wallet<br>		
  		<input type="text" name="codigo" placeholder="NUEVO CÓDIGO"></br></br>
  		<input type="hidden" name="tipo" value="2"> 
    	<input type="submit" value="EDITAR BITCOIN">
	</form> 
</div>

<div id="paypal_modal_edit" class="paypal_modal_edit" style="display:none;" title="Editar PayPal"  >
  	<form action="<?php echo base_url()?>profile/edit/donation" method="POST" class="cuentas_donaciones">
  		<div class="actual_url">
  		<br>Dirección PayPal actual<br>
  		<input type="text" name="codigoactual" placeholder="<?php echo $paypal; ?>" disabled></br></br>
  		</div>
  		Nueva dirección PayPal<br>
  		<input type="text" name="codigo" placeholder="NUEVA DIRECCIÓN DE PAYPAL"></br></br>
 		<input type="hidden" name="tipo" value="1">  
    	<input type="submit" value="EDITAR PAYPAL">
	</form> 
</div>
	<!-- FOOTER -->
	<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<div class="row">
					<div class="col-4">
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>
						<p>Copyright &copy; 2017 WEBRANDOM.algo</p>
					</div>
				</div>
			</div>
	</footer>

	<!-- Script para abrir modal bitcoin-->
<script>

  $('.icondonation.bitcoin').click(function() {
    $('#bitcoin_modal_edit').dialog().dialog('open');


});
    jQuery("#bitcoin_modal_edit").dialog({
        autoOpen: false,
        modal: true,
        maxWidth:400,
                maxHeight: 300,
                width: 400,
                height: 300,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#bitcoin_modal_edit').dialog('close');
            })

        }

    });
 
</script> 
<!-- Script para abrir modal paypal-->
<script>

  $('.icondonation.paypal').click(function() {
    $('#paypal_modal_edit').dialog().dialog('open');


});
    jQuery("#paypal_modal_edit").dialog({
        autoOpen: false,
        modal: true,
        maxWidth:400,
                maxHeight: 300,
                width: 400,
                height: 300,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#paypal_modal_edit').dialog('close');
            })

        }

    });
 
</script> 
<script>
$(document).ready(function()
{
  $("#editar_contraseña").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="error"){

    if(json.mensaje){
      $(".errorpass").append(json.mensaje).css({"display":"block"});
            alertify.alert('Error con el cambio de contraseña', json.mensaje, function(){ });

    }

}
if(json.resultado =="correcto"){

    if(json.mensaje){
      $(".correctopass").append(json.mensaje).css({"display":"block"});
            alertify.alert('Contraseña modificada correctamente', json.mensaje, function(){ });


    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script cambio de cuentas de donaciones-->
<script>
$(document).ready(function()
{
  $(".cuentas_donaciones").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="error"){
      alertify.alert('Error', json.mensaje, function(){ });

}
if(json.resultado =="correcto"){

            alertify.alert('Cuenta de donación modificada correctamente', json.mensaje, function(){window.location.reload(); });
$(".ui-dialog-content").dialog("close");



}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script editar aportaciones-->
<script>
$(document).ready(function()
{
  $(".editar_aportacion").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
 console.log(data);
if(json.resultado =="error"){
      alertify.alert('¡Error!', 'Esta función está reservada únicamente a usuarios con rango de Uploader o Premium.<br> Para más información visita tu perfíl.', function(){ });
        $('.aportacion_radio').load('<?php echo base_url(uri_string());?> .aportacion_radio', function() {
 });

}
if(json.resultado =="correcto"){

      var notification = alertify.notify('Aportación editada correctamente', 'success', 3, function(){ });





}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>


<script>
$(document).ready(function()
{
  $("#editar_email").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="error"){

    if(json.mensaje){
      alertify.alert('¡Error con el cambio de email!', json.mensaje, function(){ });

    }

}
if(json.resultado =="correcto"){

    if(json.mensaje){
      alertify.alert('Confirmación de email', json.mensaje, function(){ });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<script>
$(document).ready(function()
{
  $("#editar_privacidad").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="correcto"){

    if(json.mensaje){
      var notification = alertify.notify('El estado de privacidad de tu cuenta se ha modificado correctamente.', 'success', 3, function(){ });

    }

}

else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<script>
$(document).ready(function()
{
  $("#editar_idioma").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
if(json.resultado =="correcto"){

    if(json.mensaje){
      var notification = alertify.notify('El idioma de tu cuenta se ha modificado correctamente.', 'success', 3, function(){   });

    }

}

else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

</body>

</html>