<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/profile.css' ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide.css' ?>">

	<title>LOGIN</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/jquery.dialogOptions.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

    

	<!-- ALERTAS -->
	<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>

	<!-- SLIDE -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide/slick.css' ?>"/>
	<script type="text/javascript" src="<?php echo base_url() . 'asset/js_files/slide/slick.min.js' ?>"></script>
 	<script src="<?php echo base_url() . 'asset/js_files/slide/slick.js' ?>" type="text/javascript" charset="utf-8"></script>

<body>
		<!-- BACKGROUND SHADOW -->
	<div id="background-shadow">
	</div>
	<!-- BACKGROUND IMAGE -->


	<img id="background-image" src="<?php echo $background;?>">
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- BACKGROUND AUTO-RESIZABLE IMAGE / TRAILER -->
	<div id="background-container">
		<!-- TRAILER -->
		<!-- IMAGE -->
		<div id="background" >
		</div>
	</div>

	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div class="wrapper">
			<!-- 1ST CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1">
					<!-- POSTER -->
						<img class="avatar" src="<?php echo $avatar?>">
						
				</div>
				<!-- 2ND CONTENT COLUMN -->
				<div class="col-3">
					<h1 class="title"><?php echo $detalle->username?></h1>
					<ul class="user-data">					
						<li><h6>FECHA DE REGISTRO:</h6></li>
						<li><?php echo $fecha_registro?></li>
						<li><h6>GRUPOS:</h6></li>
						<li><?php echo $grupos?></li>
						<li><h6>NIVEL DE SOLIDARIDAD:</h6></li>
						<li><?php echo $detalle->description?></li>
					
					</ul>
					<ul class="user-data">
						<li><h6>CRÍTICAS PUBLICADAS:</h6></li>
						<li><?php echo $total_criticas?></li>						
						<li><h6>ENLACES APORTADOS:</h6></li>
						<li><?php echo $total_links?></li>
					</ul>
				</div>
			</div>
			<!-- 2ND CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1">
					<div id="botonescontainer" class="botonescontainer">

					</div>
						<!-- BUTTON MY LISTS-->					
					
				</div>
				<!-- 2ND CONTENT COLUMN (PLOT) -->
				<div class="col-3">
					<h5>UN POCO SOBRE RVEGA</h5>
					<p class="user-description">Parece un poco tímido, no ha dado ninguna descripción suya.
					</p>
				</div>
				<!-- 3RD CONTENT COLUMN (BUTTONS SEASONS) -->

				<div class="col-4">

				</div>




			</div>


				<div class="row">
					
				</div>

			</div>
		</div>


		<!-- FOOTER -->
		<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<p>ESTO ES UNA PRUEBA</p>
			</div>
		</footer>
	</div>

</body>

</html>