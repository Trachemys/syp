<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/users.css' ?>">
	<title>LOGIN</title>
	      <!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>
	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<script>
$(document).ready(function()
{
  $("#pass_olvidada").on("submit", function(e)
  {
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(datapass){
console.log(datapass);
 var json = JSON.parse(datapass);
  $(".erroremail").html("").css({"display":"none"});

 if(json.resultado =="emailenviado"){

    alertify.alert('¡Te hemos enviado un email', 'Te hemos enviado un email con las instrucciones para poder cambiar la contraseña asociada a este email.', function(){window.location.href = '/'; });

    }
if(json.resultado =="erroremail"){
      $(".erroremail").append(json.mensaje).css({"display":"block"});

}
if(json.resultado =="emailnoexiste"){
      $(".erroremail").append(json.mensaje).css({"display":"block"});

}


},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>
</head>

<body>
	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background ?>">
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form>
						<input type="text" spellcheck="false" autocomplete="off" name="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div id="main-wrapper" class="wrapper">
			<div class="row">
				<div class="col-4">
				<form action="<?php echo base_url(). 'users/pass_olvidada' ?>"  method="post" id="pass_olvidada">
    <p class="error erroremail"> </div>
    <label><b>Indicanos el email asociado a tu cuenta para restablecer tu contraseña</b></label></br></br>
    <input type="text" placeholder="EMAIL" name='email' id='email' required></br>
   

      <input type="submit" class="signupbtn" value="RECUPERAR CONTRASEÑA"></input>
</form>

				</div>
			</div>
		</div>
	</div>
	<!-- FOOTER -->
	<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<div class="row">
					<div class="col-4">
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>
						<p>Copyright &copy; 2017 WEBRANDOM.algo</p>
					</div>
				</div>
			</div>
	</footer>
</body>

</html>