<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide_profile_content.css' ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/profile.css' ?>">


	<title>LOGIN</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/jquery.dialogOptions.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

    

	<!-- ALERTAS -->
	<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>



<body>
		<!-- BACKGROUND SHADOW -->
	<div id="background-shadow">
	</div>
	<!-- BACKGROUND IMAGE -->


	<img id="background-image" src="<?php echo $background;?>">
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- BACKGROUND AUTO-RESIZABLE IMAGE / TRAILER -->
	<div id="background-container">
		<!-- TRAILER -->
		<!-- IMAGE -->
		<div id="background" >
		</div>
	</div>

	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div class="wrapper">
			<!-- 1ST CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1">
					<!-- POSTER -->
						<img class="avatar" src="<?php echo $avatar?>">
						
				</div>
				<!-- 2ND CONTENT COLUMN -->
				<div class="col-3">
					<h1 class="title"><?php echo $user->username?></h1>
					<ul class="user-data">					
						<li><h6>FECHA DE REGISTRO:</h6></li>
						<li><?php echo $fecha_registro?></li>
						<li><h6>GRUPOS:</h6></li>
						<li><?php echo $grupos?></li>
						<li><h6>NIVEL DE SOLIDARIDAD:</h6></li>
						<li><?php echo $solidarity->description?></li>
					
					</ul>
					<ul class="user-data">
						<li><h6>CRÍTICAS PUBLICADAS:</h6></li>
						<li><?php echo $total_criticas?></li>						
						<li><h6>ENLACES APORTADOS:</h6></li>
						<li><?php echo $total_links?></li>
					</ul>
				</div>
			</div>
			<!-- 2ND CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1">
					<div id="botonescontainer" class="botonescontainer">
						<button type="submit" class="button button_list">MIS LISTAS</button>

						<form action="<?php echo base_url()?>profile/edit" type="POST">
							<button type="submit" class="button ">EDITAR PERFIL</button>
						</form>

						<form action="<?php echo base_url()?>logout" type="POST">
							<button type="submit" class="button button_logout">LOGOUT</button>
						</form>

					</div>
						<!-- BUTTON MY LISTS-->					
					
				</div>
				<!-- 2ND CONTENT COLUMN (PLOT) -->
				<div class="col-3 description_wrapper">
					<h5>TU DESCRIPCIÓN</h5>
					<form method="POST" class="edit_description_user" action="<?php echo base_url()?>users/editar_descripcion_user">
						<textarea class="description_textarea" name="description_user" placeholder="Cuéntanos algo sobre ti... Esta descripción será pública y cualquiera que visite el perfil podrá verla."><?php echo $user->user_description?></textarea>
						<input type="submit" value="ENVIAR" class="btn_description_submit">
					</form>
				</div>
				<!-- 3RD CONTENT COLUMN (BUTTONS SEASONS) -->

				<div class="row">
					<h4 class="content_title">PELÍCULAS</h4>
						<div class="button_content_container">
							<form action="<?php echo base_url()?>users/profile_user_pending_films" method="POST" class="content_films_form">
								<button  type="submit" class="button button_content" name="type_content">PENDIENTES</button>
							</form>

							<form action="<?php echo base_url()?>users/profile_user_favourite_films" method="POST" class="content_films_form">							
							<button  type="submit" class="button button_content" name="type_content">FAVORITAS</button>
							</form>

							<form action="<?php echo base_url()?>users/profile_user_viewed_films" method="POST" class="content_films_form">							
							<button  type="submit" class="button button_content" name="type_content">VISTAS</button>
							</form>

						</div>
					<div class="row">
						<div class="col-4">
							<div class="contents_loading" id="contents_loading_films"></div>
							<div class="content_films" id="content_films"></div>
						</div>						
				</div>
				<div class="row">
					<h4 class="content_title">SERIES</h4>
						<div class="button_content_container">
							<form action="<?php echo base_url()?>users/profile_user_following_series" method="POST" class="content_series_form">							
							<button  type="submit" class="button button_content" name="type_content">SIGUIENDO</button>
							</form>

							<form action="<?php echo base_url()?>users/profile_user_pending_series" method="POST" class="content_series_form">							
							<button  type="submit" class="button button_content" name="type_content">PENDIENTES</button>
							</form>

							<form action="<?php echo base_url()?>users/profile_user_favourite_series" method="POST" class="content_series_form">							
							<button  type="submit" class="button button_content favourite" name="type_content">FAVORITAS</button>
							</form>

							<form action="<?php echo base_url()?>users/profile_user_viewed_series" method="POST" class="content_series_form">							
							<button  type="submit" class="button button_content" name="type_content">VISTAS</button>
							</form>

						</div>					
					<div class="row">
						<div class="col-4">
							<div class="contents_loading" id="contents_loading_series"></div>
							<div class="content_series" id="content_series"></div>

						</div>
						</div>	

					</div>



			</div>




			</div>
</div>
		    <!-- MODAL BOX PARA ABRIR LAS LISTAS -->
		    <div id="container-list" style="display: none;"  title="Ver mis listas">
		         		<div id="listado-listas" class="listado-listas">
		         			<div class="wrapper-list">
				           		<?php echo $listas ?>
			           		</div>
			           		<div class="container_btn_create_list">
								<button class="button btn_create_list" onclick="crear_lista_open_form()">CREAR NUEVA LISTA</button>
							</div>
							<div class="create_list_container">
								<br>
								<form id="nuevalista" class="nuevalista" action="<?= base_url() ?>users/nueva_lista" method='post'>
								  
								<b>Nombre de la lista</b></br>
								 <input type="text" name="nombre" class="nombre"><br><br>
								<b>Descripción</b></br>
								 <textarea name="descripcion" maxlength="500" class="description_list_textarea"></textarea><br><br>
								 <input type="radio" name="privacidad" value="0" checked> Lista pública
								 <input type="radio" name="privacidad" value="1"> Lista privada
								 <div><input type="submit" value='Guardar lista' class='crear'  id='crear' name='crear' /><br><br></div>
								 </form>								
							</div>
		           		</div>
		    </div>
		    <!-- FIN MODAL BOX PARA ABRIR LAS LISTAS -->


		<!-- FOOTER -->
		<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<p>ESTO ES UNA PRUEBA</p>
			</div>
		</footer>
	</div>

<!-- Script para recargar tipos de contenidos de películas (pendiente,visto...)-->
<script>
  jQuery(document).on('submit', '.content_films_form', function(event){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
	console.log(data);
;


	document.getElementById("content_films").innerHTML = (data);
	document.getElementById("content_films").style.display = "none";
	document.getElementById("contents_loading_films").style.display = "block";
	setTimeout(load_contents_films, 1500);



},
error:function(xhr,exception)
{

}
})
event.preventDefault();
  });

</script>
<script>
function load_contents_films() {
	$('#contents_films').fadeIn('slow');

	document.getElementById("content_films").style.display = "block";
	document.getElementById("contents_loading_films").style.display = "none";
}
</script>

<!-- Script para recargar tipos de contenidos de series (pendiente,visto...)-->
<script>
  jQuery(document).on('submit', '.content_series_form', function(event){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
	console.log(data);
;


	document.getElementById("content_series").innerHTML = (data);
	document.getElementById("content_series").style.display = "none";
	document.getElementById("contents_loading_series").style.display = "block";
	setTimeout(load_contents_series, 1500);



},
error:function(xhr,exception)
{

}
})
event.preventDefault();
  });

</script>
<script>
function load_contents_series() {
	$('#contents_films').fadeIn('slow');

	document.getElementById("content_series").style.display = "block";
	document.getElementById("contents_loading_series").style.display = "none";
}
</script>

<!-- Script para confirmar edición de descripción -->
<script>

  jQuery(document).on('submit', '.edit_description_user', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

if(json.resultado =="correcto"){

var notification = alertify.notify('Descripción editada correctamente', 'success', 3, function(){  console.log('dismissed'); });
 

}
if(json.resultado =="error"){
var notification = alertify.notify('Error: Mínimo 5 carácteres máximo 500', 'error', 3, function(){  console.log('dismissed'); });


}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>

<!-- Script para abrir modal del botón listas de los usuarios -->
<script>

  $('.button_list').click(function() {
    $('#container-list').dialog().dialog('open');


});
    jQuery("#container-list").dialog({
        autoOpen: false,
        modal: true,
        width: 500,
        MinHeight: 300,
        responsive: true,

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#container-list').dialog('close');
            })

        }

    });
 
</script>

<!-- Script para guardar la NUEVA lista -->

<script>

$(document).ready(function()
{
  $("#nuevalista").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
 $('.listado-listas').load('<?php echo base_url(uri_string());?> .listado-listas')


if(json.resultado =="correcto"){
      var notification = alertify.notify('¡Lista creada!', 'success', 3, function(){  console.log('dismissed'); });
setTimeout(function(){
    $("#formlistawrapper").dialog('close')

}, 0);


}
if(json.resultado =="errormax"){
      alertify.alert('Has alcanzado el límite máximo de listas', 'Solo puedes tener 10 listas activas a la vez. Para crear una nueva lista tendrás que borrar otra.', function(){ });
setTimeout(function(){
    $("#formlistawrapper").dialog('close')
}, 0);

}
if(json.resultado =="errortexto"){
      alertify.alert('El título y la descripción deben tener como mínimo 3 carácteres.', function(){ });



}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script> 
<script>
  	function crear_lista_open_form(){
    $(".create_list_container").toggle("slow");
    $(".wrapper-list").toggle("slow");


  }
</script>
</body>

</html>