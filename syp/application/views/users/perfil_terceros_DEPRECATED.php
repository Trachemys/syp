<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>

<title><?php echo 'Perfil: ' . $detalle->username; ?></title>
      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
          <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    #dialog{
        display: none;
    }
    </style>
</head>
 <body>
 <h4>Usuario:  <?php echo $detalle->username; ?> </h4>
<img src="<?php echo $avatar?>">
 <h4>Grupos:</h4>
<?php foreach ($grupos as $item){

    echo $item->name . ',';
    } ?> </br>
<button>Más info sobre los grupos</button>

<h1> Series </h1>

<h4>Favoritas</h4>
<?php foreach ($favoritosseries as $item){
       $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?> </br>




<h4>Siguiendo</h4>
<?php foreach ($siguiendo as $item){
 $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h4>Vistas</h4>
<?php foreach ($vistasseries as $item){
 $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h4>Pendientes</h4>
<?php foreach ($pendientesseries as $item){
 $url = base_url() . 'series/' . $item->series . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h1> Películas </h1>

<h4>Favoritas</h4>
<?php foreach ($favoritospeliculas as $item){
       $url = base_url() . 'films/' . $item->id . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>


<h4>Vistas</h4>
<?php foreach ($vistaspeliculas as $item){
       $url = base_url() . 'films/' . $item->id . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

<h4>Pendientes</h4>
<?php foreach ($pendientespeliculas as $item){
       $url = base_url() . 'films/' . $item->id . '/' . $item->title;
echo '<a href="' .  $url . '">' .  $item->title . '</a>';
    echo '</br>';
    } ?>

</body>

<div id="grupos" title="¿Qué son los grupos?">
  Los grupos son un elemento que permite distinguir a un usuario de otro. Pertenecer a un grupo te otorgará una serie de beneficios mientras navegues por nuestra web. </br></br>
  <b>Miembro:</b></br>
  Los miembros son el grupo básico de nuestra web, todos los que pertenezcan a este grupo podrán ver todas las series y películas disponibles.</br></br>
  <b>Premium:</b></br>
  El grupo de usuario premium tienen ciertas ventajas sobre los miembros. No verán ningún tipo de publicidad en nuestra web (sí en los servidores externos de vídeo) y tendrán prioridad de acceso a la web en caso de sobrecarga.</br>El acceso a este grupo es de pago. </br><a href =#> Entrar al grupo Premium</a></br></br>
  <b>Uploader:</b></br>
  Los usuarios que suban mucho contenido a nuestra web podrán pertenecer al grupo de uploaders. Tendrán exactamente las mismas ventajas que los usuarios premium y además sus links tendrán un icono (fotoicono.png) para que destaquen respecto del resto de links. </br>
  Si el contenido de los uploaders es fake o no corresponde con la serie o película podrán ser degradados de grupo.</br> <a href =#> Entrar al grupo Uploader</a></br></br> 
  <b>Fundador:</b> </br>
  El grupo de Fundadores es un grupo cerrado, única y exclusivamente podrán pertenecer a este grupo los primeros 1000 usuarios registrados en nuestra web.


</div>
<script type="text/javascript">
   $('button').click(function() {
    $('#grupos').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#grupos").dialog({
        autoOpen: false,
        modal: true,
         width: '750px',
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#grupos').dialog('close');
            })
        }
    });
}); 

</script>
</html>