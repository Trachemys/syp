<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>
      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>
<title>Tickets</title>


</head>


<body>
    <?php echo $this->session->flashdata('error'); ?>
    <?php echo $this->session->flashdata('correcto'); ?>
        </br>


<button type="button" id="button_new_ticket" class="button_new_ticket">Abrir nuevo ticket</button>

<h2> Tus tickets </h2>
<h5> Tickets abiertos </h5>
<?php echo $tickets_abiertos ?>

<h5> Esperando tu respuesta </h5>
<?php echo $tickets_pendientes ?>

<h5> Tickets cerrados </h5>
<?php echo $tickets_cerrados ?>

<div id="container_form_new_ticket" title="Abrir nuevo ticket">
<form action="<?php echo base_url()?>tickets/nuevo_ticket_validacion" METHOD="POST">
 <b>Motivo del ticket:</b></br>
 <select name="type">
<?php 
foreach ($tipos_tickets as $tipo){
	echo '<option value="'.$tipo->id.'">'.$tipo->type.'</option>';
}?>
</select></br>

 <b>Descríbenos el motivo de tu ticket:</b><br>
<textarea rows="4" cols="22" name="message"></textarea>
  <br>
  <input type="submit" value="Enviar ticket">
</form> 
</div>

<!-- Script para abrir modal box con el mensaje de comentar -->

<script>
  jQuery(document).on('click', '#button_new_ticket', function(){  

    $('#container_form_new_ticket').dialog('open');

});


jQuery(document).ready(function() {
    jQuery("#container_form_new_ticket").dialog({
        autoOpen: false,
        modal: true,

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#container_form_new_ticket').dialog('close');


            })
        }
    });
}); 

</script>
</body>

</html>