<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>
      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>
<title>Ticket #<?php echo $detalle->id; ?></title>


</head>


<body>
Ticket #<?php echo $detalle->id; ?> </br>
Estado <?php echo $detalle->status; ?> </br>
Fecha creación <?php echo $detalle->date; ?> </br>
Última actualización <?php echo $detalle->date; ?></br>

<h3>Mensajes</h3>
<?php echo $mensajes; ?>

<h4> Nuevo mensaje </h4>
<form action="<?php echo base_url()?>tickets/nuevo_mensaje_validacion" METHOD="POST">

<textarea rows="4" cols="22" name="message"></textarea>
  <input type="hidden" name="redirect" value="<?php echo base_url(uri_string());?>">
  <input type="hidden" name="id_ticket" value="<?php echo $detalle->id;?>">

  <br>
  <input type="submit" value="Enviar ticket">
</form> 
</body>

</html>