<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>
      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- ALERTAS -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>

<!-- MODAL BOX PARA ENLACES -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.0/css/iziModal.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.0/js/iziModal.min.js"></script>

<!-- ICONOS TEXTAREA -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/jquery.emojiarea.css">
    <script src="<?php echo base_url()?>asset/js_files/jquery.emojiarea.js"></script>
    <script src="<?php echo base_url()?>asset/img/icons/smileys/emojis.js"></script>

<title><?php echo $detalle->title; ?></title>


</head>

 <!-- TEXTBOX PARA LOS MENSAJES -->    
<style>
textarea, .emoji-wysiwyg-editor {
  width: 100%;
  height: 100px;
  border: 3px solid #d0d0d0;
  padding: 15px;
  font-size: 13px;
  font-family: Helvetica, arial, sans-serif;
  font-weight: normal;
  border-radius: 3px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
  -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
  -webkit-font-smoothing: antialiased;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
</style>

  <!-- MENSAJES -->
<style type='text/css'>
.comentarios {
padding: 5px;
border: 1px solid #000000;
margin-top: 15px;
list-style: none;
}

.pendiente {
}
.favorito {
}
.vista {
}
</style>
         <!-- MENSAJES -->

  <!-- BOTONES -->
<style type='text/css'>

.errorboton {
  display:none;
}

</style>
         <!-- BOTONES -->
<style>
    #dialog{
        display: none;
    }
    </style>

<body>

<div id="enlace-iframe" class="enlace-iframe" data-izimodal-transitionin="fadeInUp" data-izimodal-title="Ver película"></div>

<?php echo $this->benchmark->elapsed_time();?>

<b>Título:</b> <?php echo $detalle->title; ?><br>
<b>Año:</b> <?php echo $detalle->year; ?><br>
<b>Sinopsis:</b> <?php echo $detalle->plot; ?><br>
<b>Duración:</b> <?php echo $detalle->duration; ?><br>
<b>Carátula:</b> <?php echo $caratula; ?><br>
<b>Banner: </b><?php echo $banner; ?><br>
<b>Generos: </b><?php echo $generos; ?>





<!-- BOTONES DE SEGUIR, VISTO... -->
<div id="botonescontainer" class="botonescontainer" style="text-align: left;"><span style="line-height: 18px; font-family: Arial, Helvetica, sans-serif; font-size: 24px;"><br />

<form id="favorito" class="estados" action="<?= base_url() ?>home/favorito" method='post'>
<input type="submit" value='Favorito'  id='favorito' name='favorito' style="width: 200px; <?php echo $colorbotonfavorito; ?>" />
<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
</form>

<form id="pendiente" class="estados" action="<?= base_url() ?>home/pendiente" method='post'>
<input type="submit" value='Pendiente'  id='pendiente' name='pendiente' style="width: 200px; <?php echo $colorbotonpendiente; ?>" />
<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
</form>

<form id="pendiente" class="estados" action="<?= base_url() ?>home/vista" method='post'>
<input type="submit" value='Vista'  id='vista' name='vista' style="width: 200px; <?php echo $colorbotonvista; ?>" />
<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
</form>

</div>
<!-- BOTÓN VER ENLACES... -->

</br>
<a href="<?php echo base_url()?>films/links/<?php echo $this->uri->segment(2)?>" class="ver_enlaces">Ver enlaces</a>
</br>
<!-- BOTÓN MIS LISTAS... -->
</br>
<button type ="button" value='listasbutton'  id='listasbutton' name='listasbutton' style="width: 200px;" />Añadir a mi lista</button>
<div class="listaswrapper" id="listaswrapper" title="<?php echo 'Añadir '. $detalle->title; ?>">
        <a href='/profile'>Crear nueva lista</a></br>
<?php echo $listas ?>
</div>
</div>

<!-- Temporadas... -->
</br>
             <H1>MENSAJES</H1>
<button type="button" class="reply">Nuevo comentario</button>


<div class="errorvoto"></div>
<div class="mensajes">

<?php echo $comentarios ?>
</div>


    <div class = "wrapper_container_form_comment" title="Nuevo comentario">

   <?php
   // Si el usuario no está logeado mostramos mensaje de error, si el usuario está logeado mostramos el form para enviar el mensaje
    if (!$this->ion_auth->logged_in()){ 
      echo 'Para añadir un comentario debes entrar con tu cuenta.';
} else {
      ?>
<form id="comment_form" class="comment_form"  action="<?= base_url() ?>home/nuevo_comentario/<?= $detalle->id ?>" method='post'>

<div align="left">Comentar <button type="button" id="emojis" class="emojis">Emoticonos</button></div>
<textarea class="form-control" id="text_comentario"  name="comment" value="comment" id='comment'></textarea>




<input type='hidden' name='redirect'  id='redirect' value="<?php  echo base_url(uri_string()) ?>" />
<input type='hidden' name='parent_comment'  id='parent_comment' value="0" />
<input type='hidden' name='content' value="<?= $this->uri->segment(2) ?> ?>" id='parent_comment'/>
<input  type='hidden' class="form-control" type="text" required name="comment_name" id='name' value="<?php echo $idusuario; ?>"/>
<div id='submit_button'>
<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Enviar mensaje"/>
</div>
</form>

<?php } ?>

</div>
</div>


<div id="reporte" title="Reportar comentario">
<div class ="correctoreporte"></div>
<div class ="errorreporte"></div>
<div class="report_form">
<form id="report_comment" class="report_comment"  action="<?php echo base_url(). 'home/reportar_comentario'?>" method='post'>
<center><b>¿Cuál es el motivo del reporte?</b></br>
<select name="motivo">
  <option value="+18">Contenido +18</option>
  <option value="spam">SPAM</option>
  <option value="insultos">Faltas de respeto</option>
  <option value="ofensivo">Mensaje ofensivo</option>
  <option value="otros">Otros motivos (especificar)</option>
  </select>
  </br>
<b>Observaciones:</b>
  </br>
 <textarea name="observaciones" class="observaciones" id="observaciones" rows="4" cols="30"></textarea></br>
<input  type="submit" name="submit"  value="Enviar reporte"/>

 <input  type="hidden" name='reporte_id_comment'  id='reporte_id_comment' value="0" />
</center>
</form>
</div>

</div>

<!-- Script para abrir los modal box de los enlaces-->

<script>
$(document).on('click', '.ver_enlaces', function (event) {
    event.preventDefault();
    $('#enlace-iframe').iziModal('open', event); // Use "event" to get URL href
});
</script>

<script>
$("#enlace-iframe").iziModal({
        history: false,
    iframe : true,
    fullscreen: true,
    headerColor: '#000000',
    group: 'group1',
    loop: true,
        width: 1300,
iframeHeight: 800,
navigateArrows: true,
navigateCaption: true,

});
</script>

<!-- Script para los emoticonos -->

<script>
   var $wysiwyg = $('#text_comentario').emojiarea({button: '#emojis'});

    $wysiwyg.on('change', function() {
    });
    $wysiwyg.trigger('change');
    </script>
<!-- Script para sacar la id del comentario padre para responder a alguien -->
 <script> 
 jQuery(document).on('click', '.reply', function(e){  
  event.preventDefault();

  var id = $(this).attr("value");
    $('#parent_comment').attr("value",id);
$("#text_comentario").focus();



});
      </script>
<!-- Script para abrir modal box con el mensaje de comentar -->

<script>
  jQuery(document).on('click', '.reply', function(){  

    $('.wrapper_container_form_comment').dialog('open');

});
$('.boton_mensaje_enviar').click(function() {
  jQuery('.wrapper_container_form_comment').dialog('close');

});

jQuery(document).ready(function() {
    jQuery(".wrapper_container_form_comment").dialog({
        autoOpen: false,
        modal: true,
        width: '900px',

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('.wrapper_container_form_comment').dialog('close');


            })
        }
    });
}); 

</script>

<!-- Script para refrescar los botones-->
  <script>
  jQuery(document).on('click', '.estados', function(e){  

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', '<b>¡Enhorabuena! Has descubierto una nueva función.</b> </br> Para poder marcar tus películas y series debes tener una cuenta de usuario. ¡Es completamente gratuito!', function(){ });

}

  if(json.resultado === 'desmarcado'){

      var notification = alertify.notify('Desmarcado', 'success', 3, function(){ });
}

  if(json.resultado === 'marcadofav'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}
  if(json.resultado === 'marcadopend'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}
  if(json.resultado === 'marcadovista'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}




 $('.botonescontainer').load('<?php echo base_url(uri_string());?> .botonescontainer', function() {
  
 });


},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  


<!-- Script para refrescar los me gusta-->
  <script>
  jQuery(document).on('click', '.me_gusta_form', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

  if(json.resultado === 'correcto'){

      var notification = alertify.notify('Votado', 'success', 3, function(){  console.log('dismissed'); });
}
if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', 'Para poder votar comentarios debes entrar con tu usuario. En caso de no estar registrado te recomendamos hacerlo para desbloquear muchísimas funcionalidades. Más info...', function(){ });

      $(".errorvoto").append(json.mensaje).css({"display":"block"});

}
 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes', function() {
  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  



<!-- Script para refrescar los mensajes borrados-->
  <script>
  jQuery(document).on('click', '.borrar_form', function(e){  
  event.preventDefault();
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){

 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes', function() {
      var notification = alertify.notify('Mensaje borrado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

if(json.resultado === 'error'){

      var notification = alertify.notify('El mensaje no se ha podido borrar', 'error', 3, function(){  console.log('dismissed'); });

}
  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>



<!-- Script para abrir el formulario de reporte -->
<script type="text/javascript">
jQuery(document).on('click', '.reporteicon', function(e){  
  event.preventDefault();

      $(".errorreporte,.correctoreporte").html("").css({"display":"none"});

    $('#reporte').dialog('open');
    var id = $(this).attr("value");
    $('#reporte_id_comment').attr("value",id);

});



jQuery(document).ready(function() {
    jQuery("#reporte").dialog({
        autoOpen: false,
        modal: true,
         width: '500px',
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#reporte').dialog('close');
            })
        }
    });
}); 

</script>

<!-- Script para mostrar mensajes de reporte -->

<script>

$(document).ready(function()
{
  $("#report_comment").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
      $(".errorreporte,.correctoreporte").html("").css({"display":"none"});
 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes')
$("#observaciones").val("");
setTimeout(function(){
    $("#reporte").dialog('close')
}, 0);
if(json.resultado =="correcto"){

    if(json.mensaje){
     // $(".correctoreporte").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify('Tu reporte se ha enviado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      $(".errorreporte").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify('Hubo un problema al enviar tu reporte.', 'success', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script para recargar div después de enviar mensaje -->
<script>

  jQuery(document).on('submit', '.comment_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

      $(".errormensaje,.correctomensaje").html("").css({"display":"none"});
 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes')
$("#comment").val("");
    $('#parent_comment').val("");


if(json.resultado =="correcto"){

    if(json.mensaje){
      //$(".correctomensaje").append(json.mensaje).css({"display":"block"});
var notification = alertify.notify('¡Mensaje publicado!', 'success', 3, function(){  console.log('dismissed'); });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      //$(".errormensaje").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify(json.mensaje, 'error', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>

<!-- Script para el cambio de botones -->
 <script>
  jQuery(document).on('click', '#test', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
       console.log(data);           
 $('.botonescontainer').load('<?php echo base_url(uri_string());?> .botonescontainer', function() {

  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script> 

<!-- Script para abrir modal del botón listas de los usuarios -->
 <script>
  $('#listasbutton').click(function() {
    $('#listaswrapper').dialog('open');

});



jQuery(document).ready(function() {
    jQuery("#listaswrapper").dialog({
        autoOpen: false,
        modal: true,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#listaswrapper').dialog('close');
            })
        }
    });
}); 
</script> 
<!-- Script para refrescar las listas cuando se añade o borre una-->
  <script>
  jQuery(document).on('click', '.lista', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){

        var notification = alertify.notify('Lista editada', 'success', 1, function(){  console.log('dismissed'); });




 $('.listaswrapper').load('<?php echo base_url(uri_string());?> .listaswrapper', function() {



  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  


</body>

</html>