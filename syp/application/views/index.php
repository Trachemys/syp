<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/home.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide.css' ?>">


	<title>LOGIN</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

  	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide/slick.css' ?>"/>
	<script type="text/javascript" src="<?php echo base_url() . 'asset/js_files/slide/slick.min.js' ?>"></script>
 	<script src="<?php echo base_url() . 'asset/js_files/slide/slick.js' ?>" type="text/javascript" charset="utf-8"></script>

<style>
#cookies_container {
    position: fixed;
    bottom: 0;
    width: 100%;
    z-index: 999;
    font-size: 13px;
    background-color: rgba(0, 0, 0, 0.8);

}

#cookies_accept{
	margin-top: 5px;
	border-radius: 5px;
	border: 0px;
	padding: 6px 10px;
	font-weight: bold;
	cursor: pointer;
	text-shadow: rgb(0, 0, 0) 0px 0px 2px;
	background-color: rgb(91, 183, 91);
	color: rgb(255, 255, 255);
	text-align: center;

	}
	#cookies_info{
	margin-top: 5px;
	border-radius: 5px;
	border: 0px;
	padding: 6px 10px;
	font-weight: bold;
	cursor: pointer;
	text-shadow: rgb(0, 0, 0) 0px 0px 2px;
	background-color: #0174DF;
	color: rgb(255, 255, 255);
	text-align: center;		
	}

	#cookies_accept:hover, #cookies_info:hover{
		opacity: 0.8;
	}
</style>
</head>

<body>

	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background ?>">
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
						<span class="loading"></span>

					</form>

				</div>
			</div>

		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->

			<!-- LOS CAPITULOS PENDIENTES LOS HACEMOS EN HTML PLANO POR IMPOSIBILIDAD CON EL BACKEND -->
			<?php if(!empty($capitulos_pendientes)){
			?>
			<div class="row">
				<div class="col-4">
					<div class="slide_header"><h4>¡EH! Hay capítulos disponibles de las series que sigues</h4></div>
  					<section class="regular slider">
						<?php echo $capitulos_pendientes;?>
					</section>
				</div>
			</div>

			<?php } ?>
			<?php echo $peliculas_pendientes;?>
			<?php echo $series_pendientes;?>
			<?php echo $estrenos_cine;?>
			<?php echo $trendings_films;?>
			<?php echo $trendings_series;?>			   						   							   						   		


		<div id="main-wrapper" class="wrapper">
			<div class="row">			
 				<div class="col-1 infobox">
 					<img src="<?php echo base_url(). 'asset/img/icons/video-play.png'?>"></br></br>

 					<h5>GRATUITO</h5></br> La filosofía de esta Plataforma es que cualquiera pueda acceder a los contenidos que desee ver en un par de clicks, sin complicaciones ni cosas raras. Además, si te registras podrás acceder a muchas funcionalidades como crear tus listas, marcar tus pelis y series vistas, favoritas... Y sí, ¡Completamente gratuito!

				</div>
 				<div class="col-1 infobox">
 					<img src="<?php echo base_url(). 'asset/img/icons/clipboard.png'?>"></br></br>

 					<h5>LINKS SEGUROS</h5></br> Nuestro sistema de rangos unido a los reportes visibles hacen que puedas ir directamente al link que te interese sin que esté caído o no sea exactamente lo que quieras ver.

				</div>
 				<div class="col-1 infobox">
 					<img src="<?php echo base_url(). 'asset/img/icons/heart.png'?>"></br></br>

 					<h5>¡GRACIAS UPLOADERS!</h5></br> Desde esta Plataforma agradecemnos a los Uploaders que gastan su tiempo en añadir contenido de calidad. ¡Gracias! Y por eso os damos un rango con una serie de privilegios y la posibilidad de que el resto de usuarios os agradezcan en forma de donaciones en vuestras cuentas de PayPal o Bitcoin.

				</div>
 				<div class="col-1 infobox">
 					<img src="<?php echo base_url(). 'asset/img/icons/flame.png'?>"></br></br>				

 					<h5>¿NO ENCUENTRAS LO QUE BUSCAS?</h5></br> ¡Sin problema! Haz una petición de la película o capítulo que te interesa y si algún uploader lo tiene te lo subirá en cuanto pueda.

				</div>								 											
			</div>
		</div>

	<!-- DIV PARA ACEPTAR COOKIES -->
	<?php echo $cookies ?>
	<!-- FOOTER -->
	<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">

				<div class="row">
					<div class="col-4">
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/telegram.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/forocoches.png' ?>"></a>
						<p> 
						- 
						<a href="<?php echo base_url() ?>DMCA">DMCA</a>
						-
						<a href="<?php echo base_url() ?>TOS"> TOS </a>
						- 
						<a href="<?php echo base_url() ?>cookies">Cookies</a> 
						-
						<a href="<?php echo base_url() ?>about">Sobre nosotros</a> 
						-
						<a href="<?php echo base_url() ?>contact">Contacto</a>
						-
						</p>
						<p><b>Copyright &copy; 2017 TOBORY.com</b></p>
					</div>
				</div>
			</div>
	</footer>
  <script type="text/javascript">
      $(".regular").slick({
      	
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        variableWidth: true,
        arrows: true,
         responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        

      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: false,

      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]


      });

</script>

<!-- Script para recargar div después de enviar mensaje -->
<script>

  jQuery(document).on('submit', '#cookies_accept_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
document.getElementById("cookies_container").style.display = "none"; //or "block"

},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>

<script>
    function toggle_series_pendientes() {
    $(".pending_series_section").toggle("slow");

  }
</script>
<script>
  	function toggle_films_pendientes(){
  	$(".pending_films_section").toggle("slow");

  }
</script>

</body>

</html>