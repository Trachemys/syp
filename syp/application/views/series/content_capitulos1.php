<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<html>

<head>
      <script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>" type="text/javascript"></script> 
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- Relacionado con las alertas -->
<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>

<!-- ICONOS TEXTAREA -->
    <link rel="stylesheet" href="<?php echo base_url()?>asset/css/jquery.emojiarea.css">
    <script src="<?php echo base_url()?>asset/js_files/jquery.emojiarea.js"></script>
    <script src="<?php echo base_url()?>asset/img/icons/smileys/emojis.js"></script>

</head>
  <!-- MENSAJES -->

<style type='text/css'>
.comentarios {
padding: 5px;
border: 1px solid #000000;
margin-top: 15px;
list-style: none;
}
.seguir {
}
.pendiente {
}
.favorito {
}
.vista {
}
</style>
      <!-- TEXTBOX PARA LOS MENSAJES -->    
<style>
textarea, .emoji-wysiwyg-editor {
  width: 100%;
  height: 100px;
  border: 3px solid #d0d0d0;
  padding: 15px;
  font-size: 13px;
  font-family: Helvetica, arial, sans-serif;
  font-weight: normal;
  border-radius: 3px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
  -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 1px rgba(0,0,0,0.1);
  -webkit-font-smoothing: antialiased;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}
</style>
         <!-- MENSAJES -->

  <!-- BOTONES -->
<style type='text/css'>

.errorboton {
  display:none;
}

</style>
         <!-- BOTONES -->
<style>
    #dialog{
        display: none;
    }
    </style>
         <!-- DIVS DE MENSAJES, ENLACES, FORMULARIOS -->
<style>

    #container_formulario_enlaces{
        display: none;
    }
    #mensajes{
        display: show;
        
    }    
    

    </style>
<body>
<script>
//if(top===self){
//location.replace("<?php echo base_url()?>");

  //  alert("Solo puedes abrir esta página directamente desde la web.")
//}
</script>
<script>
$(document).ready(function(){
    $("#link_enlaces").click(function(){
        $(".listaenlace").show();
        $(".tipo_enlace").show();
        $("#container_formulario_enlaces").hide();

    });

    $("#link_formulario").click(function(){
        $("#container_formulario_enlaces").show();
        $(".listaenlace").hide();
        $(".tipo_enlace").hide();

    });

});


</script>
<a href="#" id="link_enlaces">Ver enlaces</a>
</br>
<a href="#" id="link_formulario">Añadir enlace</a>
</br>
<a href="#container_mensajes" id="link_mensajes">Críticas</a>
</br>
<?php
echo $this->session->flashdata('error') . '</br>';
echo $this->session->flashdata('correcto') . '</br>';
?>
<b>Referencia:</b> <?php echo 's'.$detalle->numerotemporada.'e'.$detalle->numeroepisodio; ?> <br>
<b>Título capitulo:</b> <?php echo $detalle->titulocapitulo; ?><br>


<b>Año:</b> <?php echo $detalle->date; ?><br>
<b>Duración:</b> <?php echo $detalle->duration; ?><br>


 <div class ="tipo_enlace"> <H3>ENLACES VISIONADO ONLINE</H3></div>
<?php echo $enlaces_visionado_online; ?>
<div class ="faltan_enlaces">
¿Faltan enlaces? ¿Están todos caídos? ¡Avísanos y los pondremos de inmediato!
<form action="<?php echo base_url()?>Reclamar_enlaces" method="POST"  class="pedir_enlaces">
<input type="hidden" name="content" value="<?php echo $this->uri->segment(2)?>">
<input type="hidden" name="type" value="1">
<input type="submit" value="Pedir enlaces">
</form>
</div>

 <div class ="tipo_enlace"> <H3>ENLACES VISIONADO ONLINE</H3></div>
<?php echo $enlaces_descarga_directa; ?>

<div class ="faltan_enlaces"> 
¿Faltan enlaces? ¿Están todos caídos? ¡Avísanos y los pondremos de inmediato!
<form action="<?php echo base_url()?>Reclamar_enlaces" method="POST" class="pedir_enlaces">
<input type="hidden" name="content" value="<?php echo $this->uri->segment(2)?>">
<input type="hidden" name="type" value="2">
<input type="submit" value="Pedir enlaces">
</form>
</div>



<div id = "container_formulario_enlaces">
              <H1>FORMULARIO ENLACES</H1>
<form id="subir_enlace" class="subir_enlace"  action="<?php echo base_url(). 'home/subir_enlace'?>" method='post'>
<b>URL</b></br>
<input type="text" name="url"></br>
<b>Resolución</b></br>
<select name="resolucion">
<?php
      foreach ($lista_calidad as $item){
         echo '<option value="'.$item->id.'">'.$item->quality.'</option>';
      }
?>
  </select></br>

  <b>Tipo de enlace</b></br>
<select name="tipo_enlace">
  <?php
      foreach ($lista_tipo_enlaces as $item){
         echo '<option value="'.$item->id.'">'.$item->type.'</option>';
      }
?>
</select></br>
  <b>Idioma audio</b></br>
  <select name="audio">
  <?php
      foreach ($lista_idiomas as $item){
        if($item->id !== '99'){
         echo '<option value="'.$item->id.'">'.$item->language.'</option>';
        }
      }
?>
</select></br>

  <b>Subtitulos</b></br>
<select name="subtitulos">
<option value="99">Sin subtitulos </option>
  <?php
      foreach ($lista_idiomas as $item){
                if($item->id !== '99'){
         echo '<option value="'.$item->id.'">'.$item->language.'</option>';
       }
      }
?>
</select></br>
<input  type="hidden" name='idcontenido'  id='idcontenido' value="<?php echo $this->uri->segment(2)?>" />
<input  type="hidden" name='redirect'  id='redirect' value="<?php echo base_url(uri_string())?>" />

<input  type="submit" name="submit"  value="Enviar"/>


</form>
</div>

<button type="button" class="reply">Nuevo comentario</button>
<div id="mensajes">

<?php echo $comentarios ?>
</div>


    <div class = "wrapper_container_form_comment" title="Nuevo comentario">
<form id="comment_form" class="comment_form"  action="<?= base_url() ?>home/nuevo_comentario/<?= $detalle->id ?>" method='post'>

<?php
    if ($this->ion_auth->logged_in())
    {
    $idusuario = $this->ion_auth->user()->row()->id;
    $variabletextarea = '';
    $placeholder = '';
    } else {
      $idusuario = "";
      $variabletextarea = 'disabled';
      $placeholder = 'Para comentar debes entrar con tu usuario';
    }
    
    ?>

<div align="left">Comentar <button type="button" id="emojis" class="emojis">Emoticonos</button></div>
<textarea class="form-control" id="text_comentario"  name="comment" <?php echo $variabletextarea; ?> value="<?= set_value("comment") ?>" id='comment'><?php echo $placeholder;?></textarea>




<input type='hidden' name='redirect'  id='redirect' value="<?php  echo base_url(uri_string()) ?>" />
<input type='hidden' name='parent_comment'  id='parent_comment' value="0" />
<input type='hidden' name='content' value="<?= $this->uri->segment(2) ?> ?>" id='parent_comment'/>
<input  type='hidden' class="form-control" type="text" required name="comment_name" id='name' value="<?php echo $idusuario; ?>"/>
<div id='submit_button'>
<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Enviar mensaje"/>
</div>
</form>
</div>
</div>


<div id="reporte" title="Reportar comentario">
<div class ="correctoreporte"></div>
<div class ="errorreporte"></div>
<div class="report_form">
<form id="report_comment" class="report_comment"  action="<?php echo base_url(). 'home/reportar_comentario'?>" method='post'>
<center><b>¿Cuál es el motivo del reporte?</b></br>
<select name="motivo">
  <option value="+18">Contenido +18</option>
  <option value="spam">SPAM</option>
  <option value="insultos">Faltas de respeto</option>
  <option value="ofensivo">Mensaje ofensivo</option>
  <option value="otros">Otros motivos (especificar)</option>
  </select>
  </br>
<b>Observaciones:</b>
  </br>
 <textarea name="observaciones" class="observaciones" id="observaciones" rows="4" cols="30"></textarea></br>
<input  type="submit" name="submit"  value="Enviar reporte"/>

 <input  type="hidden" name='reporte_id_comment'  id='reporte_id_comment' value="0" />
</center>
</form>
</div>

</div>

<!-- Script para los emoticonos -->

<script>
   var $wysiwyg = $('#text_comentario').emojiarea({button: '#emojis'});

    $wysiwyg.on('change', function() {
    });
    $wysiwyg.trigger('change');
    </script>
<!-- Script para sacar la id del comentario padre para responder a alguien -->
 <script> 
 jQuery(document).on('click', '.reply', function(e){  
  event.preventDefault();

  var id = $(this).attr("value");
    $('#parent_comment').attr("value",id);



});
      </script>
<!-- Script para abrir modal box con el mensaje de comentar -->

<script>
  jQuery(document).on('click', '.reply', function(){  

    $('.wrapper_container_form_comment').dialog('open');

});
$('.boton_mensaje_enviar').click(function() {
  jQuery('.wrapper_container_form_comment').dialog('close');

});

jQuery(document).ready(function() {
    jQuery(".wrapper_container_form_comment").dialog({
        autoOpen: false,
        modal: true,
                 width: '500px',

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('.wrapper_container_form_comment').dialog('close');


            })
        }
    });
}); 

</script>







<!-- Script para refrescar los me gusta-->
  <script>
  jQuery(document).on('click', '.me_gusta_form', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

  if(json.resultado === 'correcto'){

      var notification = alertify.notify('Votado', 'success', 3, function(){  console.log('dismissed'); });
}
if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', 'Para poder votar comentarios debes entrar con tu usuario. En caso de no estar registrado te recomendamos hacerlo para desbloquear muchísimas funcionalidades. Más info...', function(){ });

      $(".errorvoto").append(json.mensaje).css({"display":"block"});

}

   $('#mensajes').load('<?php echo base_url(uri_string());?> #mensajes');



},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  



<!-- Script para refrescar los mensajes borrados-->
  <script>
  jQuery(document).on('click', '.borrar_form', function(e){  
  event.preventDefault();
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){

 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes', function() {
      var notification = alertify.notify('Mensaje borrado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

if(json.resultado === 'error'){

      var notification = alertify.notify('El mensaje no se ha podido borrar', 'error', 3, function(){  console.log('dismissed'); });

}
  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>



<!-- Script para abrir el formulario de reporte -->
<script type="text/javascript">
jQuery(document).on('click', '.reporteicon', function(e){  
  event.preventDefault();

      $(".errorreporte,.correctoreporte").html("").css({"display":"none"});

    $('#reporte').dialog('open');
    var id = $(this).attr("value");
    $('#reporte_id_enlace').attr("value",id);

});



jQuery(document).ready(function() {
    jQuery("#reporte").dialog({
        autoOpen: false,
        modal: true,
         width: '500px',
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#reporte').dialog('close');
            })
        }
    });
}); 

</script>

<!-- Script para mostrar mensajes de reporte -->

<script>

$(document).ready(function()
{
  $("#report_enlace").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);
$("#observaciones").val("");
setTimeout(function(){
    $("#reporte").dialog('close')
}, 0);
if(json.resultado =="correcto"){

    if(json.mensaje){
      var notification = alertify.notify('Tu reporte se ha enviado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      $(".errorreporte").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify('Hubo un problema al enviar tu reporte.', 'success', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>
<!-- Script para recargar div después de enviar mensaje -->
<script>

  jQuery(document).on('submit', '.comment_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

      $(".errormensaje,.correctomensaje").html("").css({"display":"none"});
 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes')
$("#comment").val("");
    $('#parent_comment').val("");


if(json.resultado =="correcto"){

    if(json.mensaje){
      //$(".correctomensaje").append(json.mensaje).css({"display":"block"});
var notification = alertify.notify('¡Mensaje publicado!', 'success', 3, function(){  console.log('dismissed'); });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      //$(".errormensaje").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify('Error al enviar mensaje.', 'error', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>
<!-- Script para modalbox al reclamar enlaces-->
<script>

  jQuery(document).on('submit', '.pedir_enlaces', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);



if(json.resultado =="correcto"){

    if(json.mensaje){
        alertify.alert(json.header, json.mensaje, function(){ });

    }

}

else
{
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>
</body>
</html>