<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/content.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/comments.css' ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide.css' ?>">

	<title>LOGIN</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/jquery.dialogOptions.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

    

	<!-- ALERTAS -->
	<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>

	<!-- SLIDE -->
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide/slick.css' ?>"/>
	<script type="text/javascript" src="<?php echo base_url() . 'asset/js_files/slide/slick.min.js' ?>"></script>
 	<script src="<?php echo base_url() . 'asset/js_files/slide/slick.js' ?>" type="text/javascript" charset="utf-8"></script>

	<!-- MODAL BOX PARA CAPÍTULOS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/iziModal.css' ?>">
 	<script src="<?php echo base_url() . 'asset/js_files/iziModal.min.js' ?>" type="text/javascript" charset="utf-8"></script>

	<!-- ICONOS TEXTAREA -->
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/jquery.emojiarea.css">
	<script src="<?php echo base_url()?>asset/js_files/jquery.emojiarea.js"></script>
	<script src="<?php echo base_url()?>asset/img/icons/smileys/emojis.js"></script>

<body>
		<!-- BACKGROUND SHADOW -->
	<div id="background-shadow">
	</div>
	<!-- BACKGROUND IMAGE -->
	<div class="background-loading" id="background-loading">
			<div class="content_loading" id="content_loading">
			</div>
	</div>

	<img id="background-image" src="<?php echo $background;?>">
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- BACKGROUND AUTO-RESIZABLE IMAGE / TRAILER -->
	<div id="background-container">
		<!-- TRAILER -->
		<!-- IMAGE -->
		<div id="background" >
		</div>
	</div>

	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div class="wrapper">
			<!-- 1ST CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1">
					<!-- POSTER -->
					<div id="poster-container">
						<img class="poster" src="<?php echo $poster;?>">
						<p class="rating_poster_content <?php echo $rate_box;?>"><span class="rating_number_content"><?php echo $detalle->rate;?></span></p>
					</div>
				</div>
				<!-- 2ND CONTENT COLUMN -->
				<div class="col-3">
					<h1 class="media-title"><?php echo $detalle->title;?></h1>
					<ul class="media-data">
						<li><h6>AÑO:</h6></li>
						<li><?php echo $detalle->year;?></li>
						<li><h6>DURACIÓN:</h6></li>
						<li><?php echo $detalle->duration;?> min/cap</li>
						<li><h6>GÉNEROS:</h6></li>
						<li><?php echo $generos;?></li>
					
					</ul>
					<ul class="media-data">
						<li><h6>ESTADO:</h6></li>
						<li><?php echo $detalle->status;?></li>
					</ul>
				</div>
			</div>
			<!-- 2ND CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1">
					<div id="botonescontainer" class="botonescontainer">
						<!-- BUTTON VIEWED -->					
						<form id="pendiente" class="estados" action="<?= base_url() ?>home/vista" method='post'>
						<button type="submit" class="button <?php echo $colorbotonvista;?>">VISTA</button>
						<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
						</form>

						<!-- BUTTON PENDING -->					
						<form id="pendiente" class="estados" action="<?= base_url() ?>home/pendiente" method='post'>
						<button type="submit" class="button <?php echo $colorbotonpendiente;?>">PENDIENTE</button>
						<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
						</form>

						<!-- BUTTON FOLLOWING -->					
						<form id="seguir" class="estados" action="<?= base_url() ?>home/seguir" method='post'>
						<button type="submit" class="button <?php echo $colorbotonseguir;?>">SEGUIR</button>
						<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
						</form>

						<!-- BUTTON FAVOURITE -->					
						<form id="favorito" class="estados" action="<?= base_url() ?>home/favorito" method='post'>				
						<button type="submit" class="button <?php echo $colorbotonfavorito;?>">FAVORITA</button>
						<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
						</form>
					</div>
						<!-- BUTTON MY LISTS-->					
						<button type="submit" id="add-list" class="button button_addlist">AÑADIR A MIS LISTAS</button>
					
				</div>
				<!-- 2ND CONTENT COLUMN (PLOT) -->
				<div class="col-3">
					<h5>SINOPSIS</h5>
					<p class="media-plot"><?php echo $detalle->plot;?>
					<br>
					(Ver trailer)

					</p>
				</div>
				<!-- 3RD CONTENT COLUMN (BUTTONS SEASONS) -->

				<div class="col-4">
					<div class="seasons">
						<center>
							<?php foreach($temporadas as $item){
								if($item->number > 0){
									$texto_boton = 'Temporada '.$item->number;
								} else {
									$texto_boton = 'Especiales';
								}
						 	?>
						  	<form class="seasons_form" style="display: inline;" action="<?= base_url() ?>home/listar_capitulos" method='post'>
						 		<button value="<?php  echo $item->number; ?>"  id="temp<?php  echo $item->number; ?>" name="temporadabutton" class="season_button"> <?php echo $texto_boton; ?></button>
						  		<input type='hidden' name='idcontenido'  id='idcontenido' value="<?php  echo $this->uri->segment(2); ?>" />
						  		<input type='hidden' name='idtemporada'  id='idtemporada' value="<?php  echo $item->id; ?>" />
							</form>
							<?php } ?>						
						</center>
					</div>
				</div>





				<!-- 3RD CONTENT COLUMN (CHAPTER LISTING) -->
				<div class="col-4">
					<center>
						<div class="chapters_loading" id="chapters_loading">
						</div>

						<div class="chapters_container" id="chapters_container" name="chapters_container">

						</div>				
					</center>
				</div>

			</div>

				<div class="row" id="criticas-wrapper">
					<h4 class="media-title" id="toggle_criticas" onclick="ver_criticas()">&#x25BC CRÍTICAS</h4>
					<div class="col-1 button_review_div">
						<button type="button" class="button reply">PUBLICAR NUEVA CRÍTICA</button>
					</div>

					<div class="col-4">
						<?php echo $comentarios ?>
					</div>
				</div>

				<div class="row">
					<h4 class="media-title">RECOMENDACIONES</h4>
					<div class="row">
						<div class="col-4">
		  					<section class="regular slider">
		  					<?php echo $relacionados;?>
							</section>
						</div>
					</div>						
				</div>

			</div>
		</div>
		    <!-- MODAL BOX PARA ABRIR LOS CAPÍTULOS -->
			<div id="cap-iframe" class="cap-iframe" data-izimodal-transitionin="fadeInUp" data-izimodal-title="VER CAPÍTULO"></div>
         	<!-- FIN MODAL BOX PARA ABRIR LOS CAPÍTULOS -->

         	<!-- INPUT PARA COMPROBAR SI SE RECARGA TEMPORADA O SE MARCA VISTO EL CAP -->
         	<input type="hidden" id="var_reload_chapter" value="0">
         	<!-- FIN INPUT PARA COMPROBAR SI SE RECARGA TEMPORADA O SE MARCA VISTO EL CAP -->

		    <!-- MODAL BOX PARA ABRIR LAS LISTAS -->
		    <div id="container-list" style="display: none;"  title="Añadir a mis listas">
		         		<div id="listado-listas" class="listado-listas">
		           		<?php echo $listas ?>
		           		<?php echo $crear_listas_url ?>
		           			           		
		           		</div>
		    </div>
		    <!-- FIN MODAL BOX PARA ABRIR LAS LISTAS -->

		    <!-- MODAL BOX PARA ABRIR MODALBOX DE MENSAJE -->
		    <div class = "wrapper_container_form_comment" title="Nueva crítica sobre <?php echo $detalle->title ?>">
	   				<?php
	    				if (!$this->ion_auth->logged_in()){ 
	      					echo '<br><br>Para añadir una crítica sobre '.$detalle->title. ' debes entrar con tu cuenta. <br> Si aún no tienes cuenta regístrate. ¡Es completamente gratuito!';
						} else {
	      			?>
				<form id="comment_form" class="comment_form"  action="<?= base_url() ?>home/nuevo_comentario/<?= $detalle->id ?>" method='post'>
					<div align="left">
						<button type="button" id="emojis" class="emojis"><i class="icon-icons"></i> Emoticonos</button>
					</div>
					<textarea class="form-control" id="text_comentario"  name="comment" value="comment" id='comment' rows='4' cols='50'></textarea>
					<input type='hidden' name='redirect'  id='redirect' value="<?php  echo base_url(uri_string()) ?>" />
					<input type='hidden' name='parent_comment'  id='parent_comment' value="0" />
					<input type='hidden' name='content' value="<?= $this->uri->segment(2) ?> ?>" id='parent_comment'/>
					<br>
					<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Enviar crítica"/>
					<br><br>
				</form>
				<?php } ?>
			</div>
		<!-- FIN BOX PARA ABRIR LOS MENSAJES -->

		    <!-- MODAL BOX PARA ABRIR MODALBOX DE REPORTES -->
		   <div class= "reporte_criticas" id="reporte_criticas" title="Reportar crítica">
	   				<?php
	    				if (!$this->ion_auth->logged_in()){ 
	      					echo '<br>Para reportar críticas debes entrar con tu cuenta. <br> Si aún no tienes cuenta regístrate. ¡Es completamente gratuito!';
						} else {
	      			?>
				<form id="report_comment" class="report_comment" class="report_comment"  action="<?= base_url() ?>home/reportar_comentario/" method='post'>
					<br>
					<h5> Motivo del reporte </h5>
					<br>
					<?php echo $tipo_reportes ?>
					<br><br>
					<h5>Observaciones</h5>
					<br>
					<textarea class="observaciones_textarea"  name="observaciones" value="comment" rows='4' cols='20' placeholder="Cuéntanos un poco más (opcional)"></textarea>
					<input type="hidden" id="reporte_id_comment" name="reporte_id_comment"/>
					<br>
					<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Reportar"/>
					<br><br>
				</form>
				<?php } ?>
			</div>
		<!-- FIN BOX PARA ABRIR LISTA DE REPORTES -->

		    <!-- MODAL BOX PARA ABRIR MODALBOX DE MENSAJE -->
		    <div class = "wrapper_login_modal" title="Invitado">
	   				<?php
	    				if (!$this->ion_auth->logged_in()){ 
	      					echo '<br><br>Para añadir una crítica sobre '.$detalle->title. ' debes entrar con tu cuenta. <br> Si aún no tienes cuenta regístrate. ¡Es completamente gratuito!';
						} else {
	      			?>
				<form id="comment_form" class="comment_form"  action="<?= base_url() ?>home/nuevo_comentario/<?= $detalle->id ?>" method='post'>
					<div align="left">
						<button type="button" id="emojis" class="emojis"><i class="icon-icons"></i> Emoticonos</button>
					</div>
					<textarea class="form-control" id="text_comentario"  name="comment" value="comment" id='comment' rows='4' cols='50'></textarea>
					<input type='hidden' name='redirect'  id='redirect' value="<?php  echo base_url(uri_string()) ?>" />
					<input type='hidden' name='parent_comment'  id='parent_comment' value="0" />
					<input type='hidden' name='content' value="<?= $this->uri->segment(2) ?> ?>" id='parent_comment'/>
					<br>
					<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Enviar crítica"/>
					<br><br>
				</form>
				<?php } ?>
			</div>
		<!-- FIN BOX PARA ABRIR LOS MENSAJES -->
		<!-- FOOTER -->
		<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<p>ESTO ES UNA PRUEBA</p>
			</div>
		</footer>
	</div>

<!-- Script para ocultar div negro y mostrar el fondo -->
<script>
	$('#background-loading').delay(800).fadeOut(1200)
</script>

<!-- Script para ver críticas y recargar criticas -->
<script>
    function ver_criticas() {
        $(".comments-container").toggle("slow");
        $(".button_review_div").toggle("slow");

  }
  	function recargar_criticas(){
  	$(".comments-container").show();
  	$(".button_review_div").show();

  }
</script>

<!-- Script para los emoticonos -->

<script>
   var $wysiwyg = $('#text_comentario').emojiarea({button: '#emojis'});

    $wysiwyg.on('change', function() {
    });
    $wysiwyg.trigger('change');
    </script>
<!-- Script para sacar la id del comentario padre para responder a alguien -->
 <script> 
 jQuery(document).on('click', '.reply', function(e){  
  event.preventDefault();

  var id = $(this).attr("value");
    $('#parent_comment').attr("value",id);
$("#text_comentario").focus();



});
      </script>
<!-- Script para abrir modal box con el mensaje de comentar -->

<script>
  jQuery(document).on('click', '.reply', function(){  

    $('.wrapper_container_form_comment').dialog('open');

});
$('.boton_mensaje_enviar').click(function() {
  jQuery('.wrapper_container_form_comment').dialog('close');

});

jQuery(document).ready(function() {
    jQuery(".wrapper_container_form_comment").dialog({
        autoOpen: false,
        modal: true,
        responsive: true,
        width: '900px',

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('.wrapper_container_form_comment').dialog('close');


            })
        }
    });
}); 

</script>

<!-- Script para refrescar los botones-->
  <script>
  jQuery(document).on('click', '.estados', function(e){  

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', '<b>¡Enhorabuena! Has descubierto una nueva función.</b> </br> Para poder marcar tus películas y series debes tener una cuenta de usuario. ¡Es completamente gratuito!', function(){ });

}

  if(json.resultado === 'desmarcado'){

      var notification = alertify.notify('Desmarcado', 'success', 3, function(){ });
}
  if(json.resultado === 'marcadoseguir'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){  });
}
  if(json.resultado === 'marcadofav'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}
  if(json.resultado === 'marcadopend'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}
  if(json.resultado === 'marcadovista'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}




 $('.botonescontainer').load('<?php echo base_url(uri_string());?> .botonescontainer', function() {
  
 });


},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  

<!-- Script para refrescar capítulos vistos-->
  <script>
    var a = 0;
  jQuery(document).on('click', '.cap_visto', function(e){  
    var a = 1;

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', '<b>¡Enhorabuena! Has descubierto una nueva función.</b> </br> Para poder marcar tus películas y series debes tener una cuenta de usuario. ¡Es completamente gratuito!', function(){ });

}

  if(json.resultado === 'desmarcado'){

      var notification = alertify.notify('Desmarcado', 'success', 3, function(){ });
}

  if(json.resultado === 'marcadovista'){

      var notification = alertify.notify(json.mensaje, 'success', 3, function(){   });
}
// Si no hay error se vuelve a pulsar en el botón de la temporada  para actualizar sus botones
	$('#var_reload_chapter').val('1'); // Cambiamos el value del input para que no salga el recargando

document.getElementById($("#idseasonchapter").val()).click();




},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  

<!-- Script para refrescar los me gusta-->
  <script>
  jQuery(document).on('click', '.me_gusta_form', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

  if(json.resultado === 'correcto'){

      var notification = alertify.notify('Votado', 'success', 3, function(){  console.log('dismissed'); });
}
if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', 'Para poder votar comentarios debes entrar con tu usuario. En caso de no estar registrado te recomendamos hacerlo para desbloquear muchísimas funcionalidades. Más info...', function(){ });

      $(".errorvoto").append(json.mensaje).css({"display":"block"});

}
 $('#criticas-wrapper').load('<?php echo base_url(uri_string());?> #criticas-wrapper', function() {
 		recargar_criticas();

  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  



<!-- Script para refrescar los mensajes borrados-->
  <script>
  jQuery(document).on('click', '.borrar_form', function(e){  
  event.preventDefault();
$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){

 $('.mensajes').load('<?php echo base_url(uri_string());?> .mensajes', function() {
      var notification = alertify.notify('Mensaje borrado correctamente.', 'success', 3, function(){  console.log('dismissed'); });

if(json.resultado === 'error'){

      var notification = alertify.notify('El mensaje no se ha podido borrar', 'error', 3, function(){  console.log('dismissed'); });

}
  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>



<!-- Script para abrir el formulario de reporte -->
<script type="text/javascript">
jQuery(document).on('click', '.reporteicon', function(e){  
  event.preventDefault();


    $('#reporte_criticas').dialog('open');
    var id = $(this).attr("value");
    $('#reporte_id_comment').attr("value",id);

});



jQuery(document).ready(function() {
    jQuery("#reporte_criticas").dialog({
        autoOpen: false,
        modal: true,
        responsive: true,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#reporte_criticas').dialog('close');
            })
        }
    });
}); 

</script>

<!-- Script para mostrar mensajes de reporte -->

<script>

$(document).ready(function()
{
  $("#report_comment").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

$("#observaciones").val("");
setTimeout(function(){
    $("#reporte").dialog('close')
}, 0);
if(json.resultado =="correcto"){
      var notification = alertify.notify('Tu reporte se ha enviado correctamente.', 'success', 3, function(){  console.log('dismissed'); });
       jQuery('#reporte_criticas').dialog('close');


}
if(json.resultado =="error"){
      var notification = alertify.notify('Hubo un problema al enviar tu reporte.', 'success', 3, function(){  console.log('dismissed'); });

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script para recargar div después de enviar mensaje -->
<script>

  jQuery(document).on('submit', '.comment_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

      $(".errormensaje,.correctomensaje").html("").css({"display":"none"});


$("#comment").val("");
    $('#parent_comment').val("");


if(json.resultado =="correcto"){

    if(json.mensaje){
      //$(".correctomensaje").append(json.mensaje).css({"display":"block"});
var notification = alertify.notify('¡Mensaje publicado!', 'success', 3, function(){  console.log('dismissed'); });
$('#criticas-wrapper').load('<?php echo base_url(uri_string());?> #criticas-wrapper', function() {
	recargar_criticas();
 });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      //$(".errormensaje").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify(json.mensaje, 'error', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>
<!-- Script para recargar capitulos-->
<script>
  jQuery(document).on('submit', '.seasons_form', function(event){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
;

var var_reload_chapter = $('#var_reload_chapter').val();
// Si var_reload_chapter == 0 significa que el user está navegando por las temporadas por lo que mostraremos el circulo cargando
if (var_reload_chapter == 0){

	document.getElementById("chapters_container").innerHTML = (data);
	document.getElementById("chapters_container").style.display = "none";
	document.getElementById("chapters_loading").style.display = "block";
	$('#var_reload_chapter').val('0');
    setTimeout(load_chapters, 1500);
// SI var_reload_chapter !== significa que el user ha dado a capitulo visto así que modificamos el valor del input box var_reload_chapter a 1 desde el script de cap_visto
} else {
	document.getElementById("chapters_container").innerHTML = (data);
	document.getElementById("chapters_container").style.display = "block";
	$('#var_reload_chapter').val('0');
}
},
error:function(xhr,exception)
{

}
})
event.preventDefault();
  });

</script>
<script>
function load_chapters() {
	$('#chapters_container').fadeIn('slow');

	document.getElementById("chapters_container").style.display = "block";
	document.getElementById("chapters_loading").style.display = "none";
}
</script>
<!-- Script para el cambio de botones -->
 <script>
  jQuery(document).on('click', '#test', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
       console.log(data);           
 $('.botonescontainer').load('<?php echo base_url(uri_string());?> .botonescontainer', function() {

  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script> 

<!-- Script para abrir modal del botón listas de los usuarios -->
<script>

  $('#add-list').click(function() {
    $('#container-list').dialog().dialog('open');


});
    jQuery("#container-list").dialog({
        autoOpen: false,
        modal: true,
        maxWidth:400,
                maxHeight: 200,
                width: 370,
                height: 300,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#container-list').dialog('close');
            })

        }

    });
 
</script> 

<!-- Script para refrescar las listas cuando se añade o borre una-->
  <script>
  jQuery(document).on('click', '.lista', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){

        var notification = alertify.notify('Lista editada', 'success', 1, function(){  console.log('dismissed'); });




 $('#listado-listas').load('<?php echo base_url(uri_string());?> #listado-listas', function() {



  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  
<!-- Script para abrir los modal box de cada capítulo-->

<script>
$(document).on('click', '.cap-trigger', function (event) {
    event.preventDefault();
    $('#cap-iframe').iziModal('open', event); // Use "event" to get URL href
});
</script>

<script>
$("#cap-iframe").iziModal({
        history: false,
    iframe : true,
    fullscreen: true,
    headerColor: '#424242',
    group: 'group1',
    loop: true,
        borderBottom: true,

        width: 1100,
iframeHeight: 500,
navigateArrows: false,
navigateCaption: false,

});
</script>
<script type="text/javascript">
      $(".regular").slick({
      	
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5,
        variableWidth: true,
        arrows: true,
         responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,

      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: false,

      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]


      });

</script> 
</body>

</html>