<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/enlaces_capitulos.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/content.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/comments.css' ?>">

	<title>Editar perfil</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Relacionado con las alertas -->
	<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
	<!-- CSS -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
	<!-- Default theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
	<!-- Semantic UI theme -->
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>
	<!-- ICONOS TEXTAREA -->
	<link rel="stylesheet" href="<?php echo base_url()?>asset/css/jquery.emojiarea.css">
	<script src="<?php echo base_url()?>asset/js_files/jquery.emojiarea.js"></script>
	<script src="<?php echo base_url()?>asset/img/icons/smileys/emojis.js"></script>


</head>
<script>
$(document).ready(function(){
    $("#link_enlaces").click(function(){

        $(".enlaces_container").show();
        $(".container_formulario_enlaces").hide();
        
        $("#criticas-wrapper").hide();
        $(".button_review_div_chap").css({"display":"none"});
        $(".comments-container").css({"display":"none"});
        $("#btn_rply").css({"display":"none"});
    });

    $("#link_formulario").click(function(){
        $(".container_formulario_enlaces").show();
        $(".enlaces_container").hide();

        $("#criticas-wrapper").hide();
        $(".button_review_div_chap").css({"display":"none"});
        $(".comments-container").css({"display":"none"});
        $("#btn_rply").css({"display":"none"});

    });

        $("#link_mensajes").click(function(){
        $(".container_formulario_enlaces").hide();
        $(".enlaces_container").hide();

        $("#criticas-wrapper").show();
        $(".button_review_div_chap").css({"display":"block"});
        $(".comments-container").css({"display":"block"});
        $("#btn_rply").css({"display":"block"});




    });

});


</script>
<body>
	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background; ?>">
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->

		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="#" id="link_enlaces"><li>VER ENLACES</li></a>
							<a href="#" id="link_formulario"><li>AÑADIR ENLACE</li></a>
							<a href="#container_mensajes" id="link_mensajes"><li>CRÍTICAS</li></a>
						</ul>
					</nav>

				</div>
			</div>
		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div id="main-wrapper" class="wrapper">

			<!-- TITULO SERIE Y REFERENCIA -->		
			<div class="row title">
				<div class="col-4 container_numberchapter">
					<h5 class="media-numberchapter"><?php echo 'S'.$detalle->numerotemporada.' E'.$detalle->numeroepisodio;?></h5>
				</div>

				<div class="col-4">
					<h1 class="media-title"><?php echo $detalle->tituloserie;?></h1>
				</div>				
			</div>

				<!-- CRÍTICAS -->		
				<div class="row" id="criticas-wrapper">
						<button type="button" class="button reply" id="btn_rply" style="display:none">PUBLICAR NUEVA CRÍTICA</button>
					<div class="col-4">
						<?php echo $comentarios ?>
					</div>
				</div>
			<!-- ENLACES -->		
			<div class="row enlaces_container">
				<!-- ENLACES ONLINE -->
				<div class="col-4 enlaces_div">
					<div class="header-content"><p class="header-text">ENLACES ONLINE</p></div>
						<div style="overflow-x:auto;">
						  <table>
						  	<thead>
						    <tr>
						      <th>Host</th>
						      <th>Audio</th>
						      <th>Subtitulos</th>
						      <th>Calidad</th>
						      <th>Uploader</th>
						      <th>Reportes</th>
						    </tr>
							</thead>
							<tbody>
						   <?php echo $enlaces_visionado_online?>
							</tbody>
						  </table>
						</div>
						<?php if($enlaces_visionado_online == NULL){
							echo '
						Parece que no hay enlaces de visionado online. Haz una petición para que los uploaders se encarguen de ello ;).	<br><br>
						<form action="'.base_url().'Reclamar_enlaces" method="POST" class="pedir_enlaces">
						<input type="hidden" name="content" value="'.$this->uri->segment(2).'">
						<input type="hidden" name="type" value="1">
						<input type="submit" value="Pedir enlaces visionado online">
						</form>	';	
						}
						?>	
					</div>

					
				<!-- ENLACES DESCARGA DIRECTA -->
				<div class="col-4 enlaces_div">
					<div class="header-content"><p class="header-text">DESCARGA DIRECTA</p></div>
						<div style="overflow-x:auto;">
						  <table>
						  	<thead>
						    <tr>
						      <th>Host</th>
						      <th>Audio</th>
						      <th>Subtitulos</th>
						      <th>Calidad</th>
						      <th>Uploader</th>
						      <th>Reportes</th>
						    </tr>
							</thead>
							<tbody>
						   <?php echo $enlaces_descarga_directa?>
							</tbody>
						  </table>
						</div>
						<?php if($enlaces_descarga_directa == NULL){
							echo '
						Parece que no hay enlaces de visionado online. Haz una petición para que los uploaders se encarguen de ello ;).	<br><br>
						<form action="'.base_url().'Reclamar_enlaces" method="POST" class="pedir_enlaces">
						<input type="hidden" name="content" value="'.$this->uri->segment(2).'">
						<input type="hidden" name="type" value="2">
						<input type="submit" value="Pedir enlaces descarga directa">
						</form>	';	
						}
						?>								
					</div>
				</div>
				<!-- FORMULARIO SUBIR ENLACES -->
			<div class="row container_formulario_enlaces">
				<div class="col-4">
					<div class="header-content-add-url"><p class="header-text">AÑADIR ENLACES</p></div>
						<form id="subir_enlace" class="subir_enlace"  action="<?php echo base_url(). 'home/subir_enlace'?>" method='post'>
						<b>Enlace del capítulo</b></br></br>
						<input type="text" name="url">

						<div class="col-1">
							<b>Resolución</b></br></br>
							<select name="resolucion">
								<?php
							      foreach ($lista_calidad as $item){
							         echo '<option value="'.$item->id.'">'.$item->quality.'</option>'; 
								} ?>
							  </select></br>
						</div>

						<div class="col-1">
							<b>Tipo de enlace</b></br></br>
							<select name="tipo_enlace">
							    <?php
							      foreach ($lista_tipo_enlaces as $item){
							         echo '<option value="'.$item->id.'">'.$item->type.'</option>';
								} ?>
							</select></br>
						</div>

						<div class="col-1">
							<b>Idioma audio</b></br></br>
							<select name="audio">
							    <?php
							      foreach ($lista_idiomas as $item){
							        if($item->id !== '99'){
							         echo '<option value="'.$item->id.'">'.$item->language.'</option>';
							        }
								} ?>
							</select></br>
						</div>

						<div class="col-1">
							<b>Subtitulos</b></br></br>
							<select name="subtitulos">
							<option value="99">Sin subtitulos </option>
							    <?php
							      foreach ($lista_idiomas as $item){
							                if($item->id !== '99'){
							         echo '<option value="'.$item->id.'">'.$item->language.'</option>';
							       }
								} ?>
							</select></br></br>
						</div>

						<input  type="hidden" name='idcontenido'  id='idcontenido' value="<?php echo $this->uri->segment(2)?>" />
						<input  type="hidden" name='redirect'  id='redirect' value="<?php echo base_url(uri_string())?>" />

						<input  type="submit" name="submit"  value="AÑADIR NUEVO ENLACE"/>


						</form>
					</div>
				</div>				
			</div>
		</div>
	</div>
	<!-- MODAL DIVS -->

	<div id="report_link_container"  style="display: none;"  title="Reportar enlace">
		<form action="<?php echo base_url()?>home/reportar_enlaces" class="report_link_form" method="POST">
			<br><h5>Motivo del reporte</h5><br>
			<select name="type_report">
				<?php echo $tipo_reportes_enlaces ?>
			</select>
			<input type="hidden" name="key_link_input" id="key_link_input">
			<br><br><h5>Descripción</h5><br>
			<textarea  name="description" placeholder="Opcional"></textarea>
			<br><br><input type="submit" value="Reportar enlace"><br><br>

		</form>
	</div>


	 <!-- MODAL BOX PARA ABRIR MODALBOX DE MENSAJE -->
		    <div class = "wrapper_container_form_comment" title="Nueva crítica sobre <?php echo $detalle->tituloserie ?>">
	   				<?php
	    				if (!$this->ion_auth->logged_in()){ 
	      					echo '<br><br>Para añadir una crítica sobre '.$detalle->tituloserie. ' debes entrar con tu cuenta. <br> Si aún no tienes cuenta regístrate. ¡Es completamente gratuito!';
						} else {
	      			?>
				<form id="comment_form" class="comment_form"  action="<?= base_url() ?>home/nuevo_comentario/<?= $detalle->id ?>" method='post'>
					<div align="left">
						<button type="button" id="emojis" class="emojis"><i class="icon-icons"></i> Emoticonos</button>
					</div>
					<textarea class="form-control" id="text_comentario"  name="comment" value="comment" id='comment' rows='4' cols='50'></textarea>
					<input type='hidden' name='redirect'  id='redirect' value="<?php  echo base_url(uri_string()) ?>" />
					<input type='hidden' name='parent_comment'  id='parent_comment' value="0" />
					<input type='hidden' name='content' value="<?= $this->uri->segment(2) ?> ?>" id='parent_comment'/>
					<br>
					<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Enviar crítica"/>
					<br><br>
				</form>
				<?php } ?>
			</div>
		<!-- FIN BOX PARA ABRIR LOS MENSAJES -->

		    <!-- MODAL BOX PARA ABRIR MODALBOX DE REPORTES -->
		   <div class= "reporte_criticas" id="reporte_criticas" title="Reportar crítica">
	   				<?php
	    				if (!$this->ion_auth->logged_in()){ 
	      					echo '<br>Para reportar críticas debes entrar con tu cuenta. <br> Si aún no tienes cuenta regístrate. ¡Es completamente gratuito!';
						} else {
	      			?>
				<form id="report_comment" class="report_comment" class="report_comment"  action="<?= base_url() ?>home/reportar_comentario/" method='post'>
					<br>
					<h5> Motivo del reporte </h5>
					<br>
					<?php echo $tipo_reportes ?>
					<br><br>
					<h5>Observaciones</h5>
					<br>
					<textarea class="observaciones_textarea"  name="observaciones" value="comment" rows='4' cols='20' placeholder="Cuéntanos un poco más (opcional)"></textarea>
					<input type="hidden" id="reporte_id_comment" name="reporte_id_comment"/>
					<br>
					<input  type="submit" name="submit" class="boton_mensaje_enviar" value="Reportar"/>
					<br><br>
				</form>
				<?php } ?>
			</div>
		<!-- FIN BOX PARA ABRIR LISTA DE REPORTES -->


<!-- Script para ver críticas y recargar criticas -->
<script>
    function ver_criticas() {
        $(".comments-container").toggle("slow");
        $(".button_review_div").toggle("slow");

  }
  	function recargar_criticas(){
  	$(".comments-container").show();
  	$(".button_review_div").show();

  }
</script>

<!-- Script para los emoticonos -->

<script>
   var $wysiwyg = $('#text_comentario').emojiarea({button: '#emojis'});

    $wysiwyg.on('change', function() {
    });
    $wysiwyg.trigger('change');
    </script>
<!-- Script para sacar la id del comentario padre para responder a alguien -->
 <script> 
 jQuery(document).on('click', '.reply', function(e){  
  event.preventDefault();

  var id = $(this).attr("value");
    $('#parent_comment').attr("value",id);
$("#text_comentario").focus();



});
      </script>
<!-- Script para abrir modal box con el mensaje de comentar -->

<script>
  jQuery(document).on('click', '.reply', function(){  

    $('.wrapper_container_form_comment').dialog('open');

});
$('.boton_mensaje_enviar').click(function() {
  jQuery('.wrapper_container_form_comment').dialog('close');

});

jQuery(document).ready(function() {
    jQuery(".wrapper_container_form_comment").dialog({
        autoOpen: false,
        modal: true,
        responsive: true,
        width: '900px',

        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('.wrapper_container_form_comment').dialog('close');


            })
        }
    });
}); 

</script>
<!-- Script para refrescar los me gusta-->
  <script>
  jQuery(document).on('click', '.me_gusta_form', function(e){  
  event.preventDefault();

$.ajax({
type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

  if(json.resultado === 'correcto'){

      var notification = alertify.notify('Votado', 'success', 3, function(){  console.log('dismissed'); });
}
if(json.resultado === 'error'){
        alertify.alert('¡Entra en tu usuario!', 'Para poder votar comentarios debes entrar con tu usuario. En caso de no estar registrado te recomendamos hacerlo para desbloquear muchísimas funcionalidades. Más info...', function(){ });

      $(".errorvoto").append(json.mensaje).css({"display":"block"});

}
 $('#criticas-wrapper').load('<?php echo base_url(uri_string());?> #criticas-wrapper', function() {
 		recargar_criticas();

  
 });

},
error:function(xhr,exception)
{
}
})
e.preventDefault();
  });
</script>  

<!-- Script para abrir el formulario de reporte -->
<script type="text/javascript">
jQuery(document).on('click', '.reporteicon', function(e){  
  event.preventDefault();


    $('#reporte_criticas').dialog('open');
    var id = $(this).attr("value");
    $('#reporte_id_comment').attr("value",id);

});



jQuery(document).ready(function() {
    jQuery("#reporte_criticas").dialog({
        autoOpen: false,
        modal: true,
        responsive: true,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#reporte_criticas').dialog('close');
            })
        }
    });
}); 

</script>

<!-- Script para mostrar mensajes de reporte -->

<script>

$(document).ready(function()
{
  $("#report_comment").on("submit", function(e)
  {
    

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
 var json = JSON.parse(data);

$("#observaciones").val("");
setTimeout(function(){
    $("#reporte").dialog('close')
}, 0);
if(json.resultado =="correcto"){
      var notification = alertify.notify('Tu reporte se ha enviado correctamente.', 'success', 3, function(){  console.log('dismissed'); });
       jQuery('#reporte_criticas').dialog('close');


}
if(json.resultado =="error"){
      var notification = alertify.notify('Hubo un problema al enviar tu reporte.', 'success', 3, function(){  console.log('dismissed'); });

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });
});
</script>

<!-- Script para recargar div después de enviar mensaje -->
<script>

  jQuery(document).on('submit', '.comment_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

      $(".errormensaje,.correctomensaje").html("").css({"display":"none"});


$("#comment").val("");
    $('#parent_comment').val("");


if(json.resultado =="correcto"){

    if(json.mensaje){
      //$(".correctomensaje").append(json.mensaje).css({"display":"block"});
var notification = alertify.notify('¡Mensaje publicado!', 'success', 3, function(){  console.log('dismissed'); });
$('#criticas-wrapper').load('<?php echo base_url(uri_string());?> #criticas-wrapper', function() {
	recargar_criticas();
 });

    }

}
if(json.resultado =="error"){

    if(json.mensaje){
      //$(".errormensaje").append(json.mensaje).css({"display":"block"});
      var notification = alertify.notify(json.mensaje, 'error', 3, function(){  console.log('dismissed'); });

    }

}
else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>


	<script>
(function () {
	var headertext = [];
	var headers = document.querySelectorAll("thead");
	var tablebody = document.querySelectorAll("tbody");
	
	for(var i = 0; i < headers.length; i++) { headertext[i]=[]; for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) { var current = headrow; headertext[i].push(current.textContent.replace(/\r?\n|\r/,"")); } } if (headers.length > 0) {
		for (var h = 0, tbody; tbody = tablebody[h]; h++) {
			for (var i = 0, row; row = tbody.rows[i]; i++) {
			  for (var j = 0, col; col = row.cells[j]; j++) {
			    col.setAttribute("data-th", headertext[h][j]);
			  } 
			}
		}
	}
} ());
</script>

<!-- Script para mensaje confirmación al reclamar enlaces-->
<script>

  jQuery(document).on('submit', '.pedir_enlaces', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

     alertify.alert(json.header, json.mensaje, function(){ });

},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>
<!-- Script para sacar la key del enlace que estamos reportando-->
<script>
	$(document).ready(function() { 
    $('.report_icon_link').click(function() { 
        var key_link = $(this).attr('value'); 
        $(key_link).val('');
        $('#key_link_input').val(key_link);
    }); 
}); 
</script>
<!-- Script para abrir modal de reportes de ENLACES -->
<script>

  $('.report_icon_link').click(function() {
    $('#report_link_container').dialog().dialog('open');


});
    jQuery("#creport_link_container").dialog({
        autoOpen: false,
        modal: true,
        open: function(){
            jQuery('.ui-widget-overlay').bind('click',function(){
                jQuery('#report_link_container').dialog('close');
            })

        }

    });
 
</script> 
<!-- Script pmensaje confirmación reportes de ENLACES -->

<script>

  jQuery(document).on('submit', '.report_link_form', function(e){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),

success:function(data){
 var json = JSON.parse(data);

if(json.resultado =="correcto"){

    if(json.mensaje){
        alertify.alert(json.titulo, json.mensaje, function(){window.location = document.URL });
        jQuery('#report_link_container').dialog('close');


    }

}

else
{
console.log(data);
}
},
error:function(xhr,exception)
{

}
})
e.preventDefault();


  });

</script>  
</body>

</html>