<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/slide_profile_content.css' ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/content_sections.css' ?>">


	<title>LOGIN</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/jquery.dialogOptions.js"></script>
    <script src="<?php echo base_url()?>/asset/js_files/search.js"></script>

    

	<!-- ALERTAS -->
	<script src="//cdn.jsdelivr.net/alertifyjs/1.10.0/alertify.min.js"></script>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/alertify.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/default.min.css"/>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.10.0/css/themes/semantic.min.css"/>



<body>
		<!-- BACKGROUND SHADOW -->
	<div id="background-shadow">
	</div>
	<!-- BACKGROUND IMAGE -->


	<img id="background-image" src="<?php echo $background;?>">
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>
							<a href="home.html"><li>INICIO</li></a>
							<a href="films.html"><li>PELÍCULAS</li></a>
							<a href="series.html"><li>SERIES</li></a>
							<a href="login.html"><li>INICIAR SESIÓN</li></a>
							<a href="register.html"><li>REGISTRARSE</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->
					<form action="<?php echo base_url()?>search/" method="get"  id="searchform">
						<input type="text" spellcheck="false" autocomplete="off" name="search" id="search">
						<input type="submit" value="BUSCAR">
					</form>
				</div>
			</div>
		</div>
	</header>
	<!-- BACKGROUND AUTO-RESIZABLE IMAGE / TRAILER -->
	<div id="background-container">
		<!-- TRAILER -->
		<!-- IMAGE -->
		<div id="background" >
		</div>
	</div>

	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div class="wrapper">
			<!-- 1ST CONTENT ROW -->
			<div class="row">
				<!-- 1ST CONTENT COLUMN -->
				<div class="col-1 button_container">
					<!-- POSTER -->
					<form action="<?php echo base_url()?>content_sections/top_rated_films" method="GET" class="content_form">
						<button type="submit" class="button">TOP RATE</button>
					</form>
						<button type="submit" class="button">ESTRENOS</button>
						<button type="submit" class="button">POPULARES</button>
						<button type="submit" class="button">GENEROS</button>

						
				</div>
				<!-- 2ND CONTENT COLUMN -->

				<div class="col-3 content" id="content">

				</div>
								<div id="content_loading" class="content_loading"></div>

			</div>
			<!-- 2ND CONTENT ROW -->

</div>



		<!-- FOOTER -->
		<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<p>ESTO ES UNA PRUEBA</p>
			</div>
		</footer>
	</div>
<script>
  jQuery(document).on('submit', '.content_form', function(event){  
  event.preventDefault();

$.ajax({

type: "POST",
url: $(this).attr("action"),
data: $(this).serialize(),
success:function(data){
	console.log(data);
;


	document.getElementById("content").innerHTML = (data);
	document.getElementById("content").style.display = "none";
	document.getElementById("content_loading").style.display = "block";

	setTimeout(load_contents_films, 1500);



},
error:function(xhr,exception)
{

}
})
event.preventDefault();
  });

</script>
<script>
function load_contents_films() {
	$('#content').fadeIn('slow');
	document.getElementById("content_loading").style.display = "none";

	document.getElementById("content").style.display = "block";
}
</script>


</body>

</html>