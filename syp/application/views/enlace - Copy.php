<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:300,400,700">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/reset.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/main.css' ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'asset/css/users.css' ?>">
	<title>LINK</title>

	<script src="<?php echo base_url() . 'asset/jquery/jquery-3.2.1.min.js' ?>"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


	
</head>

<body>
	<!-- BACKGROUND SHADOW -->
	<div id="background-shadow"></div>
	<!-- BACKGROUND IMAGE -->
	<img id="background-image" src="<?php echo $background; ?>">
	<!-- HEADER -->
	<header>
		<!-- TOP HEADER -->
		<div id="top-header">
			<!-- LOGO -->
			<img id="logo" src="<?php echo base_url() . 'asset/img/layout/logo.png' ?>">
		</div>
		<!-- BOTTOM HEADER -->
		<div id="bottom-header">
			<!-- BOTTOM HEADER CONTAINER -->
			<div id="bottom-header-container">
				<div>
					<!-- MAIN MENU -->
					<nav id="main-menu">
						<ul>

							<a href="home.html"><li>ENLACES</li></a>
							<a href="films.html"><li>COMENTARIOS</li></a>
							<a href="series.html"><li>AÑADIR ENLACE</li></a>
							<a href="login.html"><li>REPORTAR</li></a>
						</ul>
					</nav>
					<!-- TOP SEARCH FORM -->

				</div>
			</div>
		</div>
	</header>
	<!-- MAIN CONTAINER -->
	<div id="main-container">
		<!-- MAIN CONTAINER WRAPPER -->
		<div id="main-wrapper" class="wrapper">
			<div class="row">
				<div class="col-4">
				<b>Referencia:</b> <?php echo 's'.$detalle->numerotemporada.'e'.$detalle->numeroepisodio; ?> <br>
<b>Título capitulo:</b> <?php echo $detalle->titulocapitulo; ?><br>


<b>Año:</b> <?php echo $detalle->date; ?><br>
<b>Sinopsis:</b> <?php echo $detalle->plot; ?><br>
<b>Duración:</b> <?php echo $detalle->duration; ?><br>
              <H3>ENLACES VISIONADO ONLINE</H3>
<?php echo $enlaces_visionado_online; ?>
              <H3>ENLACES DESCARGA DIRECTA</H3>
<?php echo $enlaces_descarga_directa; ?>


				</div>
			</div>
		</div>
	</div>
	<!-- FOOTER -->
	<footer>
			<!-- FOOTER WRAPPER -->
			<div class="wrapper">
				<div class="row">
					<div class="col-4">
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/twitter.png' ?>"></a>
						<a href="#"><img src="<?php echo base_url() . 'asset/img/layout/facebook.png' ?>"></a>
						<p>Copyright &copy; 2017 WEBRANDOM.algo</p>
					</div>
				</div>
			</div>
	</footer>
</body>

</html>