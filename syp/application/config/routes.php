<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//Buscador
$route['search'] = 'search/get_contents_form';

// Series
$route['series/(:num)/:any'] = 'home/perfil_series/$1';
$route['episode/(:num)/:any'] = 'home/perfil_capitulos/$1';

// Peliculas
$route['films/(:num)/:any'] = 'home/perfil_peliculas/$1';
$route['films/links/(:num)'] = 'home/perfil_peliculas_enlaces/$1';

// Listas
$route['list/(:num)/:any'] = 'home/perfil_listas/$1';

// Enlaces
$route['link/(:any)/(:num)'] = 'home/perfil_enlaces/$1/$1';

// TIckets
$route['tickets/(:num)'] = 'tickets/perfil_ticket/$1';

// Cookies Info
$route['cookies'] = 'cookies_accept/cookies_info';

// Usuarios
$route['login'] = 'users/login';
$route['logout'] = 'users/logout';
$route['forgot_password'] = 'users/pass_olvidada_form';

$route['change_email/:any/:any'] = 'users/editar_email_completar';
$route['activate/:any/:any'] = 'users/registro_completar/$1/$1';
$route['forgot_password/:any/:any'] = 'users/pass_olvidada_form_completar/$1/$1';


$route['profile'] = 'users/perfil/$1';

// Registro 
$route['register'] = 'users/registro';
$route['register/:any'] = 'users/registro/$1';

$route['users/(:num)/:any'] = 'users/perfil_terceros/$1';
$route['profile/edit'] = 'users/editar_perfil';
$route['profile/edit/donation'] = 'users/editar_cuenta_donaciones_validacion';

