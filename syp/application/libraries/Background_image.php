<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Background_image
{
	
	public function __construct()
	{
		$this->ci =& get_instance();
		$this->ci->load->model('Ion_auth_model');
		$this->ion_auth_model = $this->ci->Ion_auth_model;

		$this->ci->load->library('Ion_auth');
		$this->ion_auth = $this->ci->ion_auth; 


	}

	public function select_background(){
		if($this->ion_auth->logged_in()){
			$background = $this->background_user();
		} else {
			$background = $this->background_image_guest();
		}
		return $background;

	}

	public function background_user(){
		$query = $this->ion_auth_model->background_get_contents();
		shuffle($query);
		$background = '';
		foreach ($query as $item){
				$url= $this->ion_auth_model->background_type_content($item['content']);
				$imagen = get_headers($url);
				if ($imagen[0] !== 'HTTP/1.0 404 Not Found') {
					$background = $url;
					break;
			}
		}
		// Comprobamos si existe contenido para el usuario, si no existe mostramos background generico
		if ($background === ''){
	            $this->load->library('background_image');
				return $this->background_image->background_image();
		} else {
			return $background;
		}
	}

	public function background_image_guest(){
		$folder = opendir($_SERVER['DOCUMENT_ROOT'].'/asset/img/background');
	$i = 0;
	while(false !=($file = readdir($folder))){
	if($file != "." && $file != ".."){
	    $images[$i]= $file;
	    $i++;
	    }
	}
	$random_img=rand(0,count($images)-1);
	$imagen = $images[$random_img];
	$url = base_url() . 'asset/img/background/';
	$ruta_completa = $url . $imagen;
	return $ruta_completa;
	}


public function background_image_enlace_serie($serie){


$url = base_url() . 'asset/img/series/' . $serie . '/b.jpg' ;
return $url;
}

public function background_image_enlace_pelicula($pelicula){


$url = base_url() . 'asset/img/peliculas/' . $pelicula . '/b.jpg' ;
return $url;
}

}
