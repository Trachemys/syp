<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Mostrar_avatar
{
	
	public function __construct()
	{

	}

	
	public function mostrar_avatar($id_user, $tipo = NULL){
			$ruta_avatar = ($_SERVER['DOCUMENT_ROOT'].'/asset/img/users/' . $id_user . '/avatar/');

		$ruta_avatar_mostrar = 'asset/img/users/' . $id_user . '/avatar';
		$ruta_avatar_png = glob("".$ruta_avatar."/*.png");
		$ruta_avatar_jpg = glob("".$ruta_avatar."/*.jpg");
		$ruta_avatar_jpeg = glob("".$ruta_avatar."/*.jpeg");
		$ruta_avatar_gif = glob("".$ruta_avatar."/*.gif");

		$ruta_avatar_png_mayus = glob("".$ruta_avatar."/*.PNG");
		$ruta_avatar_jpg_mayus = glob("".$ruta_avatar."/*.JPG");
		$ruta_avatar_jpeg_mayus = glob("".$ruta_avatar."/*.JPEG");
		$ruta_avatar_gif_mayus = glob("".$ruta_avatar."/*.GIF");

		if ($tipo != NULL){
			$ext = '_thumb';
		} else {
			$ext = '';
		}

		if ($ruta_avatar_png) {
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.png';
		return $ruta;    		
		} elseif ($ruta_avatar_jpg) {
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.jpg';
		return $ruta;   			
		} elseif ($ruta_avatar_jpeg){
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.jpeg';
		return $ruta; 	
		} elseif ($ruta_avatar_gif){
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.gif';
		return $ruta; 		
		} elseif ($ruta_avatar_png_mayus){
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.PNG';
		return $ruta;
		} elseif ($ruta_avatar_jpg_mayus){
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.JPG';
		return $ruta;
		} elseif ($ruta_avatar_jpeg_mayus){
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.JPEG';
		return $ruta;
		} elseif ($ruta_avatar_gif_mayus){
		$ruta = base_url() . $ruta_avatar_mostrar .'/avatar'.$ext.'.GIF';
		return $ruta;
		} else {
		$ruta = base_url().'asset/img/layout/avatar'.$ext.'.jpg';
		return $ruta;			
		}
	}	

}
